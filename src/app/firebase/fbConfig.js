import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBMeQWT7BOZmyAbJrq3ipSzcB0J8YOMNgg",
    authDomain: "reduxchat-9987b.firebaseapp.com",
    databaseURL: "https://reduxchat-9987b.firebaseio.com",
    projectId: "reduxchat-9987b",
    storageBucket: "reduxchat-9987b.appspot.com",
    messagingSenderId: "882367378637"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

// var firebaseSecond =firebase.initializeApp(config,'Secondary');
// firebaseSecond.firestore().settings({ timestampsInSnapshots: true });



// inviterUser = (email,password) =>{
//     firebaseSecond.createUserWithEmailAndPassword(email,password).then(function(firebaseUser) {
//         console.log("User " + firebaseUser.uid + " created successfully!")
//         //I don't know if the next statement is necessary 
//        // secondaryApp.auth().signOut()
//     })
// }
export default firebase;
