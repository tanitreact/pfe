import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse/index';
import {ExampleConfig} from 'app/main/example/ExampleConfig';
import {EntrepriseConfig} from 'app/main/entreprises/entreprise/EntrepriseConfig';
import {LoginConfig} from 'app/auth/LoginConfig';
import {CatalogueConfig} from 'app/main/formations/catalogues/CatalogueConfig';
import {SessionConfig} from 'app/main/sessions/SessionConfig';
import {UserConfig} from 'app/main/users/UserConfig';
import {Error404PageConfig} from 'app/main/errors/Error404PageConfig';
//import {MenuConfig} from 'app/main/menu/MenuConfig';
import {AcademyAppConfig} from 'app/main/dashboard/AcademyAppConfig';
import {CourseConfig} from 'app/main/mycourse/CourseConfig';
//import {AcademyAppConfig } from 'app/main/academy/ AcademyAppConfig ';
import authRoles from 'app/auth/authRoles'
import _ from 'lodash';

// function setAdminAuth(configs)
// {
//     return configs.map(config => _.merge({}, config, {auth: authRoles.admin}))
// }

// function setFormateurAuth(configs)
// {
//     return configs.map(config => _.merge({}, config, {auth: authRoles.formateur}))
// }

const routeConfigs = [
 
    ExampleConfig,
    EntrepriseConfig,
    LoginConfig,
    Error404PageConfig,
    UserConfig,
    CatalogueConfig, 
    // LoginConfig,
    SessionConfig,
    AcademyAppConfig,
    CourseConfig

     //CallbackConfig
 ];
 const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs), 
    {
        path     : '/',
        component: () => <Redirect to="/login"/>
    },
    {
       exact    : true,
        component: () => <Redirect to="/app"/>
    },
];

 export default routes;
