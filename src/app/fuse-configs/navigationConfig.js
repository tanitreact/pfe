const navigationConfig = [
    
            // {
            //     'id'   : 'example-component',
            //     'title': 'Example',
            //     'type' : 'item',
            //     'icon' : 'whatshot',
            //     'url'  : '/example'
            // },
            // {
            //     'id'   : 'example-component',
            //     'title': 'Entreprises',
            //     'type' : 'item',
            //     'icon' : 'business',
            //     'url'  : '/apps/gestion-session/entreprises',
            //     'auth' : 'authRoles.admin',
            // },
            {
                'id'   : 'example-component',
                'title': 'Formations',
                'type' : 'item',
                'icon' : 'list',
                'url'  : '/app/listformation',
                //'auth' : 'authRoles.formateur',
            },
            {
                'id'   : 'example-component',
                'title': 'Formations',
                'type' : 'item',
                'icon' : 'list',
                'url'  : '/app/listformation',
                'auth' : 'authRoles.admin',
            },
            // {
            //     'id'   : 'catalogue-component',
            //     'title': 'Formations',
            //     'type' : 'collapse',
            //     'icon' : 'library_books',
            //     'auth' : 'authRoles.formateur',
            //     'auth' : 'authRoles.admin',
            //     'children': [
            //         {
            //             'id'   : 'catalogue-component',
            //             'title': 'Liste formations',
            //             'type' : 'item',
            //             'icon' : 'list',
            //             'url'  : '/app/listformation'
            //         },
            //     ]
            // },
            {
                'id'   : 'catalogue-component',
                'title': 'Sessions',
                'type' : 'item',
                'icon' : 'compare',
                'url'  : '/apps/sessions',
                'auth' : 'authRoles.formateur',
                //'auth' : 'authRoles.admin',
            },
            {
                'id'   : 'catalogue-component',
                'title': 'Sessions',
                'type' : 'item',
                'icon' : 'compare',
                'url'  : '/apps/sessions',
                //'auth' : 'authRoles.formateur',
                'auth' : 'authRoles.admin',
            },
            {
                'id'   : 'catalogue-component',
                'title': 'Gestions des utilisateurs',
                'type' : 'item',
                'icon' : 'group',
                'url'  : '/app/users',
               'auth' : 'authRoles.admin',
            },
             {
                'id'   : 'courses-component',
                'title': 'Mes sessions',
                'type' : 'item',
                'icon' : 'book',
                'url'  : '/app/mescours',
                'auth' : 'authRoles.participant',
            },
            {
                'id'   : 'examen-component',
                'title': 'Mes tests',
                'type' : 'item',
                'icon' : 'school',
                'url'  : '/app/examen',
               'auth' : 'authRoles.participant',
            },
            {
                'id'   : 'catalogue-component',
                'title': 'Statistiques',
                'type' : 'item',
                'icon' : 'rate_review',
                'url'  : '/apps/gestion-session/catalogues',
                'auth' : 'authRoles.admin',
            },
            
       
];

export default navigationConfig;
