import React from 'react';
import {FuseLoadable} from '@fuse';
import {Redirect} from 'react-router-dom';

export const AcademyAppConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display : true,
                    folded  : false,
                    position: 'left'
                },
                toolbar       : {
                    display : true,
                    style   : 'fixed',
                    position: 'below'
                },
                footer        : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                leftSidePanel : {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes  : [
        {
            path     : '/apps/academy/courses/:courseId',
            component: FuseLoadable({
                loader: () => import('./formation/Formation')
            })
        },
        {
            path     : '/apps/academy/courses',
            component: FuseLoadable({
                loader: () => import('./Formations/Formations')
            })
        },
        // {
        //     path     : '/apps/academy',
        //     component: () => <Redirect to="/apps/academy/courses"/>
        // }
    ]
};
