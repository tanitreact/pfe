import React, {Component} from 'react';
import {withStyles, Paper, Hidden, Icon, IconButton, Fab, Typography, Stepper, Step, StepLabel} from '@material-ui/core';
import {FusePageSimple, FuseScrollbars,FusePageCarded} from '@fuse';
import Grid from '@material-ui/core/Grid';
import connect from 'react-redux/es/connect/connect';
import SwipeableViews from 'react-swipeable-views';
import {green} from '@material-ui/core/colors';
import {Link} from 'react-router-dom';

import Session from '../session/Session'
import SessionDialog from '../session/SessionDialog'
import DetailFormation from '../../formations/DetailFormation';

const styles = theme => ({
    stepLabel : {
        cursor: 'pointer!important'
    },
    successFab: {
        background: green[500] + '!important',
        color     : 'white!important'
    }
});

class Formation extends Component {

    componentDidMount()
    {
        /**
         * Get the Course Data
         */
      //  this.props.getCourse(this.props.match.params);
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        /**
         * If the course is opened for the first time
         * Change ActiveStep to 1
         */
        if ( this.props.course && this.props.course.activeStep === 0 )
        {
            this.props.updateCourse({activeStep: 1});
        }
    }

    handleChangeActiveStep = (index) => {
        this.props.updateCourse({activeStep: index + 1});
    };

    handleNext = () => {
        this.props.updateCourse({activeStep: this.props.course.activeStep + 1});
    };

    handleBack = () => {
        this.props.updateCourse({activeStep: this.props.course.activeStep - 1});
    };

    render()
    {
        const {classes, course} = this.props;
        const activeStep = course && course.activeStep !== 0 ? course.activeStep : 1;

        return (
         <React.Fragment>
            <FusePageSimple
                // classes={{
                //     content: "flex flex-col flex-auto overflow-hidden",
                //     header : "h-72 min-h-72"
                // }}
                classes={{
                    header : "h-72 min-h-72",
                    sidebarHeader: "h-72 min-h-72",
                    rightSidebar : "w-400"
                }}
                header={
                    <div className="flex flex-1 items-center px-16 lg:px-24">
                        <Hidden lgUp>
                            <IconButton
                                onClick={(ev) => this.pageLayout.toggleLeftSidebar()}
                                aria-label="open left sidebar"
                            >
                                <Icon>menu</Icon>
                            </IconButton>
                        </Hidden>
                        <IconButton
                            className="mr-16"
                            to="/apps/academy/courses"
                            component={Link}
                        >
                            <Icon>arrow_back</Icon>
                        </IconButton>
                       
                            <Typography className="flex-1 text-20">formation</Typography>
                        
                    </div>
                }
                content={
                   
                        <div className="flex flex-1 relative overflow-hidden">
                            <FuseScrollbars className="w-full overflow-auto">
                                <SwipeableViews
                                    className="overflow-hidden"
                                    index={activeStep - 1}
                                    enableMouseEvents={true}
                                    onChangeIndex={this.handleChangeActiveStep}
                                >  
                                <div className="flex justify-center p-16 pb-64 sm:p-24 sm:pb-64 md:p-48 md:pb-64">                       
                                    <DetailFormation />                                               
                                </div>
                                    ))}
                                </SwipeableViews>
                            </FuseScrollbars>
                        </div>
                    
                }
               
                rightSidebarContent={
                    <Session />
                }
               
                innerScroll
                onRef={instance => {
                    this.pageLayout = instance;
                }}
            />
           <SessionDialog />
         </React.Fragment>
        )
    };
}

function mapDispatchToProps(dispatch)
{
    return ({
        // getCourse   : Actions.getCourse,
        // updateCourse: Actions.updateCourse
    });
}

function mapStateToProps(state)
{
    return {
        user: state.auth.user,
        auth : state.firebase.auth
    }
}

export default withStyles(styles, {withTheme: true})(connect(mapStateToProps, mapDispatchToProps)(Formation));
