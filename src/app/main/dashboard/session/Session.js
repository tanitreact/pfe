import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import {FuseAnimateGroup} from '@fuse';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {connect} from 'react-redux';

import {openNewDialogSession} from '../../../store/actions/session/session.action'
import {getDetailSession} from '../../../store/actions/session/user.session.action'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth:'100%',
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: 0,
    color: theme.palette.text.secondary,
  },
  button: {
    width: '100%',
    height: '50px',
    maxWidth:'100%',
    marginBottom:'20px',
    backgroundColor :'red'
  },
  ListItem:{
    width: '100%',
    maxWidth:'100%',
  },
  icon:{
    color:'red'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    display: 'flex',
    marginBottom: '15px',
   
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 250,
  },
});



class Session extends Component{
  
  handleTakeSession = (session) =>{
    //console.log('id of session',session.id)
      this.props.getDetailSession(session.id)
  }

  render(){

    const { classes } = this.props;
    const sessions = this.props.sessions || []

    const params = this.props.match.params;
    const {courseId} = params;

    const sessionItem = sessions.filter(item => item.formationId === courseId)
    const liste =  sessionItem || []
  
   // console.log( 'ju',sessionItem)

   if ( !liste || liste.length === 0 )
   {
       return (
           <div className="flex items-center justify-center h-full">
               <Typography color="textSecondary" variant="h5">
                   Aucune session disponible
               </Typography>
           </div>
       );
   }
    
      return (
            <FuseAnimateGroup
              enter={{
                animation: "transition.slideUpBigIn"
                  }}
                  className="flex flex-wrap"
                  delay={1000}
              >
              {liste.map((session) => {
                return(
                  <div className="w-full pb-24  sm:p-16" key={session.id} onClick={() =>{this.props.getDetailSession(session)}}>
                       <Card className={classes.card}  onClick={()=>{
                        this.props.openNewDialogSession()
                         }}>
                          <div className={classes.details}>
                            <CardContent className={classes.cover}>
                               <Typography component="h5" variant="h5">
                                   {session.theme}
                               </Typography>
                               <Typography variant="subtitle1" color="textSecondary">
                                    Date debut session:32 juin 2019
                               </Typography>
                            </CardContent>
                           </div> 
                          <CardMedia
                            className={classes.content}
                            image="https://fmc.si/media/cache/image/167-telekomunikacije-bcf926e444273082.jpg"
                            title="Live from space album cover"
                          /> 
                       </Card>
                  </div>
                       )
                     }
                    )}
            </FuseAnimateGroup>
   
    )}
    
 
}

Session.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch)
{
    return ({
      openNewDialogSession: () => dispatch(openNewDialogSession()),
      getDetailSession : (session) => dispatch(getDetailSession(session))
    });
}

function mapStateToProps(state)
{
   // console.log('sessions',state.firestore.ordered.sessions)
    return {
        sessions: state.firestore.ordered.sessions,
        formations: state.firestore.ordered.formations
        // searchText: eCommerceApp.products.searchText
    }
}


export default compose(
    withRouter,
      connect(mapStateToProps,mapDispatchToProps),
      firestoreConnect([
        { collection: 'sessions'},
]))(withStyles(styles)(Session));

//export default (Session);