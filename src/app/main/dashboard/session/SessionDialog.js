import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from '@lodash';
import {withStyles, Button, Card, Divider, Icon, IconButton, Typography,Dialog,DialogActions,DialogContent,Toolbar,
    AppBar,List,
    ListItem,
    ListItemIcon,
    ListItemText,} from '@material-ui/core';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import {closeNewDialogSession,closeEditDialogSession} from '../../../store/actions/session/session.action'
import {addParticipant} from '../../../store/actions/session/user.session.action'

const styles = theme => ({
    paper: {
        color: theme.palette.text.primary
    },
    ListItem:{
        width: '100%',
        maxWidth:'100%',
      },
      icon:{
        color:'red'
      },
      button: {
        width: '100%',
        height: '50px',
        maxWidth:'100%',
        marginBottom:'20px',
        backgroundColor :'red'
      },
  });

class SessionDialog extends Component{


    
        handleChange = (event) => {
            this.setState(
                _.set({...this.state}, 
                event.target.name,
                event.target.type === 'checkbox' ? event.target.checked : event.target.value));
        };
    
        closeUser= () => {
             this.props.closeEditDialogSession() ;
        };
    
       
       
        handleSubmit = () =>{
            console.log(this.state)
            this.props.inviteUser(this.state)
        }
    
        handleUpdate = () =>{
            console.log(this.state)
            this.props.updateAdd(this.state)
        }

        handleSubmitParticipant = (session_id) =>{
            console.log('ajouter',session_id)
            this.props.addParticipant(session_id)
        }

        render(){
            const {todoDialog,classes,sessionDetail} = this.props;

          
          return (
            <Dialog 
               {...todoDialog.props} onClose={this.props.closeEditDialogSession}  maxWidth="sm">
                 <DialogContent>
                     <DialogTitle id="form-dialog-title">{sessionDetail.theme}</DialogTitle>
                       <DialogContentText>
                       <List component="nav">

                            <ListItem >
                               <ListItemIcon className="mr-0">
                                  <Icon>note</Icon>Date debut formation : 
                               </ListItemIcon>
                               <ListItemText primary={new Date(sessionDetail.startDate).toDateString()}/>
                            </ListItem>

                            <ListItem >
                               <ListItemIcon className="mr-0">
                                  <Icon>note</Icon>Date fin formation : 
                               </ListItemIcon>
                               <ListItemText primary={new Date(sessionDetail.dueDate).toDateString()}/>
                            </ListItem>

                            <ListItem >
                               <ListItemIcon className="mr-0">
                                  <Icon>note</Icon>Nombre maximal de personnes : 
                               </ListItemIcon>
                               <ListItemText primary={sessionDetail.nbre_place}/>
                            </ListItem>
                            <ListItem >
                               <ListItemIcon className="mr-0">
                                  <Icon>note</Icon>Lieu : 
                               </ListItemIcon>
                               <ListItemText primary={sessionDetail.place}/>
                            </ListItem>
                        </List>
                       </DialogContentText>              
                 </DialogContent>
                <DialogActions>
                  <Button  color="primary" variant="contained" onClick={this.props.closeEditDialogSession}>
                       Annuler
                  </Button>
                  <Button  
                    color="primary" 
                    variant="contained" 
                    onClick={()=>{this.handleSubmitParticipant(sessionDetail.id)}}
                    //onClick={this.props.closeEditDialogSession}
                  >
                         S'inscrire
                  </Button>
                </DialogActions>
            </Dialog>

               
        );
    }
    
};



function mapDispatchToProps(dispatch)
{
    return {
        closeEditDialogSession: () => dispatch(closeEditDialogSession()),
        closeNewDialogSession: () => dispatch(closeNewDialogSession()),
        addParticipant : (session_id) =>dispatch(addParticipant(session_id))
    }
}

function mapStateToProps(state)
{
    console.log('Item',state.userSessionReducer.sessionDetail)
    return {
       todoDialog: state.sessionReducer.sessionDialog,
       sessionDetail: state.userSessionReducer.sessionDetail,
       //subTrainigItem : state.subTrainingReducer.subTrainigItem
       // labels    : todoApp.labels
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(SessionDialog));
//export default withStyles(null, {withTheme: true})(SessionDialog);