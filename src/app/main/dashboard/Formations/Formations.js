import React, {Component} from 'react';
import {
    withStyles,
    Button,
    Card,
    CardContent,
    OutlinedInput,
    Icon,
    TextField,
    Typography,
    CardActions,
    Divider,
    Select,
    InputLabel,
    FormControl,
    MenuItem,
    LinearProgress
} from '@material-ui/core';
import {FuseAnimate, FuseAnimateGroup} from '@fuse';
//import withReducer from 'app/store/withReducer';
import CardMedia from '@material-ui/core/CardMedia';
import connect from 'react-redux/es/connect/connect';
import classNames from 'classnames';
import _ from '@lodash';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import {Redirect} from 'react-router-dom';


const styles = theme => ({
    header    : {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + theme.palette.primary.main + ' 100%)',
        color     : theme.palette.getContrastText(theme.palette.primary.main)
    },
    headerIcon: {
        position     : 'absolute',
        top          : -64,
        left         : 0,
        opacity      : .04,
        fontSize     : 512,
        width        : 512,
        height       : 512,
        pointerEvents: 'none'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
      },
      linearProgress:{
        flexGrow: 1,
    }
});

class Formations extends Component {

 

    componentDidMount()
    {
       // this.props.getCategories();
       // this.props.getCourses();
    }

    componentDidUpdate(prevProps, prevState)
    {
        if ( !_.isEqual(this.props.courses, prevProps.courses) ||
            !_.isEqual(this.props.searchText, prevProps.searchText) ||
            !_.isEqual(this.props.categoryFilter, prevProps.categoryFilter)
        )
        {
            const data = this.getFilteredArray(this.props.courses, this.props.searchText, this.props.categoryFilter);
            this.setState({data})
        }
    }

    getFilteredArray = (data, searchText, categoryFilter) => {
        if ( searchText.length === 0 && categoryFilter === "all" )
        {
            return data;
        }

        return _.filter(data, item => {
            if ( categoryFilter !== "all" && item.category !== categoryFilter )
            {
                return false;
            }
            return item.title.toLowerCase().includes(searchText.toLowerCase())
        });
    };

    buttonStatus = (course) => {
        switch ( course.activeStep )
        {
            case course.totalSteps:
                return "COMPLETED";
            case 0:
                return "START";
            default:
                return "CONTINUE";
        }
    };


    handleClick = (item) => {
        // this.props.history.push('/apps/session/' + item.id );
         this.props.history.push('/apps/academy/courses/'+ item.id);
     };
    render()
    {
        const {classes, setSearchText, searchText, categories, categoryFilter, setCategoryFilter, theme} = this.props;

        const data = this.props.formations;

       if(data ===undefined){
         return(
            <div 
                delay={4000}
                className={classes.linearProgress}
            >
                <LinearProgress color="primary" variant="query" />
            </div>
         )
       }

        return (
            <div className="w-full" delay={1000}>

                <div className={classNames(classes.header, "relative overflow-hidden flex flex-col items-center justify-center text-center p-16 sm:p-24 h-100 sm:h-188")}>
                    <FuseAnimate animation="transition.slideUpIn" duration={400} delay={100}>
                        <Typography color="inherit" className="text-24 sm:text-40 font-light">
                            GOGO APPLICATION
                        </Typography>
                    </FuseAnimate>

                    

                    <Icon className={classes.headerIcon}>school</Icon>
                </div>

                <div className="max-w-2xl w-full mx-auto px-8 sm:px-16 py-24">
                    {/* <FuseAnimateGroup
                        enter={{
                            animation: "transition.slideUpBigIn"
                        }}
                        className="flex flex-wrap py-24"
                    > */}
                     
                    <FuseAnimateGroup
                        enter={{
                            animation: "transition.slideUpBigIn"
                        }}
                        className="flex flex-wrap py-24"
                    >
                        

                        {data.map((course) => {
                            //const category = categories.find(_cat => _cat.value === course.category);
                            return (
                                <div className="w-full pb-24 sm:w-1/2 lg:w-1/3 sm:p-16" key={course.id}>
                                    <Card elevation={1} className="flex flex-col h-300" delay={1000}>
                                        <div
                                            className="flex flex-no-shrink items-center justify-between px-24 h-64"
                                            style={{
                                                background: 'white',
                                                color:"primary"
                                               // color     : theme.palette.getContrastText(category.color)
                                            }}
                                        >
                                            <Typography className="text-center text-20 font-400">{course.title}</Typography>
                                        </div>
                                        <CardMedia
        className={classes.media}
        image="https://d2eip9sf3oo6c2.cloudfront.net/tags/images/000/000/300/full/angular2.png"
        title="Paella dish"
      />
 
                                      
                                        <Divider/>
                                   
                                        <CardActions disableSpacing className="justify-center">
                                            <Button
                                               onClick={event => this.handleClick(course)}
                                                className="justify-start px-32"
                                                //color="secondary"
                                            >
                                                En savoir plus
                                            </Button>
                                        </CardActions>
                                        <LinearProgress
                                            className="w-full"
                                            variant="determinate"
                                            value={course.activeStep * 100 / course.totalSteps}
                                            color="primary"
                                        />
                                    </Card>
                                </div>
                            )
                        })}
                    </FuseAnimateGroup>
                  
                </div>

            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return ({
        //handleGetItem: (id) => dispatch(handleGetItem(id))
    });
}

function mapStateToProps(state)
{
    return {
        formations: state.firestore.ordered.formations,
        auth : state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'formations'},
]))(withStyles(styles)(Formations));


//export default withStyles(styles, {withTheme: true})(connect(mapStateToProps, mapDispatchToProps)(Formations));
