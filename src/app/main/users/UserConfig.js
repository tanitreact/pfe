// import React from 'react';
import {FuseLoadable} from '@fuse';
//import {Redirect} from 'react-router-dom';
import React from 'react'
import PropTypes from 'prop-types'
import authRoles from '../../auth/authRoles'


// function Usertest ({ firebase, auth }) {

//     if (!isLoaded(auth)) {
//         return <span>Loading...</span>
//     }
//     if()

// }
export const UserConfig = {

    auth    : authRoles.admin,
    routes  : [
        {
            path     : '/app/users',
            component: FuseLoadable({
                loader: () => import('./Users')
            })
        },   
    ]
};


// Authguard.propTypes = {
//     firebase: PropTypes.shape({
//       login: PropTypes.func.isRequired
//     }),
//     auth: PropTypes.object
//   }
  
//   function mapStateToProps(state) {
//     return {
//       auth: state.firebase.auth
//     }
//   }
  
//   export default connect(mapStateToProps)(Usertest)