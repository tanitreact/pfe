import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {Redirect} from 'react-router-dom';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'

import {handleTakeUser,openEditUser} from '../../store/actions/users/users.action'
// import {
//   removeEntreprise,
//   getAllEntreprises,
//   UpadateSearchEntreprise,
//   getAllEntreprisesSuccess,
//    EditEntrepriseItem
//   } from '../../../store/actions/entreprise/entreprise.action'


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    //marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  iconCheck:{
    color:'green'
  },
  iconCheckNo:{
    color : 'red'
  }
});



class UserList extends Component {
  
  componentWillMount(){
    console.log('First this called');
  }

    componentDidUpdate()
    {
      console.log('hhhhhhhhhhhhhhhhhhhhhhhhpackho',this.props.auth.auth)
         if (this.props.auth.auth) {
  
             //return <span>Loading...</span>
            console.log('hhhhhhhhhhhhhhhhhhhhhhhh',this.props.auth.auth)
        }
      
    }

  //   getFilteredArray = (data, searchText) => {
  //     if ( searchText.length === 0 )
  //     {
  //         return data;
  //     }
  //     return _.filter(data, item => item.name.toLowerCase().includes(searchText.toLowerCase()));
  // };

  componentWillMount(){
    console.log('test2')
    
  }
  handleClick = (item) => {
     //this.props.EditEntrepriseItem(item)
    };
  


  render(){
    const {classes,firebase,auth} = this.props;
    const users= this.props.users || []
    console.log('this.props.auth.emailVerified ',this.props.auth.auth.uid)

    // if(this.props.auth.auth.uid ===undefined || this.props.auth.auth.emailVerified === false){
    //   return(
    //     <Redirect to='/login' />
    //   )
    // }

    // if (!isLoaded(auth.auth)) {
    //   return <span>Loading...</span>
    // }
    // if (isEmpty(auth.auth)) {
  
    //   return ( <Redirect to='/login' />)
    // }
    
    return (
        <Paper className={classes.root} delay={1000}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <CustomTableCell>Nom</CustomTableCell>
                <CustomTableCell align="right=">Email</CustomTableCell>
                <CustomTableCell  align="right=">Role</CustomTableCell>
                <CustomTableCell align="right">Etat invitation</CustomTableCell>
                <CustomTableCell align="right"></CustomTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map(user => (
                <TableRow className={classes.row} key={user.id} hover>
                  <CustomTableCell component="th" scope="entreprise">
                    {user.name}
                  </CustomTableCell>
                  <CustomTableCell align="left">{user.email_user}</CustomTableCell>
                  <CustomTableCell align="left">{user.role}</CustomTableCell>
                  {user.invitation_status ? (
                     <CustomTableCell align="right">
                       <Icon className={classes.iconCheck}>check_circle</Icon>
                     </CustomTableCell>
                      ) : (
                     <CustomTableCell align="right">
                         <Icon className={classes.iconCheckNo}>check_circle</Icon>
                     </CustomTableCell>
                  )}              
                  <CustomTableCell align="right">
                   <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     onClick={()=>{
                      this.props.handleTakeUser(user)
                      this.props.openEditUser()
                     }}
                   >
                       <i class="fas fa-edit"></i>
                   </Button>
                  </CustomTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      );
  }
}



UserList.propTypes = {
  classes: PropTypes.object.isRequired,
  firebase: PropTypes.shape({
    login: PropTypes.func.isRequired
  }),
  auth: PropTypes.object
};

const mapStateToProps = (state) => {
 
    return {
        users: state.firestore.ordered.users,
        auth : state.firebase
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleTakeUser:(user) =>(dispatch(handleTakeUser(user))),
      openEditUser:() =>(dispatch(openEditUser()))
    }}

export default compose(
  withRouter,
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'users'},
]))(withStyles(styles)(UserList));

