import React from 'react';
import {Paper, Button, Input, Icon, Typography, MuiThemeProvider} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import {openNewUser} from '../../store/actions/users/users.action'


const UserHeader = (props) => {

    // if(props.auth.auth.uid ===undefined || props.auth.auth.emailVerified === false){
    //     return(
    //       <Redirect to='/login' />
    //     )
    //   }

    return (
        <div className="flex flex-1 w-full items-center justify-between">

            <div className="flex items-center">
                {/* <FuseAnimate animation="transition.expandIn" delay={300}>
                    <Icon className="text-32 mr-0 sm:mr-12">shopping_basket</Icon>
                </FuseAnimate>
                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                    <Typography className="hidden sm:flex" variant="h6">{this.props.organism_name}</Typography>
                </FuseAnimate> */}
            </div>
            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                <Button 
                  className="whitespace-no-wrap"
                   variant="contained"
                   onClick={props.openNewUser}
                >
                <span className="hidden sm:flex">Inviter utilisateur</span>
                </Button>
            </FuseAnimate>
        </div>
    );
};

function mapDispatchToProps(dispatch)
{
    return {
       // setSearchText: Actions.setProductsSearchText
       openNewUser: () => dispatch(openNewUser())
    }
}

function mapStateToProps(state)
{
    return {
        //searchText: eCommerceApp.products.searchText,
        mainTheme : state.fuse.settings.mainTheme,
        auth : state.firebase
        //organism_name : state.firebase.profile.organism
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserHeader);
