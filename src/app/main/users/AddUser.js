import React, {Component} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar,
    
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import _ from '@lodash';

import {inviteUser,updateAdd,closeNewUser,closeEditUser,removeUser} from '../../store/actions/users/users.action'
//import {addSubTraining,closeNewSubTraining, removeSubTraining,closeEditSubTraining,updateSubTraining} from '../../../store/actions/formation/sous.formation.actions'

const roles = [
    {
      value: 'admin',
      label: 'admin',
    },
    {
      value: 'participant',
      label: 'participant',
    },
    {
      value: 'formateur',
      label: 'formateur',
    },
  ];

const newTodoState = {
    name      : '',
    email_user   : '',
    nameError      : '',
    emailError   : '',
    role:  '',
};

const styles = theme => ({
   
    textField1: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      maxWidth : '300px'
    },
    menu: {
        width: 200,
    },
  });

class AddUser extends Component {

    state = {
        ...newTodoState,
        labelMenuEl: null
    };

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        /**
         * After Dialog Open
         */

        if ( !prevProps.todoDialog.props.open && this.props.todoDialog.props.open )
        {
            
            if ( this.props.todoDialog.type === 'edit')
            {
                this.setState({...this.props.subTrainigItem});
            }

            if ( this.props.todoDialog.type === 'new' &&
                !_.isEqual(newTodoState, prevState) )
            {
                this.setState({
                        ...newTodoState,
                });
            }
        }
    }

    handleChange = (event) => {
            const isCheckbox = event.target.type ==='checkbox';
            this.setState({
                [event.target.name] : isCheckbox
                ? event.target.checked
                : event.target.value
            })
    };

    closeUser= () => {
        this.props.todoDialog.type === 'edit' ? this.props.closeEditUser() : this.props.closeNewUser();
    };

   validate = () =>{
    // var  name      = 'minimum 3 caracteres'
     let emailError ='';
     let nameError ='';
     if(!this.state.name){
        nameError='Le nom ne doit pas etre vide '
      }

     if(!this.state.email_user.includes('@')){
         emailError='Email invalid'
     }
     if(emailError || nameError){
         this.setState({
            emailError,
            nameError 
         })
         return false
     }
    return true
   }
   
    handleSubmit = (event) =>{

        event.preventDefault();
        const isValid = this.validate();  
        if(isValid){
            this.props.inviteUser(this.state)
            this.setState({
                name : '',
                email_user:'',
                nameError: '',
                emailError:''
            })
          this.props.closeNewUser();
        }
    }

    handleUpdate = (event) =>{
        console.log(this.state)


        event.preventDefault();
        const isValid = this.validate();  
        if(isValid){
            //this.props.inviteUser(this.state)
            this.props.updateAdd(this.state)
            this.setState({
                name : '',
                email_user:'',
                nameError: '',
                emailError:''
            })
            this.props.closeEditUser();
        }
    }

    canBeSubmitted()
    {
        const {name, email_user} = this.state;
        return (
            name.length > 2 && email_user.FormControl > 0
        );
    }

    render()
    {
        const {todoDialog,classes} = this.props;
        //console.log(this.props.match.params)
        
        return (
            <Dialog {...todoDialog.props} onClose={this.closeUser} fullWidth maxWidth="sm">

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            {todoDialog.type === 'new' ? 'Inviter des utilisateurs' : 'Modifier'}
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-0"}}>      

                    <div className="px-16 sm:px-24">
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Nom"
                                autoFocus
                               // error={this.state.name.length < 2}
                                name="name"
                                id="name"
                                required
                                value={this.state.name}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                                error={this.state.nameError !=''}
                                helperText={this.state.nameError}
                            />
                            
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Email"
                                id="email_user"
                                name="email_user"
                                value={this.state.email_user}
                                onChange={this.handleChange}
                                error={this.state.emailError != ''}
                                helperText={this.state.emailError}
                                required
                                variant="outlined"
                            />
                        </FormControl>  
                        {/* <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Role"
                                name="role"
                                id='role'
                                value={this.state.role}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl> */}

                         <FormControl className="mt-8 mb-16" required fullWidth>
           <TextField
               id="role"
               name="role"
               select
               label="Role"
               value={this.state.role}
               onChange={this.handleChange}
               SelectProps={{
                 native: true,
                 MenuProps: {
                    className: classes.menu,
                 },
               }}
               margin="normal"
               variant="outlined"
            >
             {roles.map(option => (
                <option key={option.id} value={option.value}>
                    {option.label}
                </option>
             ))}
            </TextField>
        </FormControl> 
                    </div>

                </DialogContent>

                {todoDialog.type === 'new' ? (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={(event) => {
                                this.handleSubmit(event)
                               
                            }}
                           // disabled={!this.canBeSubmitted()}
                        >
                            Inviter
                        </Button>
                    </DialogActions>
                ) : (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={(event) => {
                                //addFormation(this.state);
                                this.handleUpdate(event)
                            }}
                            // disabled={!this.canBeSubmitted()}
                        >
                            Save
                        </Button>
                        <IconButton
                            className="min-w-auto"
                            onClick={() => {
                                removeUser(this.state.uid);
                                this.props.closeEditUser();
                            }}
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
                )}
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return {
        
       // closeEditTodoDialog: Actions.closeEditTodoDialog,
           closeEditUser: () => dispatch(closeEditUser()),
           closeNewUser: () => dispatch(closeNewUser()),
          removeUser: () => dispatch(removeUser()),
          inviteUser:(user) => dispatch(inviteUser(user)),
          updateAdd:(user) => dispatch(updateAdd(user))
       // addTodo            : Actions.addTodo,
       // updateTodo         : Actions.updateTodo,
       // removeTodo         : Actions.removeTodo
    }
}

function mapStateToProps(state)
{
    console.log('Item',state)
    return {
       todoDialog: state.usersReducer.todoDialog,
       subTrainigItem : state.subTrainingReducer.subTrainigItem
       // labels    : todoApp.labels
    }
}

AddUser.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(AddUser));
