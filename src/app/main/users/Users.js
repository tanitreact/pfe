import React from 'react';
import {FusePageCarded} from '@fuse';
import PropTypes from 'prop-types';
import UserHeader from './UserHeader'
import UserList from './UserList';
import AddUser from './AddUser'
import { connect } from 'react-redux';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'
import {Redirect} from 'react-router-dom';

const Users = (props) => {


 return (
    <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
              <UserHeader />
            }
            content={
               <UserList />
            }
            innerScroll
        />
        <AddUser />
     </React.Fragment>
    );
};

Users.propTypes = {
    firebase: PropTypes.shape({
      login: PropTypes.func.isRequired
    }),
    auth: PropTypes.object
  }
  
  function mapStateToProps(state) {
    return {
      auth: state.firebase.auth
    }
  }
  
  export default connect(mapStateToProps)(Users)
//export default Users;