import React, {Component} from 'react';
import {Icon, Table, TableBody, TableCell, TablePagination, TableRow, Checkbox, IconButton,
    Menu,
    MenuList,
    MenuItem,
    ListItemIcon,
    ListItemText,} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import connect from 'react-redux/es/connect/connect';
import classNames from 'classnames';
import _ from '@lodash';
import FormationsTableHead from './FormationsTableHead';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles';

//import * as Actions from '../store/actions';
import {handleTakeFormation,openEditFormation,supprimerFormation} from '../../store/actions/formation/catalogue.action'

const styles = theme => ({
    actionsButtonWrapper: {
        background: theme.palette.background.paper
    }
});

class ListFormations extends Component {

    state = {
        order      : 'asc',
        orderBy    : null,
        selected   : [],
       // data       : this.props.formations || [],
        page       : 0,
        rowsPerPage: 10,
        selectedMenu: null
    };

    // componentDidMount()
    // {
    //     this.props.getProducts();
    // }

    componentDidUpdate(prevProps, prevState)
    {
        if ( !_.isEqual(this.props.products, prevProps.products) || !_.isEqual(this.props.searchText, prevProps.searchText) )
        {
            const data = this.getFilteredArray(this.props.formations, this.props.searchText);
            this.setState({data})
        }
    }

    getFilteredArray = (data, searchText) => {
        if ( searchText.length === 0 )
        {
            return data;
        }
        return _.filter(data, item => item.name.toLowerCase().includes(searchText.toLowerCase()));
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if ( this.state.orderBy === property && this.state.order === 'desc' )
        {
            order = 'asc';
        }

        this.setState({
            order,
            orderBy
        });
    };

    handleSelectAllClick = event => {
        if ( event.target.checked )
        {
            this.setState(state => ({selected: this.state.data.map(n => n.id)}));
            return;
        }
        this.setState({selected: []});
    };

    handleClick = (item) => {
        console.log('history',this.props.history)
        this.props.history.push('/app/formation/'+ item.formationId + '/'+ 'isEdit');
    };
    handleClickDetail = (item) => {
        console.log('history',this.props.history)
        this.props.history.push('/app/formation/'+ item.formationId + '/'+'isDetail');
    };

    handleCheck = (event, id) => {
        const {selected} = this.state;
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if ( selectedIndex === -1 )
        {
            newSelected = newSelected.concat(selected, id);
        }
        else if ( selectedIndex === 0 )
        {
            newSelected = newSelected.concat(selected.slice(1));
        }
        else if ( selectedIndex === selected.length - 1 )
        {
            newSelected = newSelected.concat(selected.slice(0, -1));
        }
        else if ( selectedIndex > 0 )
        {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1)
            );
        }

        this.setState({selected: newSelected});
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };
    openSelectedProductsMenu = (event) => {
        
        this.setState({selectedMenu: event.currentTarget});
    };

    closeSelectedProductsMenu = () => {
        this.setState({selectedMenu: null});
    };
   // isSelected = id => this.state.selected.indexOf(id) !== -1;
   // isSelectedFormation = formationid => this.state.selected.indexOf(id) !== -1;

   handleRemoveFormation = (id) =>{
       this.props.supprimerFormation(id)
   }
render()
    {
        
   var varvar = true;
   var varvarvar = false;
    
        const {order, orderBy, selected, rowsPerPage, page, selectedMenu} = this.state;

        const data = this.props.formations || []

        if(data ===null || data ==undefined || data==[]){

            return(
                <div>
                    <h2>Aucune formation</h2>
                </div>
            )
        } 

        const {classes} = this.props

        return (
            <div className="w-full flex flex-col" delay={1000}>

                <FuseScrollbars className="flex-grow overflow-x-auto">

                    <Table className="min-w-xl" aria-labelledby="tableTitle">

                        <FormationsTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={this.handleSelectAllClick}
                            onRequestSort={this.handleRequestSort}
                           // handleRemoveFormation={this.handleRemoveFormation}
                            //rowCount={data.length}
                        />

                        <TableBody>
                            {
                             data.map(n => {
                                  //  const isSelected = this.isSelected(n.formationId);
                                  console.log('id',n.id)
                                    return (
                                        <TableRow
                                            className="h-64 cursor-pointer"
                                            hover
                                            //role="checkbox"
                                            //aria-checked={isSelected}
                                           // tabIndex={-1}
                                            key={n.id}
                                           // selected={isSelected}
                                            
                                        >
                                            <TableCell className="w-52" component="th" scope="row" padding="none">
                                                {/* {n.images.length > 0 ? (
                                                    <img className="w-full block rounded" src={_.find(n.images, {id: n.featuredImageId}).url} alt={n.name}/>
                                                ) : ( */}
                                                    <img className="w-full block rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={n.name}/>
                                                {/* )} */}
                                            </TableCell>

                                            <TableCell 
                                               component="th" 
                                               scope="row" 
                                               onClick={() => {
                                                //this.closeSelectedProductsMenu();
                                                this.handleClickDetail(n)
                                            }}
                                            >
                                                {n.title}
                                            </TableCell>

                                            <TableCell 
                                                component="th" 
                                                scope="row"
                                              
                                                onClick={() => {
                                                    //this.closeSelectedProductsMenu();
                                                    this.handleClickDetail(n)
                                                }}
                                            >
                                                {n.chapitres.join(', ')}
                                            
                                            </TableCell>

                                            <TableCell 
                                               component="th" 
                                               scope="row" 
                                               align="left"
                                               onClick={() => {
                                                //this.closeSelectedProductsMenu();
                                                this.handleClickDetail(n)
                                            }}
                                            >
                                                {n.statut}
                                            </TableCell>
                                            <TableCell className="w-48 pl-4 sm:pl-12">     
                                                {/* <Checkbox
                                                    checked={isSelected}
                                                    onClick={event => event.stopPropagation()}
                                                    onChange={event => this.handleCheck(event, n.id)}
                                                /> */}



                                <IconButton
                                    aria-owns={selectedMenu ? 'selectedProductsMenu' : null}
                                    aria-haspopup="true"
                                    onClick={event =>this.openSelectedProductsMenu(event)}
                                >
                                    <Icon>more_horiz</Icon>
                                </IconButton>
                                <Menu
                                    id="selectedProductsMenu"
                                    anchorEl={selectedMenu}
                                    open={Boolean(selectedMenu)}
                                    onClose={this.closeSelectedProductsMenu}
                                >
                                    <MenuList>
                                        <MenuItem
                                            onClick={() => {
                                                this.closeSelectedProductsMenu();
                                                this.handleClick(n)
                                            }}
                                        >
                                            <ListItemIcon className={classes.icon}>
                                                <Icon>edit</Icon>
                                            </ListItemIcon>
                                            <ListItemText inset primary="Modifier"/>
                                        </MenuItem>
                                    </MenuList>
                                   
                                    <MenuList>
                                        <MenuItem
                                            onClick={() => {
                                                this.props.supprimerFormation(n.id);
                                                this.closeSelectedProductsMenu();
                                            }}
                                        >
                                            <ListItemIcon className={classes.icon}>
                                                <Icon>delete</Icon>
                                            </ListItemIcon>
                                            <ListItemText inset primary="Supprimer"/>
                                        </MenuItem>
                                    </MenuList>
                                </Menu>
                            
                                            </TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </FuseScrollbars>

            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return ({
        handleTakeFormation:(n) =>(dispatch(handleTakeFormation(n))),
        supprimerFormation :(id) =>(dispatch(supprimerFormation(id))),
        openEditFormation:() =>(dispatch(openEditFormation())),
    });
}

function mapStateToProps(state)
{
    return {
        formations: state.firestore.ordered.formations,
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'formations'},
]))(withStyles(styles)(ListFormations));

//export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListFormations));
