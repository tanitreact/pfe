import React, {Fragment} from 'react';
import {Icon, IconButton} from '@material-ui/core';

const FormUploadImage = ({onChange}) => {

    return (
        <Fragment>
            <input
                accept="image/*"
                className="hidden"
                id="button-file"
                type="file"
                onChange={onChange}
            />
            <label htmlFor="button-file">
             Ajouter une image
                <IconButton className="w-32 h-32 mx-4 p-0" component="span">
                    <Icon fontSize="small">image</Icon> 
                </IconButton>
            </label>
        </Fragment>
    );
};

export default FormUploadImage;
