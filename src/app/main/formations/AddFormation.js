import React, {Component} from 'react';
import {withStyles, Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography} from '@material-ui/core';
import {FuseAnimate, FusePageCarded, FuseChipSelect} from '@fuse';
import {orange} from '@material-ui/core/colors';
import {Link, withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import connect from 'react-redux/es/connect/connect';
import classNames from 'classnames';
import _ from '@lodash';

import {addFormation,getFormation,newFormation,updateFormation} from '../../store/actions/formation/catalogue.action'
//import withReducer from 'app/store/withReducer';
//import * as Actions from '../store/actions';
//import reducer from '../store/reducers';

const styles = theme => ({
    productImageFeaturedStar: {
        position: 'absolute',
        top     : 0,
        right   : 0,
        color   : orange[400],
        opacity : 0
    },
    productImageItem        : {
        transitionProperty      : 'box-shadow',
        transitionDuration      : theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover'               : {
            boxShadow                    : theme.shadows[5],
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured'            : {
            pointerEvents                      : 'none',
            boxShadow                          : theme.shadows[3],
            '& $productImageFeaturedStar'      : {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
});


const statuts = [
    {
      value: 'Privée',
      label: 'Privée',
    },
    {
      value: 'Public',
      label: 'Public',
    },
  ];
//   const form = {
//    // id              : FuseUtils.generateGUID(),
//     title            :'',
//     statut           :'',
//     description     :'',
//     chapitres      : [],
//     image          : '',
// }

class AddFormation extends Component {

    state = {
        tabValue: 0,
        title            :'',
        statut           :'',
        description     :'',
        chapitres      : [],
        image          : '',
        titleError            :'',
        descriptionError      :'',
    };
    

    // componentDidMount()
    // {
    //     this.updateProductState();
    // }

    // componentDidUpdate(prevProps, prevState, snapshot)
    // {
    //     if ( !_.isEqual(this.props.location, prevProps.location) )
    //     {
    //         this.updateProductState();
    //     }

    //     if (
    //         (this.props.formation && !this.state.form) ||
    //         (this.props.formation && this.state.form && this.props.formation.id !== this.state.form.id)
    //     )
    //     {
    //         console.log('test')
    //         this.updateFormState();
    //     }
    // }

    // updateFormState = () => {
    //     this.setState({form: this.props.formation})
    // };

    // updateProductState = () => {
    //     const params = this.props.match.params;
    //     console.log('test',params)
    //     const {formationId} = params;

    //     if ( formationId === 'new' )
    //     {
    //         this.props.newFormation()
    //     }else{
    //       this.props.getFormation(formationId)
    //       this.setState({form: this.props.formation})
    //     }
    // };

    handleChangeTab = (event, tabValue) => {
        this.setState({tabValue});
    };

    handleChange = (event) => {
  
            const isCheckbox = event.target.type ==='checkbox';
            this.setState({
                [event.target.name] : isCheckbox
                ? event.target.checked
                : event.target.value
            })
    };
    handleChipChange = (value, title) => {
        this.setState({form: _.set({...this.state.form}, title, value.map(item => item.value))});
    };

    setFeaturedImage = (id) => {
        this.setState({form: _.set({...this.state.form}, 'featuredImageId', id)});
    };

    canBeSubmitted()
    {
        const {title} = this.state.form;
        return (
            title.length > 0 
        );
    }

    Validate = () =>{

         let titleError ='';
         let descriptionError ='';
         if(!this.state.form.title){
            titleError='Le nom ne doit pas etre vide '
          }
         if(!this.state.form.description){
            descriptionError='Le nom ne doit pas etre vide '
         }
       
         if(titleError || descriptionError){
             this.setState({
                titleError,
                descriptionError 
             })
             return false
         }
        return true
    }

    handleSubmit = () =>{
   
        const isValid = this.Validate();  
        if(isValid){
            this.props.addFormation(this.state)
            this.setState({
               form    : {
                  // id              : FuseUtils.generateGUID(),
                   title            : '',
                   handle          : '',
                   description     : '',
                   chapitres      : [],
                   //tags            : [],
                   images          : [],
                },
                  titleError :'',
                  descriptionError :'',
            })
        }
    }

    handleUpdate = (event) =>{

        event.preventDefault();
        const isValid = this.Validate();  

        if(isValid){
            this.props.updateFormation(this.state,this.props.match.params.formationId)
            this.setState({
                form    : {
                   // id              : FuseUtils.generateGUID(),
                    title            : '',
                    handle          : '',
                    description     : '',
                    chapitres      : [],
                    //tags            : [],
                    images          : [],
                 },
                   titleError :'',
                   descriptionError :'',
             })
        }
    }
    render()
    {
        const {classes, saveProduct} = this.props;
        const {tabValue, form} = this.state;

        

        return (
            <FusePageCarded
                classes={{
                    toolbar: "p-0",
                    header : "h-50 min-h-50 sm:h-70 sm:min-h-70 p-20"
                }}
                header={
                    form && (
                        <div className="flex flex-1 w-full items-center justify-between">

                            <div className="flex flex-col items-start max-w-full">

                                <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                    <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/app/listformation">
                                        <Icon className="mr-4 text-20">arrow_back</Icon>
                                           Formations
                                    </Typography>
                                </FuseAnimate>

                                <div className="flex items-center max-w-full">
                                    <FuseAnimate animation="transition.expandIn" delay={300}>
                                        <img className="w-32 sm:w-48 mr-8 sm:mr-16 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.title}/>
                                    </FuseAnimate>
                                    <div className="flex flex-col min-w-0">
                                        <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                            <Typography className="text-16 sm:text-20 truncate">
                                                {form.title ? form.title : 'Nouvelle formation'}
                                            </Typography>
                                        </FuseAnimate>
                                    </div>
                                </div>
                            </div>
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                    {
                        this.props.match.params.formationId === 'new' ? (
                          <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                onClick = {this.handleSubmit}
                          >
                            Enregistrer
                        </Button>
                        ) : (
                            <Button
                              className="whitespace-no-wrap"
                              variant="contained"
                              onClick={this.handleUpdate}
                            >
                            Update
                        </Button>
                        )
                        
                    }
                        
                    </FuseAnimate>
                        </div>
                    )
                }
              
                content={
                    form && (
                        <div className="p-16 sm:p-24 max-w-2xl" delay={1000}>
                            {tabValue === 0 &&
                            (
                                <div>

                                    <TextField
                                        className="mt-8 mb-16"
                                        required
                                        label="Titre formation"
                                        autoFocus
                                        id="title"
                                        name="title"
                                        value={form.title}
                                        onChange={this.handleChange}
                                        variant="outlined"
                                        fullWidth
                                        error={this.state.titleError !=''}
                                        helperText={this.state.titleError}
                                    />
                                    <TextField
                                        className="mt-8 mb-16"
                                        helperText="statut formation"
                                        id="statut"
                                        name="statut"
                                        select
                                        value={form.statut}
                                        onChange={this.handleChange}
                                        SelectProps={{
                                            native: true,
                                            MenuProps: {
                                               className: classes.menu,
                                            },
                                          }}
                                          
                                        variant="outlined"
                                        fullWidth
                                        
                                            margin="normal"
                                          variant="outlined"
                                        >
                                          {statuts.map(option => (
                                            <option key={option.value} value={option.value}>
                                                {option.label}
                                            </option>
                                         ))}
                                    </TextField>

                                    <TextField
                                        className="mt-8 mb-16"
                                        id="description"
                                        name="description"
                                        onChange={this.handleChange}
                                        label="Description"
                                        type="text"
                                        value={form.description}
                                        multiline
                                        rows={6}
                                        variant="outlined"
                                        fullWidth
                                        error={this.state.descriptionError !=''}
                                        helperText={this.state.descriptionError}
                                    />

                                    <FuseChipSelect
                                        className="mt-8 mb-24"
                                        value={
                                            form.chapitres.map(item => ({
                                                value: item,
                                                label: item
                                            }))
                                        }
                                        onChange={(value) => this.handleChipChange(value, 'chapitres')}
                                        placeholder="Ajouter les chapitres"
                                        textFieldProps={{
                                            label          : 'Chapitres',
                                            InputLabelProps: {
                                                shrink: true
                                            },
                                            variant        : 'outlined'
                                        }}
                                        isMulti
                                    />
                                    <TextField
                                        className="mt-8 mb-16"
                                        required
                                        label="Image"
                                        id="image"
                                        name="image"
                                        type="file"
                                        value={form.image}
                                        onChange={this.handleChange}
                                        variant="outlined"
                                        fullWidth
                                        InputProps={{
                                            endAdornment: 
                                            <InputAdornment position="end">
                                              <Icon>image</Icon>
                                              </InputAdornment>,
                                        }}
                                        // error={this.state.titleError !=''}
                                        // helperText={this.state.titleError}
                                    />
                                </div>
                            )}
                        </div>
                    )
                }
                innerScroll
            />
        )
    };
}

function mapDispatchToProps(dispatch)
{
    return ({    
        newFormation: () => dispatch(newFormation()),
        addFormation:(formation) => dispatch(addFormation(formation)),
        getFormation:(formation) =>(dispatch(getFormation(formation))),
        updateFormation:(formation,id) =>(dispatch(updateFormation(formation,id))),
    });
}

function mapStateToProps(state)
{
    return {
     formation : state.catalogueReducer.formation
    }
}

export default withStyles(styles, {withTheme: true})(withRouter(connect(mapStateToProps, mapDispatchToProps)(AddFormation)));
