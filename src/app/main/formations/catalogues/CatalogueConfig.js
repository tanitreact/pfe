// import React from 'react';
import {FuseLoadable} from '@fuse';
//import {Redirect} from 'react-router-dom';
import authRoles from '../../../auth/authRoles'


export const CatalogueConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display : true,
                    folded  : false,
                    position: 'left'
                },
                toolbar       : {
                    display : true,
                    style   : 'fixed',
                    position: 'below'
                },
                footer        : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                leftSidePanel : {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },

    routes  : [
       
        {
            path     : '/apps/formation/:formationId',
            component: FuseLoadable({
                loader: () => import('./Formation')
            })
        },
        // {
        //     path     : '/app/formation/new',
        //     component: FuseLoadable({
        //         loader: () => import('../AddFormation')
        //     })
        // },
        {
            path     : '/app/listformation',
            component: FuseLoadable({
                loader: () => import('../FormationMenu')
            })
        },
        {
            path     : '/app/formation/new',
            component: FuseLoadable({
                loader: () => import('../AddFormation')
            })
        },
        {
            path     : '/app/formation/:formationId/:verify?',
            component: FuseLoadable({
                loader: () => import('../EditFormation')
            })
        },
    ]
};