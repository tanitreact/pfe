import React, {Component} from 'react';
import {TextField, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar} from '@material-ui/core';
import {connect} from 'react-redux';
import _ from '@lodash';

import {closeNewContactDialog,addFormation} from '../../../store/actions/formation/catalogue.action'

const newContactState = {
    training_name    : '',
    training_statut    : '',
};

class FormationDialog extends Component {

    state = {
        ...newContactState
    };

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if ( !prevProps.contactDialog.props.open && this.props.contactDialog.props.open )
        {
            if ( this.props.contactDialog.type === 'new' &&
                !_.isEqual(newContactState, prevState) )
            {
                this.setState({...newContactState});
            }
        }
    }

    handleChange = (event) => {
        this.setState(
            _.set({...this.state}, 
            event.target.name,
            event.target.type === 'checkbox' ? event.target.checked : event.target.value));
    };

    closeComposeDialog = () => {
        this.props.closeNewContactDialog();
    };

    canBeSubmitted()
    {
        const {training_name} = this.state;
        return (
            training_name.length > 0
        );
    }

    render()
    {
        const {contactDialog, addFormation} = this.props;

        return (
            <Dialog
                classes={{
                    paper: "m-24"
                }}
                {...contactDialog.props}
                onClose={this.closeComposeDialog}
                fullWidth
                maxWidth="xs"
            >

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            Nouvelle formation
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-24"}}>

                        <TextField
                            className="mb-24"
                            label="Titre de la formation"
                            autoFocus
                            id="training_name"
                            name="training_name"
                            value={this.state.training_name}
                            onChange={this.handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                        <TextField
                            className="mb-20"
                            label="Statut"
                            id="training_statut"
                            name="training_statut"
                            value={this.state.training_statut}
                            onChange={this.handleChange}
                            variant="outlined"
                            fullWidth
                        />
                </DialogContent>
                    <DialogActions className="justify-between pl-10">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                addFormation(this.state);
                                this.closeComposeDialog();
                            }}
                            disabled={!this.canBeSubmitted()}
                        >
                            Add
                        </Button>
                    </DialogActions>
            </Dialog>
        );
    }
}


function mapDispatchToProps(dispatch)
{
    return {
        // closeEditContactDialog: Actions.closeEditContactDialog,
        closeNewContactDialog:() => dispatch(closeNewContactDialog()),
        addFormation:(formation) => dispatch(addFormation(formation))
        // addContact            : Actions.addContact,
        // updateContact         : Actions.updateContact,
        // removeContact         : Actions.removeContact
    }
}

function mapStateToProps(state)
{
    return {
        contactDialog: state.catalogueReducer.contactDialog
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FormationDialog);
