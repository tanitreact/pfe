import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import {connect} from 'react-redux';
import TextField from '@material-ui/core/TextField';
import _ from '@lodash';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';



const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
      },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    dense: {
      marginTop: 16,
    },
    menu: {
      width: 200,
    },
    button: {
      margin: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    buttonColor: {
      color:'#d50000'
    },
  });
  
  const buttonStyle = {
      marginTop: '20px',
    marginBottom: '20px',
  };


  
  //const {entrepriseId} = params;

class UpdateCatalogue extends Component{

    state = {
        name: '',
      };

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        })
     }

    
     
    render(){
        const { classes } = this.props;
      // console.log('formation',this.props.formation)
      const formation = this.props.formation
        return (
              <Paper className={classes.root} elevation={1}>
                         <h3>Formation</h3>
                  <form className={classes.container} noValidate autoComplete="off">
                     <TextField
                         id="outlined-name"
                         label="Titre de la formation"
                         className={classes.textField}
                         value={formation.training_name}
                         onChange={this.handleChange}
                         margin="normal"
                         variant="outlined"
                         fullWidth
                      />
                      <TextField
                         id="outlined-name"
                         label="statut"
                         className={classes.textField}
                         value={formation.training_statut}
                         onChange={this.handleChange}
                         margin="normal"
                         variant="outlined"
                         fullWidth
                      />
                      <div className={classes.container}>
                        <Button variant="contained" color="secondary" className={classes.button}>
                            Delete
                            <DeleteIcon className={classes.rightIcon} />
                        </Button>
                         <Button variant="contained" color="primary" className={classes.button}>
                                modifier
                          </Button>
                      </div>

                  </form>
              </Paper>
          );
    }
}

UpdateCatalogue.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapDispatchToProps = (dispatch) => {
  return {
      
      // openNewContactDialog:() => dispatch(openNewContactDialog()),
      // formationItem: (training) => dispatch(formationItem(training))
  }
}

function mapStateToProps(state)
{
  console.log('state',state)
  return {
    formation: state.catalogueReducer.formation,
  }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(UpdateCatalogue));