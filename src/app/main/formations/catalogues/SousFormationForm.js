import React, {Component} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar,
    
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import _ from '@lodash';


import {addSubTraining,closeNewSubTraining, removeSubTraining,closeEditSubTraining,updateSubTraining} from '../../../store/actions/formation/sous.formation.actions'


const newTodoState = {
    training_title       : '',
    sub_training_title   : '',
    difficulty:  '',
    duration  : '',
    subTraninigAddId: ''
};

const styles = theme => ({
   
    textField1: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      maxWidth : '300px'
    },
  });

class SousFormationForm extends Component {

    state = {
        ...newTodoState,
        labelMenuEl: null
    };

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        /**
         * After Dialog Open
         */

        if ( !prevProps.todoDialog.props.open && this.props.todoDialog.props.open )
        {
            
            if ( this.props.todoDialog.type === 'edit')
            {
                console.log('if test')
                this.setState({...this.props.subTrainigItem});
            }

            if ( this.props.todoDialog.type === 'new' &&
                !_.isEqual(newTodoState, prevState) )
            {
                console.log('test edit')
                this.setState({
                        ...newTodoState,
                });
            }
        }
    }

    handleChange = (event) => {
        this.setState(
            _.set({...this.state}, 
            event.target.name,
            event.target.type === 'checkbox' ? event.target.checked : event.target.value));
    };

    closeSubTraining = () => {
        this.props.todoDialog.type === 'edit' ? this.props.closeEditSubTraining() : this.props.closeNewSubTraining();
    };

    // handleLabelMenuOpen = (event) => {
    //     this.setState({labelMenuEl: event.currentTarget});
    // };

    // handleLabelMenuClose = (event) => {
    //     this.setState({labelMenuEl: null});
    // };

    // handleToggleImportant = () => {
    //     this.setState({
    //         form: {
    //             ...this.state.form,
    //             important: !this.state.form.important
    //         }
    //     });
    // };

    // handleToggleStarred = () => {
    //     this.setState({
    //         form: {
    //             ...this.state.form,
    //             starred: !this.state.form.starred
    //         }
    //     });
    // };

    // handleToggleLabel = (event, id) => {
    //     event.stopPropagation();
    //     this.setState({
    //         form: _.set({
    //             ...this.state.form,
    //             labels: this.state.form.labels.includes(id) ? this.state.form.labels.filter(labelId => labelId !== id) : [...this.state.form.labels, id]
    //         })
    //     });
    // };

    // toggleCompleted = () => {
    //     this.setState({
    //         form: {
    //             ...this.state.form,
    //             completed: !this.state.form.completed
    //         }
    //     })
    // };

    // canBeSubmitted()
    // {
    //     const {sub_training_title} = this.state.form;
    //     return (
    //         sub_training_title.length > 2
    //     );
    // }
    handleSubmit = () =>{
        console.log(this.state)
        this.props.addSubTraining(this.state)
    }

    handleUpdate = () =>{
        console.log(this.state)
        this.props.updateSubTraining(this.state)
    }

    render()
    {
        const {todoDialog, addTodo, updateTodo, removeTodo, labels,classes} = this.props;
        //console.log(this.props.match.params)
        
        return (
            <Dialog {...todoDialog.props} onClose={this.closeSubTraining} fullWidth maxWidth="sm">

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            {todoDialog.type === 'new' ? 'Nouvelle activité' : 'Modification'}
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-0"}}>      

                    <div className="px-16 sm:px-24">
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Titre formation"
                                name="training_title"
                                id="training_title"
                                value={this.state.training_title}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="thème activité"
                                autoFocus
                                id="sub_training_title"
                                name="sub_training_title"
                                value={this.state.sub_training_title}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl>  
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Durée"
                                name="duration"
                                id='duration'
                                value={this.state.duration}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl> 
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Difficulté"
                                name="difficulty"
                                id='difficulty'
                                value={this.state.difficulty}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl>   
                    </div>

                </DialogContent>

                {todoDialog.type === 'new' ? (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                this.handleSubmit()
                                this.closeSubTraining();
                            }}
                           // disabled={!this.canBeSubmitted()}
                        >
                            Add
                        </Button>
                    </DialogActions>
                ) : (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                //addFormation(this.state);
                                this.handleUpdate()
                                this.props.closeEditSubTraining();
                            }}
                            // disabled={!this.canBeSubmitted()}
                        >
                            Save
                        </Button>
                        <IconButton
                            className="min-w-auto"
                            onClick={() => {
                                removeSubTraining(this.state.subTraninigAddId);
                                this.props.closeEditSubTraining();
                            }}
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
                )}
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return {
        
       // closeEditTodoDialog: Actions.closeEditTodoDialog,
       closeEditSubTraining: () => dispatch(closeEditSubTraining()),
       closeNewSubTraining: () => dispatch(closeNewSubTraining()),
       removeSubTraining: () => dispatch(removeSubTraining()),
       addSubTraining:(subFormation) => dispatch(addSubTraining(subFormation)),
       updateSubTraining:(subFormation) => dispatch(updateSubTraining(subFormation))
       // addTodo            : Actions.addTodo,
       // updateTodo         : Actions.updateTodo,
       // removeTodo         : Actions.removeTodo
    }
}

function mapStateToProps(state)
{
    console.log('Item',state.subTrainingReducer.subTrainigItem)
    return {
       todoDialog: state.subTrainingReducer.todoDialog,
       subTrainigItem : state.subTrainingReducer.subTrainigItem
       // labels    : todoApp.labels
    }
}

SousFormationForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(SousFormationForm));
