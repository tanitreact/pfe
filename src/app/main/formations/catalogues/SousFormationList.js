import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { compose} from 'redux';
import {Input, Icon, Typography, MuiThemeProvider} from '@material-ui/core';
import { firestoreConnect } from 'react-redux-firebase';
import {Link} from 'react-router-dom';

import { handleTakeSubTraining,openEditSubTraining} from '../../../store/actions/formation/sous.formation.actions'

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button : {
    margin : '20px'
  }
});




class SousFormationList extends Component{

  render(){
    const { classes} = this.props;
    const   sousformations = this.props.sousformations || []

    return(
     <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <CustomTableCell>Activités</CustomTableCell>
            <CustomTableCell align="right"></CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sousformations.map(row => (
            <TableRow className={classes.row} key={row.id}>
              <CustomTableCell component="th" scope="row">
                {row.sub_training_title}
              </CustomTableCell>
              <CustomTableCell align="right">
                <Button
                   variant="contained"
                   color="primary"
                   onClick={()=>{
                     this.props.handleTakeSubTraining(row)
                     this.props.openEditSubTraining()
                   }}
                 >
                  <i class="fas fa-edit"></i>
                </Button>
                {/* <Button 
                   component={Link} 
                   color="secondary"
                   to="/apps/gestion-session/session/new" 
                   className={classes.button} 
                   variant="contained"
                >
                    <i class="fas fa-book" color='red'>session</i>
                </Button> */}
              </CustomTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    )
  }
}

SousFormationList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  //console.log('sous formation',state.firestore.ordered.sousformation)
     return {
        sousformations:state.firestore.ordered.sousformations
     }
 }

const mapDispatchToProps = (dispatch) => {
  return {

    handleTakeSubTraining:(subTraining) =>(dispatch(handleTakeSubTraining(subTraining))),
    openEditSubTraining:() =>(dispatch(openEditSubTraining()))
    //removeEntreprise: (id) => dispatch(removeEntreprise(id)),
  }}

export default compose(
  connect(mapStateToProps,mapDispatchToProps),
  firestoreConnect([
    { collection: 'sousformations'},
  ]))(withStyles(styles)(SousFormationList));

//export default withStyles(styles)(SousFormationList);