import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';

import UpdateCatalogue from '../catalogues/UpdateCatalogue'
import SousFormationList from './SousFormationList'
const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop:'64px'
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  media: {
    height: '230px',
    paddingTop: '56.25%', // 16:9
  },
  card: {
    maxWidth: 400,
  },
});

class Formation extends Component {
 

    render(){
        const { classes } = this.props;
        return (
            <div className={classes.root}>
              <Grid container spacing={24}>
                <Grid item xs={4}>
                <Paper  className={classes.paper}>
                   <CardMedia
                     className={classes.media}
                     image="http://fhwb.org.uk/wp-content/uploads/2019/01/training-768x513.png"
                     title="Paella dish"
                   />
                  </Paper>
                </Grid>
                <Grid item xs={8}>
                  <UpdateCatalogue />
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                             <SousFormationList />
                    </Paper>
                </Grid>
              </Grid>
            </div>
          );
    }
}

Formation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Formation);