import React from 'react';
import {FusePageCarded} from '@fuse';

import FormationHeader from './FormationHeader'
import Formation from './Formation'
import SousFormationForm from './SousFormationForm';

const Formations = () => {
    return (
    <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <FormationHeader/>
            }
            content={
                <Formation />
            }
            innerScroll
        />
        <SousFormationForm />
     </React.Fragment>
    );
};

export default Formations;
