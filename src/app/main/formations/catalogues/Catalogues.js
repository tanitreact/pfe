import React, {Component} from 'react';
import {withStyles, Typography, Fab,Icon} from '@material-ui/core';
import {fade} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimateGroup, FuseAnimate} from '@fuse';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import classNames from 'classnames';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

import {openNewContactDialog,formationItem} from '../../../store/actions/formation/catalogue.action'
import FormationDialog from './FormationDialog'
    
//import Session from '../sessions/Session'


const styles = theme => ({
    root    : {
        background: 'white',
        color     : theme.palette.getContrastText(theme.palette.primary.main)
    },
    board   : {
        cursor                  : 'pointer',
        boxShadow               : theme.shadows[0],
        transitionProperty      : 'box-shadow border-color',
        transitionDuration      : theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        background              : theme.palette.primary.dark,
        color                   : theme.palette.getContrastText(theme.palette.primary.dark),
        '&:hover'               : {
            boxShadow: theme.shadows[6]
        }
    },
    newBoard: {
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: fade(theme.palette.getContrastText(theme.palette.primary.main), 0.6),
        '&:hover'  : {
            borderColor: fade(theme.palette.getContrastText(theme.palette.primary.main), 0.8)
        }
    }
});


class Catalogues extends Component {


    render()
    {
        const {classes} = this.props;
       const formations= this.props.formations || []
        
        return (
            <div className={classNames(classes.root, "flex flex-grow flex-no-shrink flex-col items-center")}>

                <div className="flex flex-grow flex-no-shrink flex-col items-center container px-16 md:px-24">

                    <div>
                        <FuseAnimateGroup
                            className="flex flex-wrap w-full justify-center py-32 px-16"
                        >
                            { formations.map(training => (
                                <div className="w-224 h-224 p-16" key={training.formationId}>
                                    <Link
                                       onClick={()=>{
                                        //console.log(entreprise.id)
                                        this.props.formationItem(training)
                                           }
                                         }
                                        
                                        to={'/apps/formation' + '/'+training.formationId}
                                        className={classNames(classes.board, "flex flex-col items-center justify-center w-full h-full rounded py-24")}
                                        role="button"
                                    >
                                        <Icon className="text-56">assessment</Icon>
                                        <Typography 
                                          className="text-16 font-300 text-center pt-16 px-32"
                                           color="inherit">
                                           {training.training_name}
                                        </Typography>
                                    </Link>
                                </div>
                            ))}
                            <FuseAnimate animation="transition.expandIn" delay={300}>
                            <div className="w-224 h-224 p-16">
                                <div
                                    className={classNames(classes.board, classes.newBoard, "flex flex-col items-center justify-center w-full h-full rounded py-24")}
                                    onClick={this.props.openNewContactDialog}
                                >
                                    <Icon className="text-56">add_circle</Icon>
                                    <Typography
                                       className="text-16 font-300 text-center pt-16 px-32" 
                                       color="inherit">
                                          Ajouter une formation
                                    </Typography>
                                </div>
                            </div>

                         </FuseAnimate>
                        </FuseAnimateGroup>
                    </div>
                    <FormationDialog/>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        
        openNewContactDialog:() => dispatch(openNewContactDialog()),
        formationItem: (training) => dispatch(formationItem(training))
    }
}

function mapStateToProps(state)
{
    return {
        formations: state.firestore.ordered.formations
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
      { collection: 'formations'},
]))(withStyles(styles,{withTheme: true})(Catalogues));