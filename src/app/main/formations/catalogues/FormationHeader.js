import React, {Component} from 'react';
import {Paper, Button, Input, Icon, Typography, MuiThemeProvider} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import { renderComponent } from 'recompose';

import {openNewSubTraining} from '../../../store/actions/formation/sous.formation.actions'



class FormationHeader extends Component {


    state = {
        formOpen: false
    };

    handleFormOpen = () => {
        this.setState({formOpen: true});
        document.addEventListener("keydown", this.escFunction, false);
    };

    handleFormClose = () => {
        if ( !this.state.formOpen )
        {
            return;
        }
        this.setState({formOpen: false});
        document.removeEventListener("keydown", this.escFunction, false);
    };

    handleCreate = (note) => {
        this.props.createNote(note);
        this.handleFormClose();
    };

    escFunction = (event) => {
        if ( event.keyCode === 27 )
        {
            this.handleFormClose();
        }
    };
    handleClickAway = (ev) => {
        const preventCloseElements = document.querySelector(".prevent-add-close");
        const preventClose = preventCloseElements ? preventCloseElements.contains(ev.target) : false;
        if ( preventClose )
        {
            return;
        }
        this.handleFormClose();
    };



     render(){
        return (
            <div className="flex flex-1 w-full items-center justify-between">
    
                <div className="flex items-center">
                    <FuseAnimate animation="transition.expandIn" delay={300}>
                        <Icon className="text-32 mr-0 sm:mr-12">shopping_basket</Icon>
                    </FuseAnimate>
                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                    
                        <Typography className="hidden sm:flex" variant="h6">Nom formation</Typography>
                    </FuseAnimate>
                </div>
    
                <div className="flex flex-1 items-center justify-center px-12">
    
                </div>
                <FuseAnimate animation="transition.slideRightIn" delay={300}>
                    <Button
                       onClick={this.props.openNewSubTraining}
                       className="whitespace-no-wrap" 
                       variant="contained"
                       
                    >
                        <span className="hidden sm:flex">Ajouter activité</span>
                    </Button>
                </FuseAnimate>
            </div>
        );
     }
};

function mapDispatchToProps(dispatch)
{
    return {
        openNewSubTraining: () => dispatch(openNewSubTraining()),
    }
}

function mapStateToProps({eCommerceApp, fuse})
{
    return {
        // searchText: eCommerceApp.products.searchText,
        // mainTheme : fuse.settings.mainTheme
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormationHeader);
