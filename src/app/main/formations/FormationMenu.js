import React from 'react';
import {FusePageCarded} from '@fuse';

import FormationDialog from './FormationDialog'
import FormationHeader from './FormationHeader';
import ListFormations from './ListFormations';
import {connect} from 'react-redux';
import { compose} from 'redux';

const FormationMenu = () => {
 return (
    <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
              <FormationHeader />
            }
            content={
               <ListFormations />
            }
            innerScroll
        />
        <FormationDialog />
     </React.Fragment>
    );
};

function mapStateToProps(state)
{  
    return {
          user: state.auth.user,
          auth : state.firebase.auth
    }
}


export default compose(
    connect(mapStateToProps),
)(FormationMenu);
//export default FormationMenu;