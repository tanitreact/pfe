import React, {Component} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar,
    InputAdornment
} from '@material-ui/core';
import {connect} from 'react-redux';
import moment from 'moment/moment';
import _ from '@lodash';
import {closeEditFormation,removeFormation} from '../../store/actions/formation/catalogue.action'
//import * as Actions from './store/actions';

const initialState = {
    formOpen : false,
    image: "",
    tabChapitre: [],
    title: ' ',
    description : ''
};
class FormationDialog extends Component {

    state = {
        ...initialState   
     };


    componentDidUpdate(prevProps, prevState, snapshot){ 
         if ( !prevProps.formationDialog.props.open && this.props.formationDialog.props.open )
         {
             console.log('upadate')
                 this.setState({...this.props.formationItem})
         }
     }
    
    handleChangeTab = (event, tabValue) => {

        this.setState({tabValue});
    }


    addAchapitre = (event) =>{

        this.setState({
            tabChapitre : [...this.state.tabChapitre, '']
        })
    }

    handleChangeChapitre = (event,index) =>{

        this.state.tabChapitre[index] = event.target.value
        this.setState({
            tabChapitre : this.state.tabChapitre
        })
    }

    // handleChange = (event) => {
    //     this.setState({
    //       [event.target.id]: event.target.value
    // })}

    handleChange = (event) => {
        this.setState(
            _.set({...this.state}, 
            event.target.name,
            event.target.type === 'checkbox' ? event.target.checked : event.target.value));
    };
    // handleChange = (event) => {
    //      _.set({...this.state}, event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    //     this.setState({form});
    // };

    closeTodoDialog = () => {
         this.props.closeEditFormation() 
    };

    handleCloseForm = (index) => {
        this.state.tabChapitre.splice(index,1)
        this.setState({
            formOpen : false,
            tabChapitre : this.state.tabChapitre
        })
    };

    handleOpenForm = () => {
        this.setState({formOpen: true})
    };
    render()
    {
        const {formationDialog, addTodo, updateTodo, removeFormation, formationItem} = this.props;
        const {form, labelMenuEl} = this.state;
        var tabChapitre = formationItem.tabChapitre || []
        

        return (
            <Dialog {...formationDialog.props} onClose={this.closeTodoDialog} fullWidth maxWidth="sm">

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            Modifier la formation
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-0"}}>

                    <div className="px-16 sm:px-24">
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Titre formation"
                                name="title"
                                id="title"
                                value={this.state.title}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                            />
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Description"
                                name="notes"
                                multiline
                                rows="6"
                                value={this.state.description}
                                onChange={this.handleChange}
                                variant="outlined"
                            />
                        </FormControl>
                        {
                        // this.state.tabChapitre.map((n) => {
                        //   return(
                        //     <FormControl className="mt-8 mb-16" required fullWidth>
                        //          <TextField
                        //            label="Chapitre "            
                        //            name="title"
                        //            value={n}
                        //            onChange={this.handleChange}
                        //            required
                        //            variant="outlined"
                        //           />
                        //     </FormControl>
                        //    )
                        //   } )

                          this.state.tabChapitre.map((chapitre,index)=>{
                            console.log('index',index)
                            return(
                                //<div key={index}>
                               
                                    <FormControl className="mt-8 mb-16" required fullWidth>
                                       <TextField
                                          required
                                          label="Chapitre"
                                          id="name"
                                          name="name"
                                          value={chapitre}
                                          onChange={(event)=>{this.handleChangeChapitre(event,index)}}
                                          variant="outlined"
                                          InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton onClick={()=>this.handleCloseForm(index)}>
                                                        <Icon className="text-18">close</Icon>
                                                    </IconButton>
                                                </InputAdornment>
                                            )
                                        }}
                                        />
                                     </FormControl>
                              //  </div>
                            )
                            })
                       }  
                       </div>
                       <Button 
                       variant="outlined" 
                       size="small" 
                       color="primary" 
                      // className={classes.margin}
                       onClick={(event)=>{
                           this.addAchapitre(event)
                       }}
                    >
                          Ajoute chapitre
                    </Button>
                    

                </DialogContent>

                <DialogActions className="justify-between pl-8 sm:pl-16">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            updateTodo(this.state.form);
                            this.closeTodoDialog();
                        }}
                    >
                         Save
                    </Button>
                    <IconButton
                        className="min-w-auto"
                        onClick={() => {
                          removeFormation(formationItem.formationId);
                          this.closeTodoDialog();
                        }}
                    >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
            
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return{
        
        closeEditFormation:() =>(dispatch(closeEditFormation())),
         removeFormation:(id) => (dispatch(removeFormation(id)))
     }
}

function mapStateToProps(state)
{

    return {
        formationDialog: state.catalogueReducer.formationDialog,
       formationItem : state.catalogueReducer.formationItem
       // labels    : todoApp.labels
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormationDialog);
