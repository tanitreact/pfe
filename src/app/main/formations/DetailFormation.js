import React, {Component} from 'react'
import {withRouter} from 'react-router-dom';
import {Icon, Input, Paper, Typography,withStyles} from '@material-ui/core';
import {FuseAnimate} from '@fuse';   
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import { connect } from 'react-redux';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import ListItem from '@material-ui/core/ListItem';
import classNames from 'classnames';
import {Redirect} from 'react-router-dom';
import _ from '@lodash';
import LinearProgress from '@material-ui/core/LinearProgress';

import {getCourse} from '../../store/actions/formation/catalogue.action'




const styles = theme => ({
    header    : {
        //background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + theme.palette.primary.main + ' 100%)',
       // color     : theme.palette.getContrastText(theme.palette.primary.main),
       // backgroundImage: "url(https://images.pexels.com/photos/1308624/pexels-photo-1308624.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"
    },
    headerIcon: {
        position     : 'absolute',
        top          : -64,
        left         : 0,
        opacity      : .04,
        fontSize     : 512,
        width        : 512,
        height       : 512,
        pointerEvents: 'none'
    },
    linearProgress:{
        flexGrow: 1,
    }
})

// const  subtitle={
//     backgroundColor : '#005074'
// }


class DetailFormation extends Component{

   state={
       course :{},
       condition : false
   }

   componentDidMount()
   {
       this.getCourseId()
       //console.log('gggggggggggggg')
   }

   componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (!_.isEqual(this.props.location,prevProps.location))
        {
           this.getCourseId();
        }
    }

    getCourseId = () =>{
     
        const params = this.props.match.params;
        const {courseId} = params;
      
        this.props.getCourse(courseId)
   }
    
   render(){


      const course = this.props.courseItem || ''
      const chapitres = course.chapitres || []
      const {classes,auth} = this.props;

    if( course === ''){
            return(
                <div 
                   delay={4000}
                   className={classes.linearProgress}
                >
                    <LinearProgress color="primary" variant="query" />
                </div>
            )
    }
    return (
        
      <div className="flex flex-col flex-1  p-5" delay={3000}>   
            <Typography variant="h4" className=" mt-30 mb-20" delay={3000}>Description</Typography>
                <Typography paragraph className='text-left'>
                     {course.description}
                </Typography>
             
            <Typography variant="h4" className=" mt-30 mb-20">Contenu de la formation</Typography>  
              {
                 chapitres.map((n) => {   
                    return(
                        <Typography paragraph className='text-left'>
                            <i className={classNames("inline-block w-8 h-8 rounded ml-8")}/>
                               {n}
                        </Typography>
                        // <ListItem >
                        //        <ListItemText className='text-left' inset primary={n}/>
                        // </ListItem>
                    )
                 } )
              }
              
          
      </div>
   );
  }
};


const mapStateToProps = (state) => {
    return {
        formations: state.firestore.ordered.formations,
        courseItem: state.catalogueReducer.courseItem,
        auth : state.firebase.auth
        
    }
}

function mapDispatchToProps(dispatch)
{
    return {
        getCourse: (id) => dispatch(getCourse(id)),
    };
}

export default compose(
    withRouter,
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'formations'},
]))(withStyles(styles)(DetailFormation))

//export default DetailFormation;