import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { compose} from 'redux';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';

import {handleTakeUser,openEditUser} from '../../store/actions/users/users.action'
// import {
//   removeEntreprise,
//   getAllEntreprises,
//   UpadateSearchEntreprise,
//   getAllEntreprisesSuccess,
//    EditEntrepriseItem
//   } from '../../../store/actions/entreprise/entreprise.action'


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    marginRight :theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 300,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
});



class ListFormation extends Component {
  
    // componentDidMount()
    // {
    //     this.props.getAllEntreprises();
    // }

  //   componentDidUpdate(prevProps, prevState)
  //   {
  //       if ( !_.isEqual(this.props.allEntreprises, prevProps.allEntreprises) || !_.isEqual(this.props.searchText, prevProps.searchText) )
  //       {
  //           const data = this.getFilteredArray(this.props.allEntreprises, this.props.searchText);
  //           this.setState({data})
  //       }
  //   }

  //   getFilteredArray = (data, searchText) => {
  //     if ( searchText.length === 0 )
  //     {
  //         return data;
  //     }
  //     return _.filter(data, item => item.name.toLowerCase().includes(searchText.toLowerCase()));
  // };


  handleClick = (item) => {
     //this.props.EditEntrepriseItem(item)
    };
  


  render(){
    const { classes,auth} = this.props;
    const users= this.props.users || []
   
    if(!auth.uid){return(<Redirect to='login'/>)}
    
    return (
        <div className='className="p-16 sm:p-24s"'>

<Paper className={classes.root} delay={1000}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <CustomTableCell>Titre de la formation</CustomTableCell>
                <CustomTableCell align="right"></CustomTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map(user => (
                <TableRow className={classes.row} key={user.id}>
                  <CustomTableCell component="th" scope="entreprise">
                    {user.displayName}
                  </CustomTableCell>
                  <CustomTableCell align="right">
                   <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     onClick={()=>{
                      this.props.handleTakeUser(user)
                      this.props.openEditUser()
                     }}
                   >
                       <i class="fas fa-edit"></i>
                   </Button>
                  </CustomTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        </div>
        
      );
  }
}

ListFormation.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
 
    return {
        users: state.firestore.ordered.users,
        auth : state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      handleTakeUser:(user) =>(dispatch(handleTakeUser(user))),
      openEditUser:() =>(dispatch(openEditUser()))
    }}

export default compose(
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'users'},
    ]))(withStyles(styles)(ListFormation));

