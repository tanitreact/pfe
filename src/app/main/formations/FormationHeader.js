import React from 'react';
import {Paper, Button, Input, Icon, Typography, MuiThemeProvider} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
//import * as Actions from '../store/actions';

const FormationHeader = ({setSearchText, searchText, mainTheme}) => {

    return (
        <div className="flex flex-1 w-full items-center justify-between">

            <div className="flex items-center" > 
             <Button component={Link} to="/apps/academy/courses" className="whitespace-no-wrap">
                <FuseAnimate animation="transition.expandIn" delay={300}>
                    <Icon className="text-32 mr-0 sm:mr-12">bookmarks</Icon>
                </FuseAnimate>
                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                    <Typography className="hidden sm:flex" variant="h6">formations</Typography>
                </FuseAnimate>
             </Button>
            </div>

            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                <Button component={Link} to="/app/formation/new" className="whitespace-no-wrap" variant="contained">
                    <span className="hidden sm:flex">Ajouter formation</span>
                    <span className="flex sm:hidden">Nouvelle</span>
                </Button>
            </FuseAnimate>
        </div>
    );
};

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
       // setSearchText: Actions.setProductsSearchText
    }, dispatch);
}

function mapStateToProps({eCommerceApp, fuse})
{
    return {
        // searchText: eCommerceApp.products.searchText,
        // mainTheme : fuse.settings.mainTheme
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormationHeader);
