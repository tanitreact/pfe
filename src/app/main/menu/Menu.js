import React, {Component} from 'react';
import {withStyles, Card, CardContent, Icon, Typography, Menu} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import classNames from 'classnames';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { compose} from 'redux';
import {Link} from 'react-router-dom';
import {Redirect} from 'react-router-dom';

const styles = theme => ({
    root: {
        background: 'radial-gradient(' + darken(theme.palette.primary.dark, 0.5) + ' 0%, ' + theme.palette.primary.dark + ' 80%)',
        color     : theme.palette.primary.contrastText
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
})


class MenuPage extends Component {

    render()
    {
        const {classes,user,auth} = this.props;

        // if(!auth.uid){return(<Redirect to='login'/>)}
        // console.log('test tes menu')

        return (
         <div className={classNames(classes.root, "flex flex-row flex-auto flex-no-shrink items-center  justify-center p-32")} delay={1000}>      
     
            <div className="flex flex-col items-center justify-center w-full">
               <Link className="font-medium" to="/app/users">
                    <FuseAnimate animation="transition.expandIn">
                        <Card className="max-w-384">
                            <CardContent className="flex flex-col items-center justify-center p-20">
                                <div className="m-10">
                                    <Icon className="text-96" color="action">account_box</Icon>
                                </div>
                                <Typography variant="h5" className="text-center mb-16">Gestion des utilisateurs</Typography>
                            </CardContent>
                        </Card>
                    </FuseAnimate>
                </Link>
            </div>
                
            <div className="flex flex-col items-center justify-center w-full">
                <Link className="font-medium" to="/apps/academy/courses"  variant="contained">
                    <FuseAnimate animation="transition.expandIn">
                        <Card className="max-w-384">
                            <CardContent className="flex flex-col items-center justify-center p-20">
                                <div className="m-10">
                                    <Icon className="text-96" color="action">book</Icon>
                                </div>
                                <Typography variant="h5" className="text-center mb-16">Gestion des formations</Typography>
                            </CardContent>
                        </Card>
                    </FuseAnimate>
                    </Link>
            </div>
           
        </div>
           
        );
    }
}
function mapStateToProps(state)
{  
    return {
          user: state.auth.user,
          auth : state.firebase.auth
    }
}


export default compose(
    withRouter,
    connect(mapStateToProps),
)(withStyles(styles,{withTheme: true})(MenuPage));

//export default withStyles(styles, {withTheme: true})(MenuPage);
