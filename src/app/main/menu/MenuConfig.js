import MenuPage  from './Menu';
import authRoles from '../../auth/authRoles'

export const MenuConfig = {
    
        settings: {
            layout: {
                config: {
                    navbar        : {
                        display : false,
                        folded  : false,
                        position: 'left'
                    },
                    toolbar       : {
                        display : false,
                        style   : 'fixed',
                        position: 'below'
                    },
                    footer        : {
                        display : false,
                        style   : 'fixed',
                        position: 'below'
                    },
                    leftSidePanel : {
                        display: false
                    },
                    rightSidePanel: {
                        display: false
                    }
                }
            }
        },
    auth    : authRoles.admin,
    routes  : [
        {
            path     : '/app/menu',
            component: MenuPage 
        }
    ]
};


