// import React from 'react';
import {FuseLoadable} from '@fuse';
//import {Redirect} from 'react-router-dom';



export const SessionConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display : true,
                    folded  : false,
                    position: 'left'
                },
                toolbar       : {
                    display : true,
                    style   : 'fixed',
                    position: 'below'
                },
                footer        : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                leftSidePanel : {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },

    routes  : [
        {
            path     : '/apps/sessions',
            component: FuseLoadable({
                loader: () => import('./Sessions')
            })
        },
        {
            path     : '/apps/add/session',
            component: FuseLoadable({
                loader: () => import('./AddSession')
            })
        },
        {
            path     : '/apps/session/:sessionId',
            component: FuseLoadable({
                loader: () => import('./EditSession')
            })
        },
        // {
        //     path     : '/apps/formation/:tranningId',
        //     component: FuseLoadable({
        //         loader: () => import('../catalogues/Formations')
        //     })
        // },
        // {
        //     path     : '/apps/formation/:formationId',
        //     component: FuseLoadable({
        //         loader: () => import('./Formation')
        //     })
        // },
        // {
        //     path     : '/app/ajoouterFormation',
        //     component: FuseLoadable({
        //         loader: () => import('../AddFormation')
        //     })
        // },
        // {
        //     path     : '/app/listformation',
        //     component: FuseLoadable({
        //         loader: () => import('../FormationMenu')
        //     })
        // },
    ]
};