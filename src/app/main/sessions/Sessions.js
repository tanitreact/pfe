import React from 'react';
import {FusePageCarded} from '@fuse';
//import withReducer from 'app/store/withReducer';
import SessionList from './SessionList';
import SessionHeader from './SessionHeader';
//import reducer from '../store/reducers';
import AddSession from './AddSession'
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { compose} from 'redux';
import {Redirect} from 'react-router-dom';


const Sessions = (props) => {

 

    return (
    <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <SessionHeader/>
            }
            content={
                <SessionList/>
            }
            innerScroll
        />
        <AddSession/>
    
    </React.Fragment>
    );
};


function mapStateToProps(state)
{
    return {
          //user: state.auth.user,
          auth : state.firebase.auth
    }
}


export default compose(
    connect(mapStateToProps),
)(Sessions);
//export default Sessions;
