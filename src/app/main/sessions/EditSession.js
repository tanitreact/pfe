import React, {Component} from 'react';
import {withStyles, Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography,    FormControl,} from '@material-ui/core';
import {FuseAnimate, FusePageCarded, FuseChipSelect} from '@fuse';
import {orange} from '@material-ui/core/colors';
import {Link, withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import connect from 'react-redux/es/connect/connect';
import classNames from 'classnames';
import {FuseUtils} from '@fuse';
import moment from 'moment/moment';
import _ from '@lodash';
//import withReducer from 'app/store/withReducer';
//import * as Actions from '../store/actions';
//import reducer from '../store/reducers';
import AddQuestion from './quiz.sessions/AddQuestion'
import ListQuestion from './quiz.sessions/ListQuestion'
import {getSession} from '../../store/actions/session/session.action'

import ListeParticipant from './ListeParticipant'


const styles = theme => ({
    productImageFeaturedStar: {
        position: 'absolute',
        top     : 0,
        right   : 0,
        color   : orange[400],
        opacity : 0
    },
    productImageItem        : {
        transitionProperty      : 'box-shadow',
        transitionDuration      : theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover'               : {
            boxShadow                    : theme.shadows[5],
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured'            : {
            pointerEvents                      : 'none',
            boxShadow                          : theme.shadows[3],
            '& $productImageFeaturedStar'      : {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
});



class EditSession extends Component {
    state = {
        tabValue: 0,
   
        form  :{
              id              : FuseUtils.generateGUID(),
            name           : '',
            theme           : ' ',
            handle          : '',
            description     : '',
            categories      : [],
            tags            : [],
            images          : [],
            priceTaxExcl    : 0,
            priceTaxIncl    : 0,
            taxRate         : 0,
            comparedPrice   : 0,
            quantity        : 0,
            sku             : '',
            width           : '',
            height          : '',
            depth           : '',
            weight          : '',
            extraShippingFee: 0,
            active          : true
            }
    };

    componentDidMount()
    {
        this.getSessioneId()
       // this.setState({...this.props.session});
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (!_.isEqual(this.props.location,prevProps.location))
        {
           this.getSessioneId();
           this.setState({...this.props.session});
        }
       
    }

    getSessioneId = () =>{
        const params = this.props.match.params;
        const {sessionId} = params;
      
          this.props.getSession(sessionId)

        if(!sessionId && sessionId !== undefined){
            console.log('theme')
        }else{
        //console.log('themedddddddddd')
        }
    }

    handleChangeTab = (event, tabValue) => {
        this.setState({tabValue});
    };

    handleChange = (event) => {
        this.setState({...this.props.session});
    };

    handleChipChange = (value, name) => {
        this.setState({form: _.set({...this.state.form}, name, value.map(item => item.value))});
    };

    setFeaturedImage = (id) => {
        this.setState({form: _.set({...this.state.form}, 'featuredImageId', id)});
    };

    canBeSubmitted()
    {
        const {name} = this.state.form;
        return (
            name.length > 0 &&
            !_.isEqual(this.props.product.data, this.state.form)
        );
    }

    render()
    {
        const {classes, saveProduct,session} = this.props;
        const {tabValue, form} = this.state;

        const {todoDialog} = this.props
        const {labelMenuEl} = this.state
        const formations = this.props.formations || []
        
        return (
        //   <React.Fragment>
            <FusePageCarded
                classes={{
                    toolbar: "p-0",
                    header : "min-h-60 h-60 sm:h-116 sm:min-h-116"
                }}
                header={
                    form && (
                        <div className="flex flex-1 w-full items-center justify-between">
                            <div className="flex flex-col items-start max-w-full">
                            </div>
                            
                        </div>
                    )
                }
                contentToolbar={
                    <Tabs
                        value={tabValue}
                        onChange={this.handleChangeTab}
                        indicatorColor="secondary"
                        textColor="secondary"
                        variant="scrollable"
                        scrollButtons="auto"
                        classes={{root: "w-full h-64"}}
                    >
                        <Tab className="h-64 normal-case" label="Liste des inscrits"/>
                        <Tab className="h-64 normal-case" label="Ajouter question"/>
                        <Tab className="h-64 normal-case" label="Gestion des presences"/>
                        <Tab className="h-64 normal-case" label="Liste des questions"/>
                        <Tab className="h-64 normal-case" label="Resultat quiz"/>
                    </Tabs>
                }
                content={
                    form && (
                        <div className="p-16 sm:p-24 max-w-2xl">
                            
                            {tabValue === 0 && (
                                <ListeParticipant />
                            )}
                            {tabValue === 1 && (
                                <AddQuestion/>
                            )}
                            {tabValue === 2 && (
                               <div>
                               <p>Gestion presences</p>
                           </div>
                            )}
                            {tabValue === 3 && (
                                 <ListQuestion/>
                            )}
                            {tabValue === 4 && (
                                <div>
                                    <p>Resultat des quiz</p>
                                </div>
                            )}
                        </div>
                    )
                }
                innerScroll
            />
         
        )
        
    };
}

function mapDispatchToProps(dispatch)
{
    return {
        getSession:(session) => (dispatch(getSession(session)))
    };
}

function mapStateToProps(state)
{
    //console.log('sessionnnnnnnn',state.sessionReducer.sessionItem)
    return {
        session: state.sessionReducer.sessionItem,
    }
}

export default withStyles(styles, {withTheme: true})(withRouter(connect(mapStateToProps, mapDispatchToProps)(EditSession)));
