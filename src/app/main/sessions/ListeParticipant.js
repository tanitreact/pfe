import React, {Component} from 'react';
import {Avatar, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Icon, Tab, Tabs, Tooltip, Typography} from '@material-ui/core';
import {FuseAnimate, FusePageCarded} from '@fuse';
import {Link, withRouter} from 'react-router-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import connect from 'react-redux/es/connect/connect';
import {bindActionCreators} from 'redux';
import GoogleMap from 'google-map-react';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles } from '@material-ui/core/styles'

import {getSession} from '../../store/actions/session/session.action'




class ListeParticipant extends Component {

    state = {
        tabValue: 0,
        form    : null,
        map     : 'shipping'
    };

    componentDidMount()
    {
       // this.props.getOrder(this.props.match.params);
    }

    handleChangeTab = (event, tabValue) => {
        this.setState({tabValue});
    };

    handleTakeSession = () =>{
        const params = this.props.match.params;
        console.log('test',params)
        const {sessionId} = params;

        this.props.getSession(sessionId)
    }

    render()
    {
        
        const users = this.props.users || []
        const session = this.props.sessionItem || ''
        const participants = session.participant || []
        const {tabValue} = this.state;

        return (
             <div className="p-6 sm:p-5 max-w-2xl w-full">
                <table className="simple">
                    <thead>
                        <tr>
                          <th>Nom</th>
                          <th>Email</th>
                        </tr>
                    </thead>                                                                  
                    <tbody>
                        {
                            participants.map((n) => {
                             return users.map((k)=>{
                                 return(
                              <tr>
                                <td>
                                    <Typography className="truncate">
                                        {k.name}
                                    </Typography>
                                 </td>
                                 <td>
                                     <Typography className="truncate">{k.email_user}</Typography>
                                 </td>
                             </tr>    
                                 )
                             })                                                                                                   
                            }
                           )
                        }
                    </tbody>
                </table>
             </div>   
             
        )
    }
}

function mapDispatchToProps(dispatch)
{
    return ({
        getSession:(id) =>(dispatch(getSession(id)))
    })
}

function mapStateToProps(state)
{
    console.log('participant',state.firestore.ordered.users)
    return { 
        sessionItem : state.sessionReducer.sessionItem,
        users: state.firestore.ordered.users,
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'users'},
]))(ListeParticipant)

//</div>export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListeParticipant));
