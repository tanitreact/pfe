import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import LinearProgress from '@material-ui/core/LinearProgress';
import {Redirect} from 'react-router-dom';
import { TablePagination, Checkbox, IconButton,
  Menu,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,} from '@material-ui/core';

// import {handleTakeUser,openEditUser} from '../../store/actions/users/users.action'
import {handleDeleteSession} from '../../store/actions/session/session.action'
// import {
//   removeEntreprise,
//   getAllEntreprises,
//   UpadateSearchEntreprise,
//   getAllEntreprisesSuccess,
//    EditEntrepriseItem
//   } from '../../../store/actions/entreprise/entreprise.action'


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
  actionsButtonWrapper: {
    background: theme.palette.background.paper
}
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    //marginTop: theme.spacing.unit * 2,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    width: '100%',
  },
  row: {
     
      backgroundColor: theme.palette.background.default,
    
  },
  iconEdit:{
    color:'green'
  },
  iconDelete:{
    color : 'red'
  },
  linearProgress:{
    flexGrow: 1,
},
tableTitre:{
   height: '64px',
},
});



class SessionList extends Component {
  
    // componentDidMount()
    // {
    //     this.props.getAllEntreprises();
    // }

  //   componentDidUpdate(prevProps, prevState)
  //   {
  //       if ( !_.isEqual(this.props.allEntreprises, prevProps.allEntreprises) || !_.isEqual(this.props.searchText, prevProps.searchText) )
  //       {
  //           const data = this.getFilteredArray(this.props.allEntreprises, this.props.searchText);
  //           this.setState({data})
  //       }
  //   }

  //   getFilteredArray = (data, searchText) => {
  //     if ( searchText.length === 0 )
  //     {
  //         return data;
  //     }
  //     return _.filter(data, item => item.name.toLowerCase().includes(searchText.toLowerCase()));
  // };

  state = {
    selectedMenu: null
  }

  openSelectedProductsMenu = (event) => {
        
    this.setState({selectedMenu: event.currentTarget});
};

closeSelectedProductsMenu = (event) => {
  //event.preventDefault()
    this.setState({selectedMenu: null});
};
  
  handleClick = (item) => {
    console.log('history',this.props.history)
    this.props.history.push('/apps/session/' + item.id );
  };

  render(){
    const { classes} = this.props;
const {selectedMenu} =this.state
        const sessions = this.props.sessions 
        const formations = this.props.formations || []
        let formationItem 
        
    // if(!this.props.auth || this.props.auth.auth.emailVerified === false){
    //   return(
    //     <Redirect to='/login' />
    //   )
    // }

    if( sessions===undefined){
      return(
          <div 
             delay={1000}
             className={classes.linearProgress}
          >
              <LinearProgress color="primary" variant="query" />
          </div>
      )
     }
    return (
        <Paper className={classes.root} delay={1000}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow className={classes.tableTitre}> 
              <CustomTableCell align="right"></CustomTableCell>
                <CustomTableCell>Titre session</CustomTableCell>
                <CustomTableCell align="right=">Formation</CustomTableCell>
                <CustomTableCell align="right=">Formateur</CustomTableCell>
                <CustomTableCell align="right=">Date debut formation</CustomTableCell>
                <CustomTableCell  align="right=">Lieu</CustomTableCell>
                <CustomTableCell align="left">Nombre de place maximun</CustomTableCell>
                <CustomTableCell align="left">Nombre d'inscrits</CustomTableCell>

              </TableRow>
            </TableHead>
            <TableBody>
              {sessions.length==0 ?
                 ( 
                    <p color="textSecondary" className='with-500'>Aucune session</p>
                 ):
                 ('')
              }
              {sessions.map(n => (
                // const  mydate = new Date('2014-04-03');
                // console.log(mydate.toDateString());
                <TableRow className={classes.row} key={n.id}  hover>
                  <CustomTableCell align="left">
                   {/* <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     onClick={()=>{
                      this.handleClick(n)
                    //  this.props.openEditUser()
                     }}
                   >
                       <Icon className={classes.iconEdit}>edit</Icon>
                   </Button>
               

                   <Button  
                     size="small" 
                     color="red"
                     className={classes.margin}
                     onClick={()=>{
                     this.props.handleDeleteSession(n)
                     //this.props.openEditUser()
                     }}
                   >
                      <Icon className={classes.iconDelete}>delete</Icon>
                   </Button> */}


<IconButton
                                  //  aria-owns={selectedMenu ? 'selectedProductsMenu' : null}
                                  //  aria-haspopup="true"
                                    onClick={event =>this.openSelectedProductsMenu(event)}
                                >
                                    <Icon>more_horiz</Icon>
                                </IconButton>
                                <Menu
                                    id="selectedProductsMenu"
                                    anchorEl={selectedMenu}
                                    open={Boolean(selectedMenu)}
                                    onClose={this.closeSelectedProductsMenu}
                                >
                                    <MenuList>
                                        <MenuItem
                                            onClick={() => {
                                                this.closeSelectedProductsMenu();
                                                this.handleClick(n)
                                            }}
                                        >
                                            <ListItemIcon className={classes.icon}>
                                                <Icon>pageview</Icon>
                                            </ListItemIcon>
                                            <ListItemText inset primary="Detail"/>
                                        </MenuItem>
                                    </MenuList>
                                   
                                    <MenuList>
                                        <MenuItem
                                            onClick={(event)=>{
                                              this.props.handleDeleteSession(n)
                                              this.closeSelectedProductsMenu(event)
                                            }}
                                           //</MenuList> onClick={}
                                        >
                                            <ListItemIcon className={classes.icon}>
                                                <Icon>edit</Icon>
                                            </ListItemIcon>
                                            <ListItemText inset primary="Modifier"/>
                                        </MenuItem>
                                    </MenuList>

                                    <MenuList>
                                        <MenuItem
                                            onClick={(event)=>{
                                             // this.props.handleDeleteSession(n)
                                              this.closeSelectedProductsMenu(event)
                                            }}
                                           //</MenuList> onClick={}
                                        >
                                            <ListItemIcon className={classes.icon}>
                                                <Icon>delete</Icon>
                                            </ListItemIcon>
                                            <ListItemText inset primary="Supprimer"/>
                                        </MenuItem>
                                    </MenuList>
                                </Menu>
                  </CustomTableCell>
                  <CustomTableCell component="th" scope="entreprise">
                    {n.theme}
                  </CustomTableCell>
                  <CustomTableCell align="left">
                     formateur nom
                  </CustomTableCell>
                  <CustomTableCell align="left">
                     {n.hours}
                  </CustomTableCell>
                  <CustomTableCell align="left">
                     { new Date(n.startDate).toDateString()}
                  </CustomTableCell>
                  <CustomTableCell align="left">{n.place}</CustomTableCell>
                  <CustomTableCell align="left">{n.nbre_place}</CustomTableCell>
                  <CustomTableCell align="left">13</CustomTableCell>
                  
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      );
  }
}

SessionList.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch)
{
    return ({
        //openEditSession: () => dispatch(openEditSession()),
        handleDeleteSession:(session) => (dispatch(handleDeleteSession(session))),
        //getSession:(session) => (dispatch(getSession(session)))
    });
}

function mapStateToProps(state)
{
    //console.log('sessions',state.firestore.ordered.formations)
    return {
        sessions: state.firestore.ordered.sessions,
        formations: state.firestore.ordered.formations
        // searchText: eCommerceApp.products.searchText
    }
}

export default compose(
    withRouter,
      connect(mapStateToProps,mapDispatchToProps),
      firestoreConnect([
        { collection: 'sessions'},
  ]))(withStyles(styles)(SessionList));