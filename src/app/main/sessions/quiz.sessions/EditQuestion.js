import React, {Component} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar,
    
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import _ from '@lodash';

import {closeEditQuestion} from '../../../store/actions/quiz/quiz.action'


const options = [
    {
      value: '1',
      label: 'option1',
    },
    {
      value: '2',
      label: 'option2',
    },
    {
      value: '3',
      label: 'option3',
    },
  ];

const newTodoState = {
    question:'',
    option1:'',
    option2:'',
    option3:'',
    correct:'',
    questionError:'',
    option1Error:'',
    option2Error:'',
    option3Error:'',
    correct:''
};

const styles = theme => ({
   
    textField1: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      maxWidth : '300px'
    },
    menu: {
        width: 200,
    },
  });

class EditQuestion extends Component {

    state = {
        ...newTodoState,
        labelMenuEl: null
    };

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        /**
         * After Dialog Open
         */

        if ( !prevProps.todoDialog.props.open && this.props.todoDialog.props.open )
        {    
            if ( this.props.todoDialog.type === 'edit')
            {
                this.setState({...this.props.question.question});
            }
        }
    }

    handleChange = (event) => {
            const isCheckbox = event.target.type ==='checkbox';
            this.setState({
                [event.target.name] : isCheckbox
                ? event.target.checked
                : event.target.value
            })
    };

    closeUser= () => {
        this.props.todoDialog.type === 'edit' ? this.props.closeEditQuestion() : this.props.closeNewQuestion();
    };

   validate = () =>{
    // var  name      = 'minimum 3 caracteres'
    let questionError =''
    let option1Error =''
    let option2Error =''
   let option3Error=''

     if(!this.state.question){
        questionError='Le nom ne doit pas etre vide '
      }
      if(!this.state.option1){
         option1Error='Le nom ne doit pas etre vide '
      }
      if(!this.state.option2){
        option2Error='Le nom ne doit pas etre vide '
      }
      if(!this.state.option3){
        option3Error='Le nom ne doit pas etre vide '
      }
    
     if(questionError || option1Error || option2Error || option3Error){
         this.setState({
             questionError :'',
             option1Error :'',
             option2Error :'',
              option3Error:'',
         })
         return false
     }
    return true
   }
   
    handleSubmit = (event) =>{

        event.preventDefault();
        const isValid = this.validate();  
        if(isValid){
            this.props.inviteUser(this.state)
            this.setState({
                question:'',
                option1:'',
                option2:'',
                option3:'',
                correct:'',
                questionError:'',
                option1Error:'',
                option2Error:'',
                option3Error:'',
            })
          this.props.closeNewUser();
        }
    }

    handleUpdate = (event) =>{
        console.log(this.state)


        event.preventDefault();
        const isValid = this.validate();  
        if(isValid){
            //this.props.inviteUser(this.state)
            this.props.updateAdd(this.state)
            this.setState({
                name : '',
                email_user:'',
                nameError: '',
                emailError:''
            })
            this.props.closeEditUser();
        }
    }

    canBeSubmitted()
    {
        const {name, email_user} = this.state;
        return (
            name.length > 2 && email_user.FormControl > 0
        );
    }

    render()
    {
        const {todoDialog,classes} = this.props;    
        return (
            <Dialog {...todoDialog.props} onClose={this.closeUser} fullWidth maxWidth="sm">

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            {todoDialog.type === 'edit' ? 'Modifier' : ''}
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-0"}}>      

                    <div className="px-16 sm:px-24">
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="question"
                                autoFocus
                               // error={this.state.name.length < 2}
                                name="Question"
                                id="question"
                                required
                                value={this.state.question}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                               // error={this.state.questionError !=''}
                                //helperText={this.state.questionError}
                            />    
                        </FormControl>
                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Option1"
                                autoFocus
                               // error={this.state.name.length < 2}
                                name="option1"
                                id="option1"
                                required
                                value={this.state.question}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                               // error={this.state.questionError !=''}
                                //helperText={this.state.questionError}
                            />    
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Option1"
                                autoFocus
                               // error={this.state.name.length < 2}
                                name="option1"
                                id="option1"
                                required
                                value={this.state.question}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                               // error={this.state.questionError !=''}
                                //helperText={this.state.questionError}
                            />    
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                label="Option3"
                                autoFocus
                               // error={this.state.name.length < 2}
                                name="option3"
                                id="option3"
                                required
                                value={this.state.question}
                                onChange={this.handleChange}
                                required
                                variant="outlined"
                               // error={this.state.questionError !=''}
                                //helperText={this.state.questionError}
                            />    
                        </FormControl>

                        <FormControl className="mt-8 mb-16" required fullWidth>
                            <TextField
                                id="correct"
                                name="correct"
                                select
                                label="Option correcte"
                                value={this.state.role}
                                onChange={this.handleChange}
                                SelectProps={{
                                  native: true,
                                    MenuProps: {
                                    className: classes.menu,
                                  },
                               }}
                                margin="normal"
                                variant="outlined"
                            >
                           {options.map(option => (
                             <option key={option.id} value={option.value}>
                                  {option.label}
                            </option>
                            ))}
                        </TextField>
                      </FormControl>  
                    </div>

                </DialogContent>
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={(event) => {
                                //addFormation(this.state);
                               // this.handleUpdate(event)
                            }}
                            // disabled={!this.canBeSubmitted()}
                        >
                            Save
                        </Button>
                        <IconButton
                            className="min-w-auto"
                            onClick={() => {
                                //removeUser(this.state.uid);
                                //this.props.closeEditUser();
                            }}
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return {
        
       // closeEditTodoDialog: Actions.closeEditTodoDialog,
       closeEditQuestion: () => dispatch(closeEditQuestion()),
        //    closeNewUser: () => dispatch(closeNewUser()),
        //   removeUser: () => dispatch(removeUser()),
        //   inviteUser:(user) => dispatch(inviteUser(user)),
        //   updateAdd:(user) => dispatch(updateAdd(user))
    }
}

function mapStateToProps(state)
{
    console.log('dddddddddd',state.quizReducer.question)
    return {
       todoDialog: state.quizReducer.todoDialog,
       question : state.quizReducer.question
       // labels    : todoApp.labels
    }
}

EditQuestion.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(EditQuestion));
