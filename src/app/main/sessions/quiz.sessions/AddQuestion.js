import React, {Component} from 'react';
import {withStyles, Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography} from '@material-ui/core';
import {FuseAnimate, FusePageCarded, FuseChipSelect} from '@fuse';
import {orange} from '@material-ui/core/colors';
import {Link, withRouter} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import connect from 'react-redux/es/connect/connect';
import classNames from 'classnames';
import _ from '@lodash';

import {addQuestion} from '../../../store/actions/quiz/quiz.action'

const styles = theme => ({
    productImageFeaturedStar: {
        position: 'absolute',
        top     : 0,
        right   : 0,
        color   : orange[400],
        opacity : 0
    },
    productImageItem        : {
        transitionProperty      : 'box-shadow',
        transitionDuration      : theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover'               : {
            boxShadow                    : theme.shadows[5],
            '& $productImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured'            : {
            pointerEvents                      : 'none',
            boxShadow                          : theme.shadows[3],
            '& $productImageFeaturedStar'      : {
                opacity: 1
            },
            '&:hover $productImageFeaturedStar': {
                opacity: 1
            }
        }
    }
});


const correctAnswer = [
    {
      value: '0',
      label: 'option1',
    },
    {
      value: '1',
      label: 'option2',
    },
    {
      value: '2',
      label: 'option3',
    },
  ];

class AddQuestion extends Component {

    state = {
         question:'',
         option1:'',
         option2:'',
         option3:'',
         correct:''
    };

    // componentDidMount()
    // {
    //     this.updateProductState();
    // }

    // componentDidUpdate(prevProps, prevState, snapshot)
    // {
    //     if ( !_.isEqual(this.props.location, prevProps.location) )
    //     {
    //         this.updateProductState();
    //     }

    //     if (
    //         (this.props.product.data && !this.state.form) ||
    //         (this.props.product.data && this.state.form && this.props.product.data.id !== this.state.form.id)
    //     )
    //     {
    //         console.log(test)
    //         this.updateFormState();
    //     }
    // }

    // updateFormState = () => {
    //     this.setState({form: this.props.product.data})
    // };

    // updateProductState = () => {
    //     const params = this.props.match.params;
    //     console.log('test',params)
    //     const {productId} = params;

    //     if ( productId === 'new' )
    //     {
    //         this.props.newProduct();
    //     }
    //     else
    //     {
    //         this.props.getProduct(this.props.match.params);
    //     }
    // };
    handleChange = (event) => {
        const isCheckbox = event.target.type ==='checkbox';
        this.setState({
            [event.target.name] : isCheckbox
            ? event.target.checked
            : event.target.value
        })
    };

    handleSubmit = () => {

        const params = this.props.match.params;
        const {sessionId} = params;
         this.props.addQuestion(this.state,sessionId)
    };

    // handleChange = (event) => {
    //     this.setState({...this.state.form}, event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value)});
    // };

    // handleChipChange = (value, name) => {
    //     this.setState({form: _.set({...this.state.form}, name, value.map(item => item.value))});
    // };

    // setFeaturedImage = (id) => {
    //     this.setState({form: _.set({...this.state.form}, 'featuredImageId', id)});
    // };

    // canBeSubmitted()
    // {
    //     const {name} = this.state.form;
    //     return (
    //         name.length > 0 &&
    //         !_.isEqual(this.props.product.data, this.state.form)
    //     );
    // }

    render()
    {
        const {classes} =this.props
 
        return (
          <div className="p-4 sm:p-5 max-w-2xl">
            <TextField
              className="mt-8 mb-16"
                           // error={form.name === ''}
              required
              label="Question"
              id="question"
              name="question"
              value={this.state.question}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />

            <TextField
              className="mt-8 mb-16"
                                       // error={form.name === ''}
              required
              label="Option 1"
              id="option1"
              name="option1"
              value={this.state.question2}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />

           <TextField
              className="mt-8 mb-16"
                                       // error={form.name === ''}
              required
              label="Option 2"
              id="option2"
              name="option2"
              value={this.state.option2}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />

          <TextField
              className="mt-8 mb-16"
                                       // error={form.name === ''}
              required
              label="Reponse exacte"
              id="option3"
              name="option3"
              value={this.state.option3}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />
            <TextField
               fullWidth
               id="correct"
               name="correct"
               select
               //label="Formation"
               value={this.state.correct}
               onChange={this.handleChange}
               SelectProps={{
                 native: true,
                 MenuProps: {
                    className: classes.menu,
                 },
               }}
               margin="normal"
               variant="outlined"
            >
             {correctAnswer.map(option => (
                <option key={option.id} value={option.value}>
                    {option.label}
                </option>
             ))}
            </TextField>

<FuseAnimate animation="transition.slideRightIn" delay={300} className="flex flex-1  items-center justify-between">
                                <Button
                                    className="whitespace-no-wrap"
                                    variant="contained"
                                    // disabled={!this.canBeSubmitted()}
                                     onClick={this.handleSubmit}
                                >
                                    Ajouter
                                </Button>
                            </FuseAnimate>                  
            </div>

        )
    };
}

function mapDispatchToProps(dispatch)
{
    return {
        
        addQuestion: (question,id) => dispatch(addQuestion(question,id))
        // getProduct : Actions.getProduct,
        // newProduct : Actions.newProduct,
        // saveProduct: Actions.saveProduct
    };
}

function mapStateToProps()
{
    return {

    }
}

export default withStyles(styles, {withTheme: true})(withRouter(connect(mapStateToProps, mapDispatchToProps)(AddQuestion)));
