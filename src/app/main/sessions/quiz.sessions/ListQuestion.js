import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { compose} from 'redux';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';

import EditQuestion from './EditQuestion'
import {openEditQuestion,handleTakeQuestion} from '../../../store/actions/quiz/quiz.action'


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
  },
  row: {
   
      backgroundColor: theme.palette.background.default,
    
  },
  iconCheck:{
    color:'green'
  },
  iconCheckNo:{
    color : 'red'
  }
});



class ListQuestion extends Component {
  
  render(){
    const {classes,firebase,auth} = this.props;
    const questions= this.props.questions || []


    return (
      <div>
        <Paper className={classes.root} delay={1000}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <CustomTableCell>Questions</CustomTableCell>
                <CustomTableCell  align="right">Reponses</CustomTableCell>
                <CustomTableCell align="right">
                <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     variant="contained"
                     color="secondary"
                   >
                      Activer
                   </Button>
                </CustomTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {questions.map(item => (
                <TableRow className={classes.row} key={item.id} hover>
                  <CustomTableCell align="left" scope="question">
                    {item.question.question}
                  </CustomTableCell>
                    {
                    item.question.correct == 0 ?
                    ( <CustomTableCell align="right" >
                           option 1
                    </CustomTableCell>  ):
                    (
                      <CustomTableCell align="right" >
                           option {item.question.correct}
                      </CustomTableCell>  )
                     }
                                         
                  <CustomTableCell align="right">
                   <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     onClick={()=>{
                      this.props.handleTakeQuestion(item)
                      this.props.openEditQuestion()
                     }}
                   >
                       <i class="fas fa-edit"></i>
                   </Button>
                  </CustomTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <EditQuestion/>
        </div>
      );
  }
}


function mapDispatchToProps(dispatch)
{
  return {
    openEditQuestion:() =>(dispatch(openEditQuestion())),
    handleTakeQuestion:(n) =>(dispatch(handleTakeQuestion(n)))
  }
}

function mapStateToProps(state)
{
  return {
    questions: state.firestore.ordered.questions,
      // searchText: eCommerceApp.orders.searchText
  }
}

export default compose(
  connect(mapStateToProps,mapDispatchToProps),
  firestoreConnect([
    { collection: 'questions'},
]))(withStyles(styles)(ListQuestion))

//export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListQuestion));
