import React from 'react';
import {
    TableHead,
    TableSortLabel,
    TableCell,
    TableRow,
    Checkbox,
    Tooltip,
    IconButton,
    Icon,
    Menu,
    MenuList,
    MenuItem,
    ListItemIcon,
    ListItemText,
    withStyles
} from '@material-ui/core';
import classNames from 'classnames';

const rows = [
    {
        id            : 'formation',
        align         : 'left',
        disablePadding: false,
        label         : 'Nom formation',
        sort          : true
    },
    {
        id            : 'hours',
        align         : 'left',
        disablePadding: false,
        label         : 'Nombre heures',
        sort          : true
    },
    {
        id            : 'days',
        align         : 'left',
        disablePadding: false,
        label         : 'Nombre de jours',
        sort          : true
    },
    {
        id            : 'place',
        align         : 'right',
        disablePadding: false,
        label         : 'Lieu formation',
        sort          : true
    },
    {
        id            : 'address',
        align         : 'right',
        disablePadding: false,
        label         : 'Adresse',
        sort          : true
    }
];

const styles = theme => ({
    actionsButtonWrapper: {
        background: theme.palette.background.paper
    }
});

class SessionListHeader extends React.Component {
    state = {
        selectedProductsMenu: null
    };

    createSortHandler = property => event => {

        this.props.onRequestSort(event, property);
    };

    openSelectedProductsMenu = (event) => {
        this.setState({selectedProductsMenu: event.currentTarget});
    };

    closeSelectedProductsMenu = () => {
        this.setState({selectedProductsMenu: null});
    };

    render()
    {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount, classes} = this.props;
        const {selectedProductsMenu} = this.state;

        return (
            <TableHead >
                <TableRow className="h-64">
                    
                    {rows.map(row => {
                        return (
                            <TableCell
                                key={row.id}
                                align={row.align}
                                padding={row.disablePadding ? 'none' : 'default'}
                                //sortDirection={orderBy === row.id ? order : false}
                            >
                                {row.sort && (
                                    <Tooltip
                                        //title="Sort"
                                       // placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                        //enterDelay={300}
                                    >
                                        <TableSortLabel
                                           // active={orderBy === row.id}
                                            //direction={order}
                                           // onClick={this.createSortHandler(row.id)}
                                        >
                                            {row.label}
                                        </TableSortLabel>
                                    </Tooltip>
                                )}
                            </TableCell>
                        )
                    })}
                </TableRow>
            </TableHead>
        );
    }
}

export default withStyles(styles, {withTheme: true})(SessionListHeader);
