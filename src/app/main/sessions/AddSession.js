import React, {Component} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar, 
} from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {connect} from 'react-redux';
import moment from 'moment/moment';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


import {closeNewSession,closeEditSession,addSession} from '../../store/actions/session/session.action'



const newTodoState = {
    'formationId'       : '',
     theme : '',
    // 'hours'    : '',
    // 'days'    : '',
    'startDate': new Date(),
    'dueDate'  : new Date(),
    'address': '',
     'formateur' : '',
     'place'  : '',
    'nbre_place' : '',
    'participant': [],

    formationIdError      : '',
    themeError : '',
    // hoursError    : '',
    // daysError    : '',
    addressError: '',
    placeError  : '',
    nbre_placeError : '',
    formateurError :''
    
};

const styles = theme => ({
   
    textField1: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      maxWidth : '300px'
    },
    menu: {
        width: 200,
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        flexBasis: 200,
    },
  });

class AddSession extends Component {

    state = {
        ...newTodoState, 
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        

        if ( !prevProps.todoDialog.props.open && this.props.todoDialog.props.open )
        {

            if ( this.props.todoDialog.type === 'new' &&
                !_.isEqual(newTodoState, prevState) )
            {
                //console.log('test edit')
                this.setState({
                        ...newTodoState,
                });
            }
        }
    }

    // handleChange = (event) => {

    //     const form = _.set({...this.state.form}, 
    //     event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    //     this.setState({form});
    // };
    
    // handleChange = (event) => {
    //     this.setState(
    //         _.set({...this.state}, 
    //         event.target.name,
    //         event.target.type === 'checkbox' ? event.target.checked : event.target.value));

    //         //console.log(this.state.startDate)
    // };

    
    handleChange = (event) => {
        // this.setState(
        //     _.set({...this.state}, 
        //     event.target.name,
        //     event.target.type === 'checkbox' ? event.target.checked : event.target.value));
            const isCheckbox = event.target.type ==='checkbox';
            this.setState({
                [event.target.name] : isCheckbox
                ? event.target.checked
                : event.target.value
            })
    };


    // handleChangeOnlyNumber = (e) => {
    //     const re = /^[0-9\b]+$/;

    //     if (e.target.value === '' || re.test(e.target.value)) {
    //      this.setState(
    //         _.set({...this.state}, 
    //         e.target.name,
    //         e.target.type === 'checkbox' ? e.target.checked : e.target.value));
    //         //console.log(this.state.startDate)
    //     }else{
            
    //     }
    // };
    closeUser= () => {
            this.props.closeNewSession();
    };
   
    //handleSubmit = () =>{ this.props.inviteUser(this.state) }

    validate = () =>{

        let themeError = '';
        // let hoursError ='';
        // let daysError  ='';
        let addressError='';
        let placeError ='';
        let nbre_placeError='';
        let formationIdError='';
        let formateurError=''

         if(!this.state.theme){
            themeError='Le theme ne doit pas etre vide'
         }
         if(!this.state.formateur){
            formateurError='Le formateur ne doit pas etre vide'
         }
         if(!this.state.address){
            addressError='Adresse ne doit pas etre vide'
         }
        //   if(!this.state.hours){
        //     hoursError='Le nombre heures ne doit pas etre vide'
        //   }

        //   if(!this.state.days){
        //     daysError='Le nombre de jours ne doit pas etre vide'
        //   }
          if(!this.state.place){
            placeError='Le lieu ne doit pas etre vide'
          }
          if(!this.state.nbre_place){
            nbre_placeError='Le nombre de place disponible ne doit pas etre vide'
          }
          if(!this.state.formationId){
            formationIdError='La formation de place disponible ne doit pas etre vide'
          }
         if(themeError || addressError || placeError || nbre_placeError || formationIdError ||formateurError){
             this.setState({
                themeError,
                // hoursError,
                // daysError,
                addressError,
                placeError,
                nbre_placeError,
                formationIdError,
                formateurError
             })
             return false
         }
        return true
       }

       handleSubmit = (event) =>{
        console.log('test test',this.state)
        event.preventDefault();
        const isValid = this.validate();  
        if(isValid){
            console.log('test test',this.state)
            this.props.addSession(this.state)
            this.setState({
                formationId: '',
                theme: '',
                // hours: '',
                // days: '',
                startDate : new Date(),
                dueDate  : new Date(),
                address: '',
                place  : '',
                nbre_place : '',
                participant : [],
                formateur:''
            })
            this.props.closeNewSession();
        }
        
    }
    handleUpdate = () =>{this.props.updateAdd(this.state.themeError)}

    // handleSubmit = () =>{
    //     console.log('submit',this.state)
    //     this.props.addSession(this.state)
    // }

    render()
    {
        const {todoDialog,classes} = this.props
        const {form, labelMenuEl} = this.state
        const formations = this.props.formations || []
  



          let  startDate = moment(this.state.startDate).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS);
           let    dueDate =   moment(this.state.dueDate).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS);
        
        //console.log(this.props.match.params)
        
        return (
        <Dialog {...todoDialog.props} onClose={this.closeUser} fullWidth maxWidth="sm">

            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                   <Typography variant="subtitle1" color="inherit">
                       {todoDialog.type === 'new' ? 'Nouvelle session' : 'Modification'}
                   </Typography>
                </Toolbar>
            </AppBar>

        <DialogContent classes={{root: "p-0"}}>

       <div className="px-16 sm:px-24">

       <FormControl className="mt-8 mb-10" required fullWidth>
           <TextField
               id="theme"
               name="theme"
               label="titre de formation"
               value={this.state.theme}
               onChange={this.handleChange}
               margin="normal"
               variant="outlined"
               error={this.state.themeError !=''}
               helperText={this.state.themeError}
            >
            </TextField>
        </FormControl>

       <FormControl className="mt-8 mb-16" required fullWidth>
           <TextField
               id="formationId"
               name="formationId"
               select
               //label="Formation"
               helperText="formation"
               value={this.state.formation}
               onChange={this.handleChange}
               SelectProps={{
                 native: true,
                 MenuProps: {
                    className: classes.menu,
                 },
               }}
               margin="normal"
               variant="outlined"
               error={this.state.formationIdError !=''}
               helperText={this.state.formationIdError}
            >
             {formations.map(option => (
                <option key={option.id} value={option.formationId}>
                    {option.title}
                </option>
             ))}
            </TextField>
        </FormControl>
        <FormControl className="mt-8 mb-16" required fullWidth>
        <TextField
            id="formateur"
            name="formateur"
            label="formateur"
            value={this.state.formateur}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
            error={this.state.formateurError !=''}
            helperText={this.state.formateurError}
        >
        </TextField>
        </FormControl>

     {/* <div className="flex">
        <TextField
            id="hours"
           // error={this.state.hours !== }
           type="number"
            name="hours"
            className="mt-8 mb-16 mr-8"
            variant="filled"
            //label="Hour"
            value={this.state.hours}
            onChange={this.handleChange}
            helperText="Durée de la session en heure"
            InputProps={{
              endAdornment: 
              <InputAdornment position="end">
                <Icon>watch_later</Icon>  Heures
                </InputAdornment>,
               }}
            variant="outlined"
            error={this.state.hoursError !=''}
            helperText={this.state.hoursError}
        />
     
        <TextField
            id="days"
            name="days"
            className="mt-8 mb-16 mr-8"
            variant="filled"
            //label="Hour"
            type="number"
            value={this.state.days}
            onChange={this.handleChange}
            helperText="Durée de la session en jour"
            InputProps={{
              endAdornment:
              <InputAdornment position="end">
                    <Icon color='bleu'>view_day</Icon>Jours
              </InputAdornment>
              }}
            variant="outlined"
            error={this.state.daysError !=''}
            helperText={this.state.daysError}
        />
    </div> */}

   
    <div className="flex">
        <TextField
            id="startDate"
            name="startDate"
            label="Date debut formation"
            type="datetime-local"
            className="mt-8 mb-16 mr-8"
            InputLabelProps={{
                shrink: true
            }}
            inputProps={{
                max: dueDate
            }}
            value={startDate}
            onChange={this.handleChange}
            variant="outlined"
        />
        <TextField
            id="dueDate"
            name="dueDate"
            label="Date fin formation "
            type="datetime-local"
            className="mt-8 mb-16 ml-8"
            InputLabelProps={{
                shrink: true
            }}
            inputProps={{
                min: startDate
            }}
            value={dueDate}
            onChange={this.handleChange}
            variant="outlined"
        />
     </div>

     <div className="flex">
     <TextField
            id="nbre_place"
            name="nbre_place"
            className="mt-8 mb-16 mr-8"
            variant="filled"
            type="number"
            label="nombre maximal place"
            value={this.state.nbre_place}
            onChange={this.handleChange}
            helperText="Durée de la session en heure"
            InputProps={{
              endAdornment: 
              <InputAdornment position="end">
                <Icon>person_outline</Icon>
                </InputAdornment>,
               }}
            variant="outlined"
            error={this.state.nbre_placeError!=''}
            helperText={this.state.nbre_placeError}
        />

        <TextField
            id="place"
            name="place"
            className="mt-8 mb-16 mr-8"
            variant="filled"
            label="Lieu de formation"
            value={this.state.place}
            onChange={this.handleChange}
            InputProps={{
              endAdornment: 
              <InputAdornment position="end">
                <Icon>place</Icon> 
                </InputAdornment>,
               }}
            variant="outlined"
            error={this.state.placeError!=''}
            helperText={this.state.placeError}
        />
     </div>

      <FormControl className="mt-8 mb-16" required fullWidth>
            <TextField
                id="address"
                name="address"
                label="Adresse"
                value={this.state.address}
                onChange={this.handleChange}
                required
                variant="outlined"
                error={this.state.addressError!=''}
                helperText={this.state.addressError}
            />
        </FormControl>

   </div>
 </DialogContent>

                {todoDialog.type === 'new' ? (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={(event) => {
                                this.handleSubmit(event)
                               // this.props.closeNewSession();
                            }}
                           // disabled={!this.canBeSubmitted()}
                        >
                            Ajouter
                        </Button>
                    </DialogActions>
                ) : (
                    <DialogActions className="justify-between pl-8 sm:pl-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                //addFormation(this.state);
                                //this.handleUpdate()
                               // this.props.closeEditSession();
                            }}
                            // disabled={!this.canBeSubmitted()}
                        >
                            Enregistrer
                        </Button>
                        <IconButton
                            className="min-w-auto"
                            onClick={() => {
                               // removeUser(this.state.uid);
                              //  this.props.closeEditSession();
                            }}
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
                )}
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return {
        
       // closeEditTodoDialog: Actions.closeEditTodoDialog,
           closeEditSession: () => dispatch(closeEditSession()),
           closeNewSession: () => dispatch(closeNewSession()),
         // removeUser: () => dispatch(removeUser()),
          addSession:(session) => dispatch(addSession(session)),
        //  updateAdd:(user) => dispatch(updateAdd(user))
       // addTodo            : Actions.addTodo,
       // updateTodo         : Actions.updateTodo,
       // removeTodo         : Actions.removeTodo
    }
}

function mapStateToProps(state)
{
    return {
       todoDialog: state.sessionReducer.todoDialog,
       subTrainigItem : state.subTrainingReducer.subTrainigItem,
       formations: state.firestore.ordered.formations
       // labels    : todoApp.labels
    }
}

AddSession.propTypes = {
    classes: PropTypes.object.isRequired,
  };


export default compose(
    withRouter,
      connect(mapStateToProps,mapDispatchToProps),
      firestoreConnect([
        { collection: 'formations'},
]))(withStyles(styles)(AddSession));

