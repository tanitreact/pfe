import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'
import {Redirect} from 'react-router-dom';

import Login from '../../auth/Login'



Authguard.propTypes = {
  firebase: PropTypes.shape({
    login: PropTypes.func.isRequired
  }),
  auth: PropTypes.object
}

function mapStateToProps(state) {
  return {
    auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(Authguard)