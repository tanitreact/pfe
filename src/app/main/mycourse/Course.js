import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import {FuseAnimateGroup} from '@fuse';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import firebase from '../../firebase/fbConfig';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'
import {currentUserSession,getSelectedItem} from '../../store/actions/session/user.session.action'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth:'100%',
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: 0,
    color: theme.palette.text.secondary,
  },
  button: {
    width: '100%',
    height: '50px',
    maxWidth:'100%',
    marginBottom:'20px',
    backgroundColor :'red'
  },
  ListItem:{
    width: '100%',
    maxWidth:'100%',
  },
  icon:{
    color:'red'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    display: 'flex',
    marginBottom: '10px',
  },
  content: {
    flex: '1 0 auto',
    maxWidth:'30%',
    width: 250,
  },
});



class Course extends Component{
  
 
  componentDidMount()
  {
      this.unsubscribe = firebase.auth().onAuthStateChanged(currentUser => {
         this.props.currentUserSession(currentUser.uid)
      })

  }

  componentWillUnmount() {
    this.unsubscribe()
  }

handleTakeSession = (item) =>{

     // this.props.currentUserSession()
    this.props.getSelectedItem(item)
    console.log('test',item)
  }

  render(){

    if (!isLoaded(this.props.currentUser)) {
      return <span delay={15000}>Loading...</span>
    }
  
    const { classes,user,currentUser } = this.props;
    const userSessions = this.props.userSessions || []
    
      return (
            <FuseAnimateGroup
              enter={{
                animation: "transition.slideUpBigIn"
                  }}
                  className="flex flex-wrap"
                  delay={1500}
              >
              {
                userSessions.map((sessionUser) => {
                   return(
              <div className="w-full   sm:p-4 cursor-pointer"  key={sessionUser.id}>
                  <Divider/>
                       <Card className={classes.card} hover onClick={event => this.handleTakeSession(sessionUser)}>
                          <CardMedia
                            className={classes.content}
                            image="https://previews.123rf.com/images/iterum/iterum1408/iterum140800221/37511668-froiss%C3%A9-vert-mod%C3%A8le-de-r%C3%A9seau-g%C3%A9om%C3%A9trique-triangulaire-d-herbe-de-style-bas-de-poly-abstract-background-gra.jpg"
                            title="Live from space album cover"
                          />                    
                          <CardContent className={classes.cover}>
                            <Typography component="h5" variant="h5">
                                {sessionUser.theme} 
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                Date debut session:32 juin 2019
                            </Typography>
                            </CardContent>
                             
                       
                        </Card>
                  <Divider/>
              </div>
                   )
            })       
            }    
            </FuseAnimateGroup>
   
    )}
    
 
}

Course.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch)
{
    return ({
          currentUserSession: (userId) => dispatch(currentUserSession(userId)),
          getSelectedItem: (item) => dispatch(getSelectedItem(item))
          
    //   getDetailSession : (session) => dispatch(getDetailSession(session))
    });
}

function mapStateToProps(state)
{
      console.log('1', state.userSessionReducer.userSessions)
    return {
        sessions: state.firestore.ordered.sessions,
        userSessions : state.userSessionReducer.userSessions,
        formations: state.firestore.ordered.formations,
        currentUser: state.firebase.auth,
        user: state.auth.user,
        // searchText: eCommerceApp.products.searchText
    }
}

Course.propTypes = {
  classes: PropTypes.object.isRequired,
  firebase: PropTypes.shape({
    login: PropTypes.func.isRequired
  }),
  auth: PropTypes.object
};

export default compose(
    withRouter,
      connect(mapStateToProps,mapDispatchToProps),
      firestoreConnect([
        { collection: 'sessions'},
]))(withStyles(styles)(Course));

//export default Course


