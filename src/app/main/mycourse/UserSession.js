
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import {FuseAnimateGroup} from '@fuse';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card';
import { compose} from 'redux';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import firebase from '../../firebase/fbConfig';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'

import {currentUserSession} from '../../store/actions/session/user.session.action'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth:'100%',
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    margin: 0,
    color: theme.palette.text.secondary,
  },
  button: {
    width: '100%',
    height: '50px',
    maxWidth:'100%',
    marginBottom:'20px',
    backgroundColor :'red'
  },
  ListItem:{
    width: '100%',
    maxWidth:'100%',
  },
  icon:{
    color:'red'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    display: 'flex',
    marginBottom: '10px',
  },
  content: {
    flex: '1 0 auto',
    maxWidth:'30%',
    width: 250,
  },
});



class UserSession extends Component{

    componentDidMount()
    {
        this.handleTakeSession();
    }
  
    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if ( !_.isEqual(this.props.location, prevProps.location) )
        {
            this.handleTakeSession();
        }
      }
    handleTakeSession = () =>{
      this.props.currentUserSession(this.props.userId)
    }

  render(){

    const { userId} = this.props
      return (
          <div>
              <p>test test</p>
              <p>{userId}</p>
          </div>  
    )}
    
 
}

function mapDispatchToProps(dispatch)
{
    return ({
          currentUserSession: (userId) => dispatch(currentUserSession(userId)),
    //   getDetailSession : (session) => dispatch(getDetailSession(session))
    });
}
function mapStateToProps(state)
{
    return {
       // sessions: state.firestore.ordered.sessions,
        userSessions : state.userSessionReducer.userSessions,
       // formations: state.firestore.ordered.formations,
       // currentUser: state.firebase.auth,
       // user: state.auth.user,
        // searchText: eCommerceApp.products.searchText
    }
}

export default compose(
    withRouter,
      connect(mapStateToProps,mapDispatchToProps),
  )(withStyles(styles)(UserSession));


//export default UserSession;