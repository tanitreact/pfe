import {FuseLoadable} from '@fuse';

export const CourseConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/app/mescours',
            component: FuseLoadable({
                loader: () => import('./Courses')
            })
        }
    ]
};