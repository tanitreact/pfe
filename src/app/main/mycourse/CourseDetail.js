import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Divider from '@material-ui/core/Divider';
import { connect } from 'react-redux';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { withStyles,Typography}from '@material-ui/core'
   


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    padding: '16px'
  },
});

function CourseDetail({classes,sessionDetail}) {
  //const {classes}= this.props 

  return (
<div>
    <Typography variant="h4" className="p-16">{sessionDetail.theme}</Typography>
    <List className={classes.root} animation="transition.slideUpIn">
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Nombres d'heures" secondary={sessionDetail.hours}/>
      </ListItem>
      <Divider variant="inset" component="li" />

      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <WorkIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Nombres de jours" secondary={sessionDetail.days} />
      </ListItem>
      <Divider variant="inset" component="li" />

      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BeachAccessIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Nombre de place" secondary={sessionDetail.nbre_place} />
      </ListItem>
      <Divider variant="inset" component="li" />


      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BeachAccessIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Date de debut de formation" secondary='13 juillet 2019' />
      </ListItem>

      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BeachAccessIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Date de fin de formation" secondary='13 juillet 2019}' />
      </ListItem>
    </List>
    </div>
  );
}



function mapStateToProps(state)
{
   // console.log('aaaaaaaaah',state.userSessionReducer.sessionDetail)
    return {
        sessionDetail : state.userSessionReducer.sessionDetail,
      //  selectedItem: fileManagerApp.selectedItem
    }
}
export default withStyles(styles, {withTheme: true})(connect(mapStateToProps)(CourseDetail))
//export default connect(mapStateToProps)(CourseDetail);
