import React, {Component} from 'react';
import {Fab, Icon, IconButton, Typography} from '@material-ui/core';
import {FusePageSimple, FuseAnimate} from '@fuse';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
//import withReducer from 'app/store/withReducer';
//import FileList from './FileList';
//import DetailSidebarHeader from './DetailSidebarHeader';
//import DetailSidebarContent from './DetailSidebarContent';
import CourseDetailHeader from './CourseDetailHeader';
import Course from './Course';
import store from '../../store';
import history from '../../../history';
import CourseDetail from './CourseDetail';
//import MainSidebarContent from './MainSidebarContent';
// import * as Actions from './store/actions';
// import reducer from './store/reducers';

class Courses extends Component {
    
    render()
    {
        

        return (
            <FusePageSimple
                classes={{
                    header       : "h-40 min-h-40 sm:h-60 sm:min-h-60 mb-24",
                    sidebarHeader: "h-40 min-h-40 sm:h-60 sm:min-h-60",
                    rightSidebar : "w-400"
                }}
                header={
                    <div className="flex flex-col flex-1 p-8 sm:p-12 relative">
                        <div className="flex items-center justify-between">
                        <Typography variant="h4" className=" mt-30 mb-20" delay={3000}>Mes sessions</Typography>
                        </div>
                    </div>
                }
                content={
                    <Course/>
                }
              //  leftSidebarVariant="temporary"
                // leftSidebarHeader={
                //     <MainSidebarHeader/>
                // }
                // leftSidebarContent={
                //     <MainSidebarContent/>
                // }
                rightSidebarHeader={
                    <CourseDetailHeader/>
                }
                rightSidebarContent={
                    <CourseDetail />
                }
                // onRef={instance => {
                //     this.pageLayout = instance;
                // }}
                // innerScroll
            />
        )
    };
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
       // getFiles: Actions.getFiles
    }, dispatch);
}

function mapStateToProps()
{
    return {
        // files       : fileManagerApp.files,
        // selectedItem: fileManagerApp.selectedItem
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Courses));