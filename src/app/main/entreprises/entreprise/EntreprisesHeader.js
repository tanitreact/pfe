import React from 'react';
import {Paper, Button, Input, Icon, Typography, MuiThemeProvider} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';


const EntreprisesHeader = ({setSearchText, searchText, mainTheme}) => {

    return (
        <div className="flex flex-1 w-full items-center justify-between">

            <div className="flex items-center">
                <FuseAnimate animation="transition.expandIn" delay={2000}>
                    <Icon className="text-32 mr-0 sm:mr-12">business</Icon>
                </FuseAnimate>
                <FuseAnimate animation="transition.slideLeftIn" delay={2000}>
                    <Typography className="hidden sm:flex" variant="h6">Entreprise</Typography>
                </FuseAnimate>
            </div>
            
            <FuseAnimate animation="transition.slideRightIn" delay={1500}>
                <Button component={Link} to="/apps/gestion-session/entreprise/new" className="whitespace-no-wrap" variant="contained">
                    <span className="hidden sm:flex">New Entreprise</span>
                    <span className="flex sm:hidden">New</span>
                </Button>
            </FuseAnimate>
        </div>
    );
};

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        //setSearchText: Actions.setProductsSearchText
    }, dispatch);
}

function mapStateToProps({eCommerceApp, fuse})
{
    return {
       // searchText: eCommerceApp.products.searchText,
        mainTheme : fuse.settings.mainTheme
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntreprisesHeader);
