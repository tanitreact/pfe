import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { compose} from 'redux';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {LinearProgress} from '@material-ui/core';


import {
  removeEntreprise,
  getAllEntreprises,
  UpadateSearchEntreprise,
  getAllEntreprisesSuccess,
   EditEntrepriseItem
  } from '../../../store/actions/entreprise/entreprise.action'


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    //marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  linearProgress:{
    flexGrow: 1,
}
});



class EntreprisesTable extends Component {
  
    componentDidMount()
    {
        this.props.getAllEntreprises();
    }

  //   componentDidUpdate(prevProps, prevState)
  //   {
  //       if ( !_.isEqual(this.props.allEntreprises, prevProps.allEntreprises) || !_.isEqual(this.props.searchText, prevProps.searchText) )
  //       {
  //           const data = this.getFilteredArray(this.props.allEntreprises, this.props.searchText);
  //           this.setState({data})
  //       }
  //   }

  //   getFilteredArray = (data, searchText) => {
  //     if ( searchText.length === 0 )
  //     {
  //         return data;
  //     }
  //     return _.filter(data, item => item.name.toLowerCase().includes(searchText.toLowerCase()));
  // };


  handleClick = (item) => {
     this.props.EditEntrepriseItem(item)
    };
  


  render(){
    const { classes} = this.props;
    const entreprises= this.props.entreprises 


    if(entreprises === undefined){
      return(
         <div 
             className={classes.linearProgress}
         >
             <LinearProgress delay={4000} color="primary" variant="query" />
         </div>
      )
    }
   
    return (
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <CustomTableCell>Name</CustomTableCell>
                <CustomTableCell align="right=">Email</CustomTableCell>
                <CustomTableCell  align="right=">Adress</CustomTableCell>
                <CustomTableCell align="right">City</CustomTableCell>
                <CustomTableCell align="right"></CustomTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {entreprises.map(entreprise => (
                <TableRow className={classes.row} key={entreprise.id}>
                  <CustomTableCell component="th" scope="entreprise">
                    {entreprise.name_entreprise}
                  </CustomTableCell>
                  <CustomTableCell align="left">{entreprise.email_entreprise}</CustomTableCell>
                  <CustomTableCell align="left">{entreprise.adress_entreprise}</CustomTableCell>
                  <CustomTableCell align="right">{entreprise.city}</CustomTableCell>
                  <CustomTableCell align="right">
                  <Button 
                      size="small"
                      color="secondary" 
                      className={classes.margin}
                      onClick={()=>{
                          //console.log(entreprise.id)
                        this.props.removeEntreprise(entreprise.id)
                      }
                      }
                    >
                      <i class="fas fa-trash"></i>
                   </Button>
                   <Button  
                     size="small" 
                     color="black"
                     className={classes.margin}
                     component={Link} to={'/apps/gestion-session/entreprise/' + entreprise.id + '/' + entreprise.name_entreprise}
                     
                    //  onClick={()=>{                      
                    //    //this.props.UpadateSearchEntreprise(entreprise)
                    //    this.handleClick(entreprise)
                       
                    // }
                    // }
                    onClick={()=> this.handleClick(entreprise)}
                   >
                       <i class="fas fa-edit"></i>
                   </Button>
                  </CustomTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      );
  }
}

EntreprisesTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
 // console.log('table',this.props.allEntreprises)
    return {
        allEntreprises: state.entrepriseReducer.allEntreprises,
        searchText: state.entrepriseReducer.searchText,
        entreprises: state.firestore.ordered.entreprises
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
      getAllEntreprises: () => dispatch(getAllEntreprises()),
      removeEntreprise: (id) => dispatch(removeEntreprise(id)),
      UpadateSearchEntreprise: (entreprise) => dispatch(UpadateSearchEntreprise(entreprise)),
      getAllEntreprisesSuccess :() =>dispatch(getAllEntreprisesSuccess()),
      EditEntrepriseItem :(entreprise) =>dispatch( EditEntrepriseItem(entreprise))
    }}

export default compose(
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
      { collection: 'entreprises'},
    ]))(withStyles(styles)(EntreprisesTable));

//export default withStyles(styles)(EntreprisesTable);