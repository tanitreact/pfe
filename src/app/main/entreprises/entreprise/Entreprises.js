import React, {Component} from 'react';
import {FusePageCarded} from '@fuse';
import EntreprisesTable from './EntreprisesTable';
import EntreprisesHeader from './EntreprisesHeader';
import { connect } from 'react-redux';
import { compose} from 'redux';
import _ from '@lodash';
import { firestoreConnect } from 'react-redux-firebase';
import {Redirect} from 'react-router-dom';

import firebase from '../../../firebase/fbConfig';

//import Error404Page from '../../errors/Error404Page'



class Entreprises extends Component{

    // state = {
    //     authUser: null,
    //   };


    // componentDidMount() {
    //     this.props.firebase.auth.onAuthStateChanged(authUser => {
    //       authUser
    //         ? this.setState({ authUser })
    //         : this.setState({ authUser: null });
    //     });
    // }

    // componentDidMount() {
    //     this.listener = this.props.firebase.auth.onAuthStateChanged(
    //       authUser => {
    //         authUser
    //           ? this.setState({ authUser })
    //           : this.setState({ authUser: null });
    //       },
    //     );
    // }
    // componentWillUnmount() {
    //     this.listener();
    // }
    
    render(){

        return (
            <FusePageCarded
                classes={{
                     content: "flex",
                     header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
                }}
                header={
                    <EntreprisesHeader/>
                }
                content={
                    <EntreprisesTable/>
                }
                innerScroll
            />
       ) 
    }
};

const mapStateToProps = (state) => {
    return {
        entreprises: state.firestore.ordered.entreprises
    }
}
export default compose(
    connect(mapStateToProps,null),
    firestoreConnect([
      { collection: 'entreprises'},
    ]))(Entreprises);

