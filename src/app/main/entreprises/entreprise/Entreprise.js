import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import {withStyles, Button,Typography} from '@material-ui/core';
import {FuseAnimate, FusePageCarded} from '@fuse';
import { connect } from 'react-redux';
import _ from '@lodash';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

import {
    addEntreprise,
    getAllEntreprises,
    UpadateSearchEntreprise,
    updateEntreprise} from '../../../store/actions/entreprise/entreprise.action'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});



class Entreprise extends Component {

    state = {
        name_entreprise:'',
        email_entreprise:'',
        activity_entreprise:'',
        type_entreprise:'',
        adress_entreprise:'',
        postal_entreprise:'',
        city_entreprise:'',
        country_entreprise:'',
      };

    componentDidMount()
    {
        this.updateEntrepriseState();
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        console.log('url',this.props.location)
        console.log('url1',prevProps.location)
        if ( !_.isEqual(this.props.location, prevProps.location) )
        {
            this.updateEntrepriseState();
        }
    }

    

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        })
     }

    updateEntrepriseState = () => {

        const params = this.props.match.params;
        const {entrepriseId} = params;
    
        if ( entrepriseId === 'new' )
        { 
           this.setState({
            name_entreprise:'',
            email_entreprise:'',
            activity_entreprise:'',
            type_entreprise:'',
            adress_entreprise:'',
            postal_entreprise:'',
            city_entreprise:'',
            country_entreprise:''
           })
        }
        else
        {
            const {entrepriseItem} = this.props
            this.setState({
                name_entreprise:entrepriseItem.name_entreprise,
                email_entreprise:entrepriseItem.email_entreprise,
                activity_entreprise:entrepriseItem.activity_entreprise,
                type_entreprise:entrepriseItem.type_entreprise,
                adress_entreprise:entrepriseItem.adress_entreprise,
                postal_entreprise:entrepriseItem.postal_entreprise,
                city_entreprise:entrepriseItem.city_entreprise,
                country_entreprise:entrepriseItem.country_entreprise,
                identfiant:entrepriseId
               })
        }
    };

    handleSubmit =(e) => {
        this.props.addEntreprise(this.state)

        this.setState({
            name_entreprise:' ',
            email_entreprise:' ',
            activity_entreprise:' ',
            type_entreprise:' ',
            adress_entreprise:' ',
            postal_entreprise:' ',
            city_entreprise:' ',
            country_entreprise:' '
          })
    }

    handleUpdateEntreprise = (event) =>{
        this.props.updateEntreprise(this.state)
        this.setState({
            name_entreprise:' ',
            email_entreprise:' ',
            activity_entreprise:' ',
            type_entreprise:' ',
            adress_entreprise:' ',
            postal_entreprise:' ',
            city_entreprise:' ',
            country_entreprise:' '
          })
    }

    render()
    {
        const { classes } = this.props;
        //console.log(this.props.entreprises)
        return (
            <FusePageCarded
                classes={{
                    toolbar: "p-0",
                    header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
                }}
                header={
                    <div className="flex flex-1 w-full items-center justify-between">

                    <div className="flex flex-col items-start max-w-full">
                        <div className="flex items-center max-w-full">
                            
                            <div className="flex flex-col min-w-0">
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography className="text-16 sm:text-20 truncate">
                                        {this.state.name_entreprise ? this.state.name_entreprise : 'Entreprise'}
                                    </Typography>
                                </FuseAnimate>
                            </div>
                        </div>
                    </div>
                    <FuseAnimate animation="transition.slideRightIn" delay={300}>
                    {
                        this.props.match.params.entrepriseId === 'new' ? (
                            <Button
                            className="whitespace-no-wrap"
                            variant="contained"
                           // disabled={!this.canBeSubmitted()}
                           // onClick={() => saveProduct(form)}
                           onClick={this.handleSubmit }
                        >
                            Save
                        </Button>
                        ) : (
                            <Button
                            className="whitespace-no-wrap"
                            variant="contained"
                           // disabled={!this.canBeSubmitted()}
                           // onClick={() => saveProduct(form)}
                           onClick={this.handleUpdateEntreprise  }
                        >
                            Update
                        </Button>
                        )
                        
                    }
                        
                    </FuseAnimate>
                </div>
                }
                contentToolbar={
                    <div className="px-24"><h4>New entreprise</h4></div>
                }
                content={
                    <div className="p-24">
                        <form className={classes.container} noValidate autoComplete="off">

                            <TextField
                               id="name_entreprise"
                               value={this.state.name_entreprise}
                               label="Name"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               Width
                            />

                            <TextField
                               id="email_entreprise"
                               value={this.state.email_entreprise}
                               label="Email"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               maxWidth
                            />                      

                            <TextField
                               id="activity_entreprise"
                               value={this.state.activity_entreprise}
                               label="Type of activity"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               Width
                            />  

                            <TextField
                               id="type_entreprise"
                               value={this.state.type_entreprise}
                               label="Type of entreprise"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               Width
                            /> 

                            <TextField
                               id="adress_entreprise"
                               value={this.state.adress_entreprise}
                               label="Adress"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               fullWidth
                            />

                           <TextField
                               id="postal_entreprise"
                               value={this.state.postal_entreprise}
                               label="Postal"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               fullWidth
                            />

                            <TextField
                               id="city_entreprise"
                               value={this.state.city_entreprise}
                               label="City"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               fullWidth
                            />                       

                           <TextField
                               id="country_entreprise"
                               value={this.state.country_entreprise}
                               label="Country"
                               className={classes.textField}
                               margin="normal"
                               variant="outlined"
                               onChange={this.handleChange}
                               fullWidth
                            /> 
    
                        </form>
                    </div>
                }
            />
        )
    }
}


Entreprise.propTypes = {
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
    return {
        entreprisesAll: state.entrepriseReducer.allEntreprises,
        entrepSearch: state.entrepriseReducer.search,
        entrepriseItem: state.entrepriseReducer.entrepriseItem,
        entreprises: state.firestore.ordered.entreprises
    }
}
   
const mapDispatchToProps = (dispatch) => {
    return {
        getAllEntreprises: () => dispatch(getAllEntreprises()),
       addEntreprise: (entreprise) => dispatch(addEntreprise(entreprise )),
       UpadateSearchEntreprise: () =>dispatch(UpadateSearchEntreprise()),
       updateEntreprise:(entreprise) => dispatch(updateEntreprise(entreprise)),
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
      { collection: 'entreprises'},
]))(withStyles(styles)(Entreprise));

//export default withStyles(styles)(Entreprise);