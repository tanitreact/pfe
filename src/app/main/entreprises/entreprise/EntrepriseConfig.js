// import React from 'react';
import {FuseLoadable} from '@fuse';
//import {Redirect} from 'react-router-dom';

export const EntrepriseConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/gestion-session/entreprises',
            component: FuseLoadable({
                loader: () => import('./Entreprises')
            })
        },
        {
            path     : '/apps/gestion-session/entreprise/:entrepriseId/:name?',
            component: FuseLoadable({
                loader: () => import('./Entreprise')
            })
        },
    ]
};