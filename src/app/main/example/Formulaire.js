import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import { connect } from 'react-redux';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

import {addEntreprise} from '../../store/actions/entreprise/entreprise.action'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    margin: theme.spacing.unit,
  },
});



class Formulaire extends Component {

 state={
     name_entreprise : '',
 }   
  
  handleChange =  event => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  handleSubmit = (event) =>{
      this.props.addEntreprise(this.state.name_entreprise)
  }

  render(){
    const { classes} = this.props;
    console.log(this.props.entreprises)
    console.log(this.state.name_entreprise)

    // if (entreprises){
    //     //console.log('if',entreprises)
    //    const liste= entreprises.map( (chat) =>
    //                 <p>{chat.entreprise}</p>
    //     );
    //       return(
    //           <div>{liste}</div>
    //       )
    //    }
    
    
    return (
        <div className={classes.container}>
          <Input
            id='name_entreprise'
            className={classes.input}
            inputProps={{
              'aria-label': 'Description',
            }}
            onChange={this.handleChange}
          />
          <Button 
            variant="contained"
            className={classes.button}
            onClick={this.handleSubmit}
          >
              addd
            </Button>
        </div>
      );
  }
}

Formulaire.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    console.log(state)
    return {
        //entreprises: state.entrepriseReducer.entreprises,
        entreprises: state.firestore.ordered.entreprises
    }
}
   
const mapDispatchToProps = (dispatch) => {
    return {
      addEntreprise: (entreprise) => dispatch(addEntreprise(entreprise))
    }
}
  
// export default compose(
//     connect(mapStateToProps, mapDispatchToProps),
//     firestoreConnect([
//       { collection: 'entreprises'},
//     ]))(withStyles(styles)(Formulaire));
   
//export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(Formulaire));
export default withStyles(styles)(Formulaire);