import * as reduxModule from 'redux';
import {applyMiddleware, compose, createStore} from 'redux';
import createReducer from './reducers';
import thunk from 'redux-thunk';
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase} from 'react-redux-firebase';

//import 'bootstrap/dist/css/bootstrap.min.css';

//import '../../styles/index.css';

import firebase from '../firebase/fbConfig'


/*
Fix for Firefox redux dev tools extension
https://github.com/zalmoxisus/redux-devtools-instrument/pull/19#issuecomment-400637274
 */
reduxModule.__DO_NOT_USE__ActionTypes.REPLACE = '@@redux/INIT';

const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

// const enhancer = compose(
//     applyMiddleware(thunk.withExtraArgument(getFirebase,getFirestore)),
//     reactReduxFirebase(firebase),
//     reduxFirestore(firebase),
    
// );

//const reactReduxFirebaseMiddleware = reactReduxFirebase(firebase, {enableRedirectHandling: false});

//const authIsReadypromise = reactReduxFirebaseMiddleware.authIsReady; // THIS IS THE BUILT-IN PROMISE


const store = createStore(createReducer(),
 composeEnhancers(
        applyMiddleware(
            thunk.withExtraArgument( {getFirestore,getFirebase})
        ),
        reduxFirestore(firebase),
       // reactReduxFirebase(firebase,{attachAuthIsReady : true}),
     //   reactReduxFirebaseMiddleware,
        
    )
 );

store.asyncReducers = {};

export const injectReducer = (key, reducer) => {
    if ( store.asyncReducers[key] )
    {
        return;
    }
    store.asyncReducers[key] = reducer;
    store.replaceReducer(createReducer(store.asyncReducers));
    return store;
};

export default store;
