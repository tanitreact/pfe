import _ from '@lodash';


const initialState = {
    
    todoDialog     : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    }
};

const sessionReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        
        
        case 'OPEN_NEW_SESSION_DIALOG':
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        case 'CLOSE_NEW_SESSION_DIALOG':
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        default:
            return state;
    }
};

export default sessionReducer;
