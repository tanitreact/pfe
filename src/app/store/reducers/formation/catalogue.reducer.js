

const initialState = {
    contactDialog     : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    },
    formationDialog    : {
        props: {
            open: false
        },
        data : null
    },
    formation : '',
    courseItem: '',
    formationItem:''
}


const catalogueReducer = ( state = initialState, action ) =>{

    if(action.type === 'ADD_CATALOGUE_STARTED'){
        console.log('started')
    }

    if(action.type === 'ADD_CATALOGUE_SUCCES'){
        console.log('ended')
    }

    if(action.type === 'OPEN_EDIT_FORMATION'){
       
            return {
                ...state,
            formationDialog : {
                    props: {
                        open: true
                    },
                    data : null
                }
            };
    }

    if(action.type === 'CLOSE_EDIT_FORMATION')
        {
            return {
                ...state,
                formationDialog: {
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
    if(action.type === 'COURSE_ITEM'){
       
        return {
            ...state,
            courseItem : action.payload
        };
    }

    if(action.type === 'HANDLE_TAKE_ITEM'){

        return{
            ...state,
            formationItem:action.payload
        }
    }
    if(action.type === 'OPEN_NEW_CONTACT_DIALOG'){
        return {
            ...state,
            contactDialog: {
                type : 'new',
                props: {
                    open: true
                },
                data : null
            }
        };
    }
    if(action.type === 'CLOSE_NEW_CONTACT_DIALOG'){

        return {
            ...state,
            contactDialog: {
                type : 'new',
                props: {
                    open: false
                },
                data : null
            }
        };
    }
    if(action.type === 'FORMATION_ITEM'){
        console.log('appa',action.payload)
        return {
            ...state,
            formation : action.payload
        };
    }
    if(action.type === 'NEW_FORMATION'){
        console.log('appa',action.payload)
        return {
            ...state,
            formation : action.payload
        };
    }
    return state;
}

export default catalogueReducer;