const initialState = {
    todoDialog    : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    },
    subTrainigItem : ''
}


const subTrainingReducer = ( state = initialState, action ) =>{

    if(action.type === 'HANDLE_TAKE_ITEM'){
        return{
            ...state,
        subTrainigItem:action.payload
        }
    }
    if(action.type === 'OPEN_EDIT_SUB_DIALOG'){
        console.log('open edit')
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
    }
    if(action.type === 'OPEN_NEW_SUB_DIALOG') 
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_NEW_SUB_DIALOG')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_EDIT_SUB_DIALOG')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
    return state;
}

export default subTrainingReducer;