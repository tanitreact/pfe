const initialState = {
    todoDialog    : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    },
    subTrainigItem : ''
}


const usersReducer = ( state = initialState, action ) =>{

    if(action.type === 'HANDLE_TAKE_ITEM'){
        return{
            ...state,
        subTrainigItem:action.payload
        }
    }
    if(action.type === 'OPEN_EDIT_USER'){
        console.log('open edit')
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
    }
    if(action.type === 'OPEN_NEW_USER') 
        {
            console.log('reducers')
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_NEW_USER')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_EDIT_USER')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
    return state;
}

export default usersReducer;