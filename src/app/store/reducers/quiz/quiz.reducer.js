const initialState = {
    todoDialog    : {
        type : 'edit',
        props: {
            open: false
        },
        data : null
    },
    question : ''
}


const quizReducer = ( state = initialState, action ) =>{

    if(action.type === 'HANDLE_TAKE_ITEM'){
        return{
            ...state,
        question:action.payload
        }
    }
    if(action.type === 'OPEN_EDIT_QUESTION'){

            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
    }

    if(action.type==='CLOSE_EDIT_QUESTION')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
    }
    if(action.type === 'HANDLE_TAKE_ITEM'){
        return{
            ...state,
        question:action.payload
        }
    }
    return state;
}

export default quizReducer;