import {combineReducers} from 'redux';
import fuse from './fuse';
import auth from 'app/auth/store/reducers';
import quickPanel from 'app/fuse-layouts/shared-components/quickPanel/store/reducers';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';
import login from 'app/auth/store/reducers/login.reducer'
import register from 'app/auth/store/reducers/register.reducer'

import entrepriseReducer from './entreprise/entreprise.reducer'
import catalogueReducer from './formation/catalogue.reducer'
import subTrainingReducer from './formation/sous.formation.reducers'
import usersReducer from './users/users.reducer'
import sessionReducer from './session/session.reducer'
import userSessionReducer from './session/user.session.reducer'
import quizReducer from './quiz/quiz.reducer'

const createReducer = (asyncReducers) =>
    combineReducers({
        auth,
        login,
        register,
        fuse,
        quickPanel,
        ...asyncReducers,
        firestore: firestoreReducer,
        firebase: firebaseReducer,

        entrepriseReducer,
        catalogueReducer,
        sessionReducer,
        subTrainingReducer,
        usersReducer,
        sessionReducer,
        userSessionReducer,
        quizReducer
    });

export default createReducer;
