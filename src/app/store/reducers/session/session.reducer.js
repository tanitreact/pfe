const initialState = {
    todoDialog    : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    },
    sessionDialog    : {
        type : 'new',
        props: {
            open: false
        },
        data : null
    },
    sessionItem : '',
    sessionDetail : ''
   // subTrainigItem : ''
}


const sessionReducer = ( state = initialState, action ) =>{


    if(action.type === 'OPEN_EDIT_SESSION'){
        console.log('open edit')
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
    }
    if(action.type === 'OPEN_NEW_SESSION') 
        {
          
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_NEW_SESSION')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'new',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_EDIT_SESSION')
        {
            return {
                ...state,
                todoDialog: {
                    type : 'edit',
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        if(action.type === 'OPEN_NEW_DIALOG_SESSION') 
        {
            return {
                ...state,
                sessionDialog: {
                    props: {
                        open: true
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_NEW_DIALOG_SESSION')
        {
            return {
                ...state,
                todoDialog: {
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        if(action.type==='CLOSE_EDIT_DIALOG_SESSION')
        {
            return {
                ...state,
                sessionDialog: {
                    props: {
                        open: false
                    },
                    data : null
                }
            };
        }
        
        
        if(action.type === 'SESSION_ITEM'){
            return {
                ...state,
                sessionItem : action.payload
            };
        }

        // if(action.type === 'SESSION_DETAIL'){
       
        //   //  console.log('payload',action.payload)
        //     return {
        //         ...state,
        //         sessionDetail : action.payload
        //     };
        // }
        // if(action.type === 'GET_ALL_USER_SESSION_SUCCES'){
       
        //       console.log('payload',Object.values(action.payload))
        //     //   return {
        //     //       ...state,
        //     //       sessionDetail : action.payload
        //     //   };
        //   }
    return state;
}

export default sessionReducer;