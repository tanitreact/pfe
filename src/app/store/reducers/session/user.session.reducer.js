const initialState = {

    sessionItem : ' ',
    sessionDetail : ' ',
    userSessions : []
   // subTrainigItem : ''
}


const userSessionReducer = ( state = initialState, action ) =>{

        if(action.type === 'SESSION_DETAIL'){
       
          //  console.log('payload',action.payload)
            return {
                ...state,
                sessionDetail : action.payload
            };
        }
        if(action.type === 'GET_ALL_USER_SESSION_SUCCES'){
       
              let liste =Object.values(action.payload)
              return {
                  ...state,
                  userSessions :Object.values(liste)
              };
          }
    return state;
}

export default userSessionReducer;