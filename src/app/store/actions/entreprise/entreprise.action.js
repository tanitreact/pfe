import firestore from '../../../firebase/fbConfig';


 export const addEntreprise = (entreprise) => {

    return (dispatch, getState, getFirestore ) => {
         
         //const firestore = getFirestore();
         firestore.firestore().collection('entreprises').add({
                    ...entreprise,
                    createdAt: new Date()
        })
           .then(() => {
                dispatch({
                    type: "ADD_ENTREPRISE",
                })
            })
        }
}

export const removeEntreprise = (id) =>{
    return(dispatch,getFirestore ) =>{
        //const firestore = getFirestore();
      if( id !== null){
        firestore.firestore().collection('entreprises').doc(id)
         .delete()
         .then(()=>{
            console.log('removed from the list')
         })
         .then(() => {
           dispatch({
             type: "REMOVE_ENTREPRISE",
             id 
            })
           })
         }
     } 
  }
 
export const UpadateSearchEntreprise = entreprise => ({
	type: 'UPDATE_FIND_ENTREPRISE',
	payload: entreprise 
});


let allEntreprises=[]

export const getAllEntreprises = () =>{
    return(dispatch) =>{
        firestore.firestore().collection("entreprises")
        .get()
        .then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
          const entrep={
             ...doc.data(),
              id:doc.id
          }
           allEntreprises.push(entrep) 
               // console.log(doc.id, " => ", doc.data());
      });
         dispatch(getAllEntreprisesSuccess(allEntreprises)); 
           return allEntreprises;
                // dispatch({
                //     type: "GET_ALL_ENTREPRISES",
                //     payload :allEntreprises
                //    })
        })
        .catch(function(error) {
          console.error("Error writing document: ", error);
      });
     } 
  }


export const getAllEntreprisesSuccess = allEntreprises => ({
    type: 'GET_ALL_ENTREPRISES_SUCCESS',
    payload: allEntreprises
  });

  export const setEntreprisesSearchText =(event)=>
  {
       return(
         {
          type      : 'SET_ENTREPRISES_SEARCH_TEXT',
          searchText: event.target.value
         }
       )        
  }



// export function EditEntrepriseItem(entreprise){

//   return(dispatch)=>{
//     dispatch({
//       type: "EDIT_ENTREPRISE_ITEM",
//       entreprise
//     })
//   }
// }

export function EditEntrepriseItem(entreprise){
  return{
      type: "EDIT_ENTREPRISE_ITEM",
      entreprise
  }
}

export const updateEntreprise =(entreprise)=>{

  return(dispatch)=>{   
  
    firestore.firestore().collection('entreprises').doc(entreprise.identfiant)
     .update(
       {
         ...entreprise,
       }
      )
     .then(() =>{ 
       dispatch({
         type:'UPDATE_ENTREPRISE',  
       })  
      }
      )
 }
}