import firestore from '../../../firebase/fbConfig';
import firebase from '../../../firebase/fbConfig';
import store from 'app/store';
import * as Actions from 'app/store/actions';
import firebaseSecond from '../../../firebase/fbConfigSecond';
/**add user */

var actionCodeSettings = {
    url: "http://localhost:3000",
    // This must be true.
    handleCodeInApp: true
  };



export const inviteUser=(model) =>{

    const {name,email_user,role} = model;
    const password = '12345678'

    console.log('invitation',model)
    return(dispatch,getState)=>{

        const organism = getState().firebase.profile.organism || ''
        firebaseSecond.auth().createUserWithEmailAndPassword(email_user, password)
        .then((response) => {    

            if(response.user){

                firebaseSecond.auth().currentUser.sendEmailVerification(actionCodeSettings)
                .then(()=>{
                    firestore.firestore().collection('users').doc(response.user.uid).set({
                        displayName : name,
                        createdAt: new Date(),
                        emailVerified:response.user.emailVerified,
                        uid:response.user.uid,
                        //first_name,
                        email_user:email_user,
                        name:name,
                        role:role,
                        organism,
                        invitation_status:response.user.emailVerified
                  })
                })
            }
        }).then(()=>{
            dispatch({
                type: 'INVITATION_SUCCESS',
        }); 
        }).then((error)=>{
            if(error){
                store.dispatch(Actions.showMessage({message: "Invitation non envoyée"}));
            }
            if(!error){
                store.dispatch(Actions.showMessage({message: "Invitation envoyée"}));
            }
        
        })
    }
}


export const editUser = () =>{
   const editItem =this.handleTakeSubTraining()
 
}

/**retreieve item */
export const handleTakeUser=(user)=>{
   
    return {
        type:'HANDLE_TAKE_ITEM',
        payload:user
    }
}

/**
 * open user popup
 */
export function openEditUser()
{
   
    return {
        type: 'OPEN_EDIT_USER',
    }
}

export function closeNewUser()
{
    return {
        type: 'CLOSE_NEW_USER'
    }
}

/**
 * close user popup
 */
export function closeEditUser()
{
    return {
        type: 'CLOSE_EDIT_USER'
    }
}

export function openNewUser()
{
    console.log('action')
    return {
        type: 'OPEN_NEW_USER'
}
}


/**remove */



export const removeUser = (id) =>{

    firestore.firestore().collection('users').doc(id)
    .delete() 
    .then((error)=>{
        if(!error){
             store.dispatch(Actions.showMessage({message: "Cet utilisateur  a été supprimée"}));
        }
     })    
}

  /**update */

  export const updateAdd =(user)=>{

    const {name,email_user,role}= user
    return(dispatch,getState)=>{     
      firestore.firestore().collection('users').doc(user.uid)
       .update(
         {
            name,
            email_user,
            role
         }
        )
       .then(() =>{ 
         dispatch({
           type:'UPDATE_SUB_TRAINING',  
         })  
        }
        )
        .then((error)=>{
            if(!error){
                 store.dispatch(Actions.showMessage({message: "utilisateur "+name+" a été modifié"}));
            }
         }) 
   }
  }