
import firestore from '../../../firebase/fbConfig';
import firebase from '../../../firebase/fbConfig';
import * as Actions from 'app/store/actions';
import store from 'app/store';
import history from 'history.js';
import { func } from 'prop-types';
/**
 * add a session
 */


export const addSession=(session) =>{

    const {formationId,startDate,dueDate,address,place,nbre_place,theme,participant,formateur} = session
   // console.log('formation',title)
    return(dispatch,getState)=>{
        firestore.firestore().collection('sessions').add({
            formationId,
            theme,
            // hours,
            // days,
            startDate,
            dueDate,
            address,
            place,
            nbre_place,
            participant,
            formateur
         })
        .then(function(docRef){
            var sessionAddId =firestore.firestore().collection("sessions").doc(docRef.id)
            sessionAddId.update({
            sessionId: docRef.id
           })
        })
        .then(()=>{
            return dispatch({
                type   : 'SESSION_ADDED',
            });
        })
        .then((error)=>{
            console.log(error)
        })
        .then(() => {
            store.dispatch(Actions.showMessage({message: "session ajoutée"}));
        })
    }
    }


/**
 * open session popup
 */
export function openEditSession()
{
   
    return {
        type: 'OPEN_EDIT_SESSION',
    }
}

export function closeNewSession()
{
    return {
        type: 'CLOSE_NEW_SESSION'
    }
}

/**
 * close session popup
 */
export function closeEditSession()
{
    return {
        type: 'CLOSE_EDIT_SESSION'
    }
}

export function openNewSession()
{

    return {
        type: 'OPEN_NEW_SESSION'
}
}

export function openNewDialogSession()
{
    return {
        type: 'OPEN_NEW_DIALOG_SESSION'
}
}

export function closeNewDialogSession()
{
    return {
        type: 'CLOSE_NEW_DIALOG_SESSION'
    }
}

export function closeEditDialogSession()
{
    return {
        type: 'CLOSE_EDIT_DIALOG_SESSION'
    }
}

/**
 * 
 * delete session
 */

export function handleDeleteSession(session){
    return(dispatch)=>{
        firestore.firestore().collection('sessions').doc(session.id)
        .delete() 
        .then((error)=>{
           if(!error){
                store.dispatch(Actions.showMessage({message: "Cette session a été supprimée avec succès"}));
           }
        })
    }
       
}
/**
 * get session during the list click
 */

// export const getSessionClick =( id) =>{

//     return(dispatch)=>{
//         firestore.firestore().collection('sessions').doc(id)
//         .get()
//         .then((session)=>{
//             dispatch(getSessionTake(session.data()))
//         })
//     }
// }

// export function getSessionTake(session){
//     return (dispatch) => {
//         dispatch({
//             type   : 'SESSION_ITEM',
//             payload: session
//         })
//     }
// }

/**
 * take the formation item when click on the list
 */
export const getSession =(id) =>{

    //sessionStorage.setItem("formation",item);
    return(dispatch)=>{
        firestore.firestore().collection('sessions').doc(id)
        .get()
        .then((session)=>{
            dispatch(getSessionTake(session.data()))
        })
    }
}

export function getSessionTake(session){
    return (dispatch) => {
        dispatch({
            type   : 'SESSION_ITEM',
            payload: session
        })
    }
  //  console.log('apparament',formation)
}
