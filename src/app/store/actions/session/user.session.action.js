import firestore from '../../../firebase/fbConfig';
import firebase from '../../../firebase/fbConfig';
import * as Actions from 'app/store/actions';
import store from 'app/store';
import history from 'history.js';
import { func } from 'prop-types';


/**
 * get session when click on detail
 */
export function getDetailSession (session){

    return (dispatch) => {
        dispatch({
            type   : 'SESSION_DETAIL',
            payload: session
        })
    }
}


/**
 * add a participant in the session list
 */

export const addParticipant = (session_id) =>{

    console.log('session the action ',session_id)
    var user = firebase.auth().currentUser;
    return(dispatch,getState)=>{
       // firestore.firestore().collection('sessions').add({})
        //firestore.firestore().collection("sessions").where("email_user", "==", email).get()
        firebase.firestore().collection('sessions').doc(session_id)
             .set(
                   { participant: [user] },
                    { merge: true }
                  ).then((error)=>{
                    if(!error){
                          {history.push('/app/mescours')}
                    }
                   })
        // .then(()=>{
        //     return dispatch({
        //         type   : 'SESSION_ADDED',
        //     });
        // })
    }
}

/**
 *  get current user sessions
 */

 export const currentUserSession = (userId) => {
    return(dispatch,getState)=>{
    // var user = firebase.auth().currentUser;
    // const userId = getState().firebase.auth.uid
    dispatch(getUserAllSessionsStart());
	//await sleep(200000);
    firebase.firestore().collection('sessions')
            .where("participant", "array-contains",userId)
            .get().then(function(querySnapshot) {
                let userSessions = [];
                let i = 0
                querySnapshot.forEach(function(doc) {
                    userSessions = {
                        ...userSessions,
                        [i++]: {...doc.data() } 
                    };
                });
                dispatch(getAllUserSessionsSuccess(userSessions));
			        return userSessions ;
            })
    }
 }

export const getAllUserSessionsSuccess = userSessions =>{
    return(
            {type: 'GET_ALL_USER_SESSION_SUCCES',
             payload: userSessions}
    )
}

export const getUserAllSessionsStart = () => {
    return(
        {
            type: 'GET_ALL_USER_SESSION_START',
         }
)
};

/**
 * selected item for detail
 */
export function getSelectedItem(session){

    return (dispatch) => {
        dispatch({
            type   : 'SESSION_DETAIL',
            payload: session
        })
    }
}