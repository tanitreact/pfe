import firestore from '../../../firebase/fbConfig';


/**add sub training */

export const addSubTraining=( subTraining) =>{

    const {training_title,sub_training_title,difficulty,duration} = subTraining;

    return(dispatch,getState)=>{
        firestore.firestore().collection('sousformations').add({
            training_title,
            sub_training_title,
            difficulty,
            duration,
            createdAt: new Date(),
         })
        .then(function(docRef) {
            var subTraninigAddId =firestore.firestore().collection("sousformations").doc(docRef.id)
            subTraninigAddId.update({
                subTraninigAddId: docRef.id
             })
        })
        .then((error)=>{
           console.log('erro',error)
        })
    }
}

/**retreieve item */
export const editSubTraining = () =>{
   const editItem =this.handleTakeSubTraining()
   console.log('editItem',editItem)
}

export const handleTakeSubTraining=(subTraining)=>{
    return {
        type:'HANDLE_TAKE_ITEM',
        payload:subTraining
    }
}

export function openEditSubTraining()
{
    console.log('action')
    return {
        type: 'OPEN_EDIT_SUB_DIALOG',
    }
}

export function closeNewSubTraining()
{
    return {
        type: 'CLOSE_NEW_SUB_DIALOG'
    }
}

export function closeEditSubTraining()
{
    return {
        type: 'CLOSE_EDIT_SUB_DIALOG'
    }
}

export function openNewSubTraining()
{
    return {
        type: 'OPEN_NEW_SUB_DIALOG'
}
}


/**remove sub training */



export const removeSubTraining = (id) =>{

        firestore.firestore().collection('sousformations').doc(id)
        .delete()
        .then(()=>{
           console.log('removed from the list')
        }) 
        
}

  /**update */

  export const updateSubTraining =(subTraining)=>{

    const {training_title,sub_training_title,difficulty,duration}= subTraining
    return(dispatch)=>{   
    
      firestore.firestore().collection('sousformations').doc(subTraining.subTraninigAddId)
       .update(
         {
            training_title,
            sub_training_title,
            difficulty,
            duration
         }
        )
       .then(() =>{ 
         dispatch({
           type:'UPDATE_SUB_TRAINING',  
         })  
        }
        )
   }
  }