import firestore from '../../../firebase/fbConfig';
import * as Actions from 'app/store/actions';
import store from 'app/store';
import {FuseUtils} from '@fuse';



export const addFormation=(formation) =>{

    const {title,description,chapitres,statut} = formation.form
   // console.log('formation',title)
    return(dispatch,getState)=>{

        const organism = getState().firebase.profile.organism || ''
        const createdByName = getState().firebase.profile.name || ''
        const createdByMail = getState().firebase.profile.email_user || ''

        firestore.firestore().collection('formations').add({
            title ,
            description,
            chapitres,
            statut ,
            createdAt: new Date(),
            organism,
            createdBy:{
                createdByName,
                createdByMail
            }

         })
        .then(function(docRef){
            var formationAddId =firestore.firestore().collection("formations").doc(docRef.id)
            formationAddId.update({
                formationId: docRef.id
           })
        })
        .then(()=>{
            return dispatch({
                type   : 'FORMATION_ADDED',
            });
        })
        .then((error)=>{
            console.log(error)
        })
        .then(() => {
            store.dispatch(Actions.showMessage({message: "Formation ajoutée"}));
        })
    }
    }

    export const handleGetItem =(id)=>{

        return(dispatch)=>{
            firestore.firestore().collection("formations").doc(id)
           .get()
           .then((doc)=>{
            console.log("Document data:", doc.data());
           }).catch((error)=>{
            console.log("Error getting document:", error);
            })
        }
    }
export const formationItem = (formation) =>{
    return(dispatch)=>{
        dispatch({
            type:'FORMATION_ITEM',
            payload: formation
        })
    }
}

export const getCourse =( id) =>{

    return(dispatch)=>{
        firestore.firestore().collection('formations').doc(id)
        .get()
        .then((course)=>{
            dispatch(getCourseTake(course.data()))
        })
    }
}

export const getCourseTake =(course) =>{

    return (dispatch) => {
        dispatch({
            type   : 'COURSE_ITEM',
            payload: course
        })
    }
}

export const addCatalogueStarted = () => ({

    type: 'ADD_CATALOGUE_STARTED',

});

export const addCatalogueSuccess = catalogue => ({

    type: 'ADD_CATALOGUE_SUCCESS',
    payload : catalogue

});

export function openNewContactDialog()
{
    return {
        type: 'OPEN_NEW_CONTACT_DIALOG'
    }
}

export function closeNewContactDialog()
{
    return {
        type: 'CLOSE_NEW_CONTACT_DIALOG'
    }
}

/**retreieve item */

export const handleTakeFormation=(item)=>{
  
    return {
        type:'HANDLE_TAKE_ITEM',
        payload:item
    }
}

/**
 * delete formation
 */
export const removeFormation = (id) =>{
   
    return(dispatch) =>{
        firestore.firestore().collection('formations').doc(id)
        .delete()
        .then((error)=>{
            if(!error){
                store.dispatch(Actions.showMessage({message: "formation supprimée"}));
            }
            return dispatch({
                type   : 'REMOVE_FORMATION_ERROR',
                payload : error
            })
        })
    }  
}
/**
 * open user popup
 */
export function openEditFormation()
{
    
    return {
        type: 'OPEN_EDIT_FORMATION',
    }
}

/**
 * close user popup
 */
export function closeEditFormation()
{

    return {
        type: 'CLOSE_EDIT_FORMATION'
    }
}

/**
 * take the formation item when click on the list
 */
export const getFormation =(id) =>{

    //sessionStorage.setItem("formation",item);
    return(dispatch)=>{
        firestore.firestore().collection('formations').doc(id)
        .get()
        .then((formation)=>{
            dispatch(getFormationTake(formation.data()))
        })
    }
}

export function getFormationTake(formation){
    return (dispatch) => {
        dispatch({
            type   : 'FORMATION_ITEM',
            payload: formation
        })
    }
  //  console.log('apparament',formation)
}

/**
 * new formation form
 */
export function newFormation()
{
    const form = {
        id              : FuseUtils.generateGUID(),
        title            :'',
        statut           :'',
        description     :'',
        chapitres      : [],
        image          : '',
    }
    return (dispatch) => {
        dispatch({
            type   : 'NEW_FORMATION',
            payload: form
        })
    }
}

/**
 * update
 */
export const updateFormation =(formation,id)=>{

    const {title,description,chapitres,statut} = formation.form

    // const organism = getState().firebase.profile.organism || ''
    // const createdByName = getState().firebase.profile.name || ''
    // const createdByMail = getState().firebase.profile.email_user || ''


    return(dispatch,getState)=>{   
    
      firestore.firestore().collection('formations').doc(id)
       .update(
         {
            title,description,chapitres,statut
         }
        )
       .then(() =>{ 
         dispatch({
           type:'UPDATE_FORMATION',  
         })  
        }
        )
        .then((error)=>{
            if(!error){
                 store.dispatch(Actions.showMessage({message: "La formation  a été modifié"}));
            }
         }) 
   }
  }

  /**
   * 
   * delete formation
   */
  export function supprimerFormation (id){
      //console.log('id formation',id)
    return(dispatch)=>{
        firestore.firestore().collection('formations').doc(id)
        .delete() 
        .then((error)=>{
           if(!error){
                store.dispatch(Actions.showMessage({message: "Cette formation a été supprimée avec succès"}));
           }
        })
    }
       
}