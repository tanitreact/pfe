

export function openNewSessionDialog()
{
    console.log('OPEN_NEW_SESSION_DIALOG')
    return {
        type: 'OPEN_NEW_SESSION_DIALOG'
    }
}

export function closeNewSessionDialog()
{
    return {
        type: 'CLOSE_NEW_SESSION_DIALOG'
    }
}

