import firestore from '../../../firebase/fbConfig';
import firebase from '../../../firebase/fbConfig';
import * as Actions from 'app/store/actions';
import store from 'app/store';

/**
 * 
 * add a question
 */

export const addQuestion=(item) =>{

    const {question,option1,option2,option3,correct} = item
    const listOption =[option1,option2,option3]
  
    return(dispatch,getState)=>{

        const organism = getState().firebase.profile.organism || ''
        const createdByName = getState().firebase.profile.name || ''
        const createdByMail = getState().firebase.profile.email_user || ''

        firestore.firestore().collection('questions').add({
         question:{
            question,
            listOption,
            correct
           },
            organism,
            createdByName,
            createdByMail,
            createdByMail
         })
        .then(function(docRef){
            var formationAddId =firestore.firestore().collection("questions").doc(docRef.id)
            formationAddId.update({
                questionId: docRef.id
           })
        })
        .then(()=>{
            return dispatch({
                type   : 'QUESTION_ADDED',
            });
        })
        .then(() => {
            store.dispatch(Actions.showMessage({message: "Formation ajoutée"}));
        })
    }
}

/**
 * open the windows on click
 */
export function openEditQuestion()
{ 
    return {
        type: 'OPEN_EDIT_QUESTION',
    }
}

/**
 * close session popup
 */
export function closeEditQuestion()
{
    return {
        type: 'CLOSE_EDIT_QUESTION'
    }
}


 /**
 * 
 * delete session
 */

export function handleDeleteQuuestion(question){
    return(dispatch)=>{
        firestore.firestore().collection('questions').doc(question.id)
        .delete() 
        .then((error)=>{
           if(!error){
                store.dispatch(Actions.showMessage({message: "Cette session a été supprimée avec succès"}));
           }
           if(error){
            store.dispatch(Actions.showMessage({message: "Erreur de suppression"}));
       }
        })
    }
       
}
/**
 * modifier
 */
export const handleTakeQuestion=(question)=>{
   
    console.log('uuu',question)
    return {
        type:'HANDLE_TAKE_ITEM',
        payload:question
    }
}