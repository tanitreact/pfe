const prodConfig = {
    // apiKey: "AIzaSyBMeQWT7BOZmyAbJrq3ipSzcB0J8YOMNgg",
    // authDomain: "reduxchat-9987b.firebaseapp.com",
    // databaseURL: "https://reduxchat-9987b.firebaseio.com",
    // projectId: "reduxchat-9987b",
    // storageBucket: "reduxchat-9987b.appspot.com",
    // messagingSenderId: "882367378637"
};

const devConfig = {
    // apiKey: "AIzaSyBMeQWT7BOZmyAbJrq3ipSzcB0J8YOMNgg",
    // authDomain: "reduxchat-9987b.firebaseapp.com",
    // databaseURL: "https://reduxchat-9987b.firebaseio.com",
    // projectId: "reduxchat-9987b",
    // storageBucket: "reduxchat-9987b.appspot.com",
    // messagingSenderId: "882367378637"
};

const config = process.env.NODE_ENV === 'production' ? prodConfig : devConfig;

export default config;
