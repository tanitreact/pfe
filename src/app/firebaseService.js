import config from './services/firebaseService/firebaseServiceConfig';
//import firebase from 'firebase/app';
//import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';
import firebase from './firebase/fbConfig';


class firebaseService {

    init()
    {
        if ( firebase.apps.length )
        {
            return;
        }
        firebase.initializeApp(config);
        this.db = firebase.database();
        this.auth = firebase.auth();
        firebase.firestore().settings();

    }

    getUserData = (userId) => {
        if ( !firebase.apps.length )
        {
            return;
        }
        return new Promise((resolve, reject) => {
            this.db.ref(`users/${userId}`)
                .once('value')
                .then((snapshot) => {
                    const user = snapshot.val();
                    resolve(user);
                });
        });
    };

    updateUserData = (user) => {
        // if ( !firebase.apps.length )
        // {
        //     return;
        // }
        // return this.db.ref(`users/${user.uid}`)
        //     .set(user);

        //     .then((response) => {    
        //         console.log(response.user)
        //             firestore.firestore().collection('users').doc(response.user.uid).set({
        //             displayName : name,
        //             createdAt: new Date(),
        //             uid:response.user.uid,
        //            // emailVerified:response.uid.emailVerified,
        //             first_name,
        //             name,
        //             role:'admin',
        //             organism,
        //             //emailVerfiied: response.user.emailVerfiied
        //          })
        //       })

        console.log('firebase',user)
    };

    // onAuthStateChanged = (callback) => {
    //     if ( !firebase.auth())
    //     {
    //         return;
    //     }
    //     firebase.auth().onAuthStateChanged(callback);
    // };

    signOut = () => {
        if ( !firebase.auth() )
        {
            return;
        }
        this.auth.signOut();
    }
}

const instance = new firebaseService();

export default instance;
