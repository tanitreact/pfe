import React, {Component} from 'react';
import {TextField, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar} from '@material-ui/core';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import _ from '@lodash';
import {Redirect} from 'react-router-dom';

import {closeRegisterDialog} from '../auth/store/actions/login.actions'



class DialogRegister extends Component {

    
    closeComposeDialog = () => {
        this.props.closeRegisterDialog();
    };
    
    redirectRegister = () =>{
            return(
                <Redirect to='/register'/>
            )
    }
    render()
    {
        const {registerDialog, addFormation} = this.props;

        return (
            <Dialog
                {...registerDialog.props}
                onClose={this.closeComposeDialog}
            >

                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                           Bonjour et Bienvenu
                        </Typography>
                    </Toolbar>
                </AppBar>

                <DialogContent classes={{root: "p-24"}}>
                      <h2>Ce compte n'est pas enregistré</h2>
                      <h2>Pour creer un nouveau un nouveau compte veuillez cliquer sur <span>Nouveau compte</span></h2>
                </DialogContent>
                    <DialogActions className="justify-between pl-10">
                        <Button 
                           component={Link} 
                           to='/register' 
                          variant="contained"
                          color="primary"
                          onClick={() => {
                            this.closeComposeDialog();
                        }}
                          >
                          <span className="hidden sm:flex">Nouveau compte</span>
                       </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={this.closeComposeDialog}
                        >
                            Annulez
                        </Button>
                    </DialogActions>
            </Dialog>
        );
    }
}


function mapDispatchToProps(dispatch)
{
    return {
        // closeEditContactDialog: Actions.closeEditContactDialog,
        closeRegisterDialog:() => dispatch(closeRegisterDialog()),
        
    }
}

function mapStateToProps(state)
{
    return {
        registerDialog: state.login.registerDialog
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DialogRegister);
