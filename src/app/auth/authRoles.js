/**
 * Authorization Roles
 */
const authRoles = {
    admin    :['admin','formateur'],
    formateur    : ['admin','formateur'],
    participant     : ['participant','formateur','admin'],
};

export default authRoles;
