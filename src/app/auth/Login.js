import React, {Component} from 'react';
import {withStyles, Button, Card, CardContent, Checkbox, Divider,  TextField, Typography,  Icon} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import {withRouter} from 'react-router-dom';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import _ from '@lodash';
import {connect} from 'react-redux';
import { compose} from 'redux';
import {Redirect} from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import InputAdornment from '@material-ui/core/InputAdornment';


import {submitLoginWithFireBase,
    authWithGoogle,openRegisterDialog,
    handleTakeEmail,
    openEntrepriseDialog,
    openLoginDialog} from './store/actions/login.actions'

import LoginDialog from './LoginDialog'
import firebase from '../firebase/fbConfig'
import DialogEntreprise from './DialogEntreprise';

const styles = theme => ({
    root: {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + darken(theme.palette.primary.dark, 0.5) + ' 100%)',
        color     : theme.palette.primary.contrastText
    }
});

class Login extends Component {

    state = {
        email   :'',
        password:'',
        remember: true,
        role :'',
        organism :'',
        emailError:'',
        passwordError:'',
    };
    

    handleChange = (event) => {
        this.setState(
            _.set({...this.state}, 
            event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value)
            );
    };

    /**
     * validation des donnees saisies
     */
    validate = () =>{
         let emailError ='';
         let passwordError ='';
         if(!this.state.password){
            passwordError='Le password ne doit pas etre vide'
          }
    
         if(!this.state.email.includes('@')){
             emailError='Email invalid'
         }
         if(emailError || passwordError){
             this.setState({
                emailError,
                passwordError 
             })
             return false
         }
        return true
    }
    
    userVerification = (event) =>{

         event.preventDefault(); 
            const isValid = this.validate();  
            if(isValid){
            //     this.props.inviteUser(this.state)
            var dataUser = this.props.users || []
            const {email,password} = this.state
            const users = dataUser.filter(item => item.email_user === email)
            const user = users[0]

            if(user){
                this.props.submitLoginWithFireBase(email,password)
                this.setState({
                    email   : '',
                    password: '',
                })
            } else{
                this.props.openLoginDialog()
            } 
            this.setState({
                email : '',
                password:'',
                passwordError:'',
                emailError:''
            })
            } 
    }
    

    render()
    {
        const {classes,auth,toFormation,error} = this.props;
        const {email, password,organism} = this.state;

       // console.log('role user',this.props.user)
       // console.log('username fail',error.username)

 
        return (
            <div className={classNames(classes.root, "flex flex-col flex-auto flex-no-shrink p-24 md:flex-row md:p-0")}>

                <div className="flex flex-col flex-no-grow items-center text-white p-16 text-center md:p-128 md:items-start md:flex-no-shrink md:flex-1 md:text-left">

                    <FuseAnimate animation="transition.expandIn">
                        <img className="w-128 mb-32" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Valeo_Logo.svg/1200px-Valeo_Logo.svg.png" alt="logo"/>
                    </FuseAnimate>

                    <FuseAnimate animation="transition.slideUpIn" delay={300}>
                        <Typography variant="h3" color="inherit" className="font-light">
                            GOGO APPLICATION
                        </Typography>
                    </FuseAnimate>
                </div>

                <FuseAnimate animation={{translateX: [0, '100%']}}>

                    <Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>

                        <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">

                            <Typography variant="h6" className="md:w-full mb-32">CONNECTEZ À VOTRE COMPTE</Typography>

                            <form name="loginForm" noValidate className="flex flex-col justify-center w-full">

                                <TextField
                                    className="mb-16"
                                    label="Email"
                                    autoFocus
                                    type="email"
                                    name="email"
                                    value={email}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>email</Icon>
                                          </InputAdornment>,
                                    }}
                                    error={this.state.emailError !=''}
                                    helperText={this.state.emailError}
                                />

                                <TextField
                                    className="mb-16"
                                    label="Mot de passe"
                                    type="password"
                                    name="password"
                                    value={password}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>vpn_key</Icon>
                                          </InputAdornment>,
                                         }}
                                    error={this.state.passwordError !=''}
                                    helperText={this.state.passwordError}
                                />
                                

                                <Button
                                    variant="contained"
                                    color="primary"
                                    className="w-full mx-auto mt-16"
                                    aria-label="LOG IN"
                                   // disabled={!this.canBeSubmitted()}
                                    onClick={this.userVerification}
                                >
                                    Se connecter
                                </Button>

                            </form>
                                
                            <div className="my-24 flex items-center justify-center">
                                <Divider className="w-32"/>
                                <span className="mx-8 font-bold">OR</span>
                                <Divider className="w-32"/>
                            </div>

                            <Button 
                              variant="contained" 
                              color="secondary" 
                              size="small"
                              className="normal-case w-192 mb-8"
                              onClick={()=>{
                                  this.props.authWithGoogle()
                              }}
                              >
                              
                                Se connecter avec google
                            </Button>

                            <div className="flex flex-col items-center justify-center pt-32 pb-24">
                                <span className="font-medium">Don't have an account?</span>
                                <Link className="font-medium" to="/register">Nouveau compte</Link>
                            </div>

                        </CardContent>
                    </Card>
                </FuseAnimate>
                <LoginDialog/>
                <DialogEntreprise/>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitLoginWithFireBase:(email,password) => dispatch(submitLoginWithFireBase(email,password)),
        authWithGoogle:() => dispatch(authWithGoogle()),
        openRegisterDialog:()=>dispatch(openRegisterDialog()),
        handleTakeEmail:()=>dispatch(handleTakeEmail()),
        openLoginDialog:()=>dispatch(openLoginDialog()),
        openEntrepriseDialog:()=>dispatch(openEntrepriseDialog())
    }
}

function mapStateToProps(state)
{
    console.log('best state',state)
    return {
        users: state.firestore.ordered.users,
        toFormation: state.login.toFormation,
        emailVerified : state.firebase.auth.emailVerified,
        auth : state.firebase.auth,
        organism : state.firebase.profile.organism,
        error: state.login.error, 
    }
}
//export default withStyles(styles, {withTheme: true})(Login);
export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
      { collection:'users'},
    ])
)(withStyles(styles,{withTheme: true})(Login));