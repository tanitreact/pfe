
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {closeEntrepriseDialog} from './store/actions/login.actions'

class DialogEntreprise extends React.Component {

//     closeComposeDialog = () => {
//         this.props.closeEntrepriseDialog();
//     };
//   state = {
//     open: false,
//   };

//   handleClickOpen = () => {
//     this.setState({ open: true });
//   };

  handleClose = () => {
    this.props.closeEntrepriseDialog();
  };

  render() {

    const {openDialog, addFormation} = this.props;

   
    return (
      <div>
       
        <Dialog
            open={openDialog}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Bonjour et bienvenue"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Votre compte n'est lieu a aucune entreprise
            </DialogContentText>
          </DialogContent>
          <DialogActions>
          <Button 
               //variant="contained"
               color="primary"
               onClick={this.handleClose}
            >
              Annulez
            </Button>
            
            <Button 
             component={Link} 
             to='/register-complement' 
            // variant="contained"
             color="primary"
             onClick={() => {
               this.handleClose();
             }}
              >
              ajouter une entreprise
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}


function mapDispatchToProps(dispatch)
{
    return {
        // closeEditContactDialog: Actions.closeEditContactDialog,
        closeEntrepriseDialog:() => dispatch(closeEntrepriseDialog()),
        
    }
}

function mapStateToProps(state)
{
    // console.log('hhhhhhhhh',state.login.openEntreprise)
    return {
        openDialog: state.login.openEntreprise
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DialogEntreprise);
//export default DialogEntreprise;