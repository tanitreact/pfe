import React, {Component} from 'react';
import history from 'history.js';
//mport * as userActions from 'app/auth/store/actions';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { compose} from 'redux';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import firebaseService from 'app/services/firebaseService';
import firestore from '../firebase/fbConfig';
import firebase from '../firebase/fbConfig';
import { withFirebase, isLoaded, isEmpty } from 'react-redux-firebase'

import {setUserDataFirebase} from 'app/auth/store/actions/user.actions'



class Auth extends Component {
    
    constructor(props)
    {
        super(props);
        this.firebaseCheck();
    }

    onAuthStateChanged = (callback) => {
        if ( !this.auth )
        {
            return;
        }
        this.auth.onAuthStateChanged(callback);
    };
    
    firebaseCheck = () => {
        
        if(!firebase.auth()){
            return;
        }
        firebase.auth().onAuthStateChanged(authUser => {

            // if((authUser===null || authUser===undefined || authUser==="")){
            //     history.push("/login");
            // }
            firestore.firestore().collection("users")
            .doc(authUser.uid)
            .get()
            .then((user)=>{
                if(authUser.emailVerified ===true){
                    this.props.setUserDataFirebase(user.data(),authUser);
                        //console.log('utilisateur existe ',user.data())    
                }
            })
        });
    };

    render()
    {
             
        const {children} = this.props;
            
       
        //console.log('children}',children.props.history.location.pathname)
        return (
            <React.Fragment>
                {children}
            </React.Fragment>
        );
    }
}

Auth.propTypes = {
    classes: PropTypes.object.isRequired,
    firebase: PropTypes.shape({
      login: PropTypes.func.isRequired
    }),
    auth: PropTypes.object
  };

const mapDispatchToProps = (dispatch) => {
    return {
        setUserDataFirebase:(user,auth) => dispatch(setUserDataFirebase(user,auth)),
        //showMessage:() => dispatch(showMessage()),
        //hideMessage:() => dispatch(hideMessage()),
        
    }
}
const mapStateToProps = (state) => {
    return{
        authVerif : state.firebase
    }
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    
)(Auth);