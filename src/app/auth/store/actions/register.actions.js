
import * as Actions from 'app/store/actions';
//import jwtService from 'app/services/jwtService';
import history from 'history.js';

import firebase from '../../../firebase/fbConfig';
import firestore from '../../../firebase/fbConfig';


var actionCodeSettings = {
    url: "https://reduxchat-9987b.firebaseapp.com",
    // This must be true.
    handleCodeInApp: true
  };

  /**
   * create a user
   * Mail verification
   * save user database
   */
export function registerWithFirebase(model)
{
    const {email, password,name,first_name,organism} = model;
    return (dispatch,getState) =>{

        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {  
            if(response.user){  
                history.push('/mailconfirm')
             firebase.auth().currentUser.sendEmailVerification(actionCodeSettings)
            .then(()=>{
                   firestore.firestore().collection('users').doc(response.user.uid).set({
                   displayName : name,
                   createdAt: new Date(),
                   emailVerified:response.user.emailVerified,
                   uid:response.user.uid,
                   first_name,
                   email_user:email,
                   name,
                   organism,
                   role:'admin',
                   invitation_status:response.user.emailVerified
                 })
               })
            // .then((error)=>{
            //     if(!error & response.user.emailVerified){history.push('./register-complement')}
            // })
               .then(()=>{
                dispatch({
                    type: 'REGISTER_SUCCESS',
                });
            })
                   
         }}) 
        // .then(()=>{
        //     firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)
        //      .then(function() {
        //        window.localStorage.setItem('emailForSignIn', email);
        //     })
        // })            
        .then((error)=>{
            console.log('error type',error)
        })

           
    }
}


export const registerEntreprise=( entreprise, admin) =>{

    const {name_entreprise,adress_entreprise,postal_code,type_organism,city,email_entreprise} = entreprise;
    return(dispatch,getState)=>{
        const adminId = getState().firebase.auth.uid
        const role='admin'
        firestore.firestore().collection('entreprises').add({
            name_entreprise,
            email_entreprise,
            adress_entreprise,
            city,
            postal_code,
            type_organism,
            createdAt: new Date(),
            adminId : adminId,
            role
         })
        .then(function(docRef) {
            var organismAddId =firestore.firestore().collection("entreprises").doc(docRef.id)
             organismAddId.update({
                organismId: docRef.id
             })
             firestore.firestore().collection("users").doc(adminId)
             .update({
                organism : name_entreprise
           })
        })
        .then((error)=>{
            if(!error){history.push('/apps/academy/courses')}
            return dispatch({
                type   : 'ENTRPRISE_ERROR',
                payload: error
            });
        })
        .then(()=>{
            return dispatch({
                type   : 'ENTRPRISE_ADDED',
                payload: name_entreprise
            });
        })
    }
}


