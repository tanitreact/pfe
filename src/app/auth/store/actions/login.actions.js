import history from 'history.js';
import firebase from '../../../firebase/fbConfig';
import store from 'app/store';
import * as Actions from 'app/store/actions';
import firestore from '../../../firebase/fbConfig';


export const submitLoginWithFireBase=(email, password)=>{
    return (dispatch, getState, getFirestore,getFirebase) =>{
        firestore.firestore().collection("users").where("email_user", "==", email).get()
        .then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                if(doc.data()){
                    if(!doc.data().emailVerified){
                        store.dispatch(Actions.showMessage({message:'Verifier votre email'}));
                       // history.push('/login')
                    }
                    if(doc.data().organism===null || doc.data().organism==='' && doc.data().emailVerified){
                       firebase.auth().signInWithEmailAndPassword(email, password)
                        dispatch(openEntrepriseDialog())
                    }
                    if(doc.data().organism!==null && doc.data().organism!=='' && doc.data().emailVerified ){
                        firebase.auth().signInWithEmailAndPassword(email, password)
                         .then(() => {
                           dispatch({
                             type: 'LOGIN_SUCCESS'
                             });
                           })
                           .then((error)=>{
                            if(!error){
                                  {history.push('/apps/academy/courses')}
                            }
                           })
                         .catch(error => {
                          const passwordErrorCodes = [
                            'auth/weak-password',
                            'auth/wrong-password'
                         ];
                         const response = {
                           //username: usernameErrorCodes.includes(error.code) ? error.message : null,
                           password: passwordErrorCodes.includes(error.code) ? error.message : null
                         };
                          if(response.username){
                             store.dispatch(Actions.showMessage({message:'Ce compte n est pas enregistre avec ce mail'}));
                          }
                         if(response.password){
                           store.dispatch(Actions.showMessage({message:'Mot de passe invalide'}));
                         }
                          if ( error.code === 'auth/invalid-api-key' )
                          {
                            console.log( error.message);
                          }         
                return dispatch({
                    type   : 'LOGIN_ERROR',
                    payload: response       
                 }
                )
                
            })

                    }            
                }
               // if(!doc.data()){
                   
                //}
            })
        }).then(()=>{
            console.log('no document')
        })
    }
    
}


export const authWithGoogle = () =>{

    return (dispatch, getState, getFirestore,getFirebase) =>{
           var provider = new firebase.auth.GoogleAuthProvider();
           provider.addScope('');

    firebase.auth().signInWithPopup(provider).then(function(result) {

        var user = result.user;
        (user.notification == null ) ? user.notification = false : user.notification = user.notification;
         firestore.firestore().collection("users").doc(result.user.uid).get()
         .then(function(doc) {
            if(doc.exists){
                const user=doc.data()
                if(user.organism==='' || user.organism === undefined){
                    {history.push('/register-complement')}
                }else{
                    {history.push('/apps/academy/courses')} 
                }
            }else{
                console.log('existe',result.user)
                firestore.firestore().collection('users').doc(result.user.uid).set({
                    displayName : result.user.displayName,
                    emailVerified:result.user.emailVerified,
                    uid:result.user.uid,
                    email_user:result.user.email,
                    name:result.user.displayName,
                    organism:'',
                    role:'admin',
                   invitation_status:result.user.emailVerified
                  }).then(()=>{
                    {history.push('/register-complement')}
                  })
            }
        })

   


        

        
       // firestore.firestore().collection("users").where("email_user", "==", user.email).get()
        // .then(function(querySnapshot) {
        //    // console.log('querySnapshot',querySnapshot)
        //     querySnapshot.forEach(function(doc) {
        //         if(doc.data()){  
        //             return(takeUserInDataBase(doc.data()))
        //         }
        //     })
        // })
        //dispatch(updateUserGoogle(user))
       // console.log('user',user.email)
      }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        console.log('// Handle Errors here.',errorCode)
        var errorMessage = error.message;
        // The email of the user's account used.
        console.log('// Handle Errors here.',errorMessage)
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        console.log('// The firebase.auth.AuthCredential type that was used.',email)
        var credential = error.credential;
        // ...
      })
}
}


export function openRegisterDialog()
{
    console.log('action register')
    return {
        type: 'OPEN_REGISTER_DIALOG'
    }
}

export function closeRegisterDialog()
{
    return {
        type: 'CLOSE_REGISTER_DIALOG'
    }
}

export const handleTakeEmail=(email)=>{
    console.log('user email',email)
    return {
        type:'HANDLE_TAKE_EMAIL',
        payload:email
    }
}

export function openLoginDialog()
{
    return {
        type: 'OPEN_LOGIN_DIALOG'
    }
}

export function closeLoginDialog()
{
    return {
        type: 'CLOSE_LOGIN_DIALOG'
    }
}



export function closeEntrepriseDialog()
{
    return {
        type: 'CLOSE_ENTREPRISE_DIALOG'
    }
}

export function openEntrepriseDialog()
{
    return {
            type   : 'OPEN_ENTREPRISE_DIALOG',
        }
}

function takeUserInDataBase(user_data){
    console.log('aaaaaaaaaah',this.updateUserGoogle())
//   if(user_data){
//      return user_data.name;
//   }y
}


function updateUserGoogle(user)
{

    console.log('new user',user.data())
    
}