import history from 'history.js';
import _ from '@lodash';

import firebase from '../../../firebase/fbConfig';
import firestore from '../../../firebase/fbConfig';

export const SET_USER_DATA = '[USER] SET DATA';
export const REMOVE_USER_DATA = '[USER] REMOVE DATA';
export const USER_LOGGED_OUT = '[USER] LOGGED OUT';



/**
 * Set user data from Firebase data
 */
export function setUserDataFirebase(user, authUser)
{
    return(dispatch)=>{
        //dispatch(setDefaultSettings(user.settings));
        createUserSettingsFirebaseMail(authUser);
        dispatch(setUserData(user)) 
    }
}

export function createUserSettingsFirebaseMail(authUser)
{
            
    firestore.firestore().collection("users").doc(authUser.uid)
    .update({
          emailVerified : authUser.emailVerified,
          invitation_status : authUser.emailVerified
     })
    //console.log('reateUserSettingsFirebaseMail',authUser.emailVerified)
}


export function setUserData(user)
{ 

    sessionStorage.setItem("user",user);
   // sessionStorage.setItem("email_verif",user.emailVerified);
    return (dispatch) => {
        dispatch({
            type   : 'SET_USER_DATA',
            payload: user
        })
    }
}

export function setUserDataMailVerif(emailVerif){

    return (dispatch) => {
        dispatch({
            type   : 'SET_USER_DATA_VERIF_MAIL',
            payload: emailVerif
        })
    }
}

/**
 * Logout
 */
export function logoutUser()
{
    sessionStorage.removeItem("user")
    localStorage.clear();
    return (dispatch, getState) => {

        firebase.auth().signOut()

        history.push(
           '/login'
        ); 

        dispatch({
            type: 'USER_LOGGED_OUT'
        })
    }
}
