//import * as Actions from '../actions';

const initialState = {
    toFormation: '',
    error  : {
        username: null,
        password: null
    },
    registerDialog: {
        props: {
            open: false
        },
    },
            openEntreprise: false,
    open:false,
    email_user: '',
};

const login =(state = initialState, action)=> {

    if(action.type==='LOGIN_SUCCESS'){
        console.log('succes')
        return {
            ...initialState,
            toFormation: true
        };
        //console.log(state.success)
    }

    if(action.type==='LOGIN_SUCCESS_GOOGLE'){
        console.log('succes google')
        return {
            ...initialState,
            toFormation: true
        };
        //console.log(state.success)
    }

    if(action.type==='LOGIN_ERROR'){
        console.log('loggin fail',action.payload)
        return {
            toFormation: false,
            error  : action.payload
        };

    }
    // if(action.type === 'OPEN_REGISTER_DIALOG'){
    //     console.log('reducer register')
    //     return {
    //         ...state,
    //         registerDialog: {
    //             props: {
    //                 open: true
    //             },
    //         }
    //     };
    // }

    // if(action.type === 'CLOSE_REGISTER_DIALOG'){
    //     return {
    //         ...state,
    //         registerDialog: {
    //             props: {
    //                 open: false
    //             },
    //         }
    //     };
    // }

    if(action.type === 'OPEN_LOGIN_DIALOG'){
        return {
            ...state,
            open : true
        };
    }
    
    if(action.type === 'CLOSE_LOGIN_DIALOG'){
        return {
            ...state,
            open : false
        };
    }

    if(action.type === 'OPEN_ENTREPRISE_DIALOG'){
        console.log('oooooooooooooooooooooooooooooooooooooooooooooooooooo')
        return {
            ...state,
            openEntreprise: true
        };
    }
    
    if(action.type === 'CLOSE_ENTREPRISE_DIALOG'){
        return {
            ...state,
            openEntreprise: false
        };
    }

    if(action.type === 'HANDLE_TAKE_EMAIL'){
        return{
            ...state,
        email_user:action.payload
        }
    }
    
    return state;
};

export default login;