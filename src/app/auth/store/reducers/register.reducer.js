//import * as Actions from '../actions';

const initialState = {
    success: false,
    error  : {
        username: null,
        password: null
    },
    entrepriseDialog: {
        props: {
            open: false
        },
    },
    toCompletFormulaire: false,
    userRegisterUid:'',
    authError : null,
    entrepCurrent : ''
};

const register = function (state = initialState, action) {
    switch ( action.type )
    {
        case 'REGISTER_SUCCESS':
        {
            console.log('register success')
            return {
                ...state,
                authError : null,
                success : true
            };
        }
        case 'REGISTER_ERROR':
        {
            console.log('register error')
            return {
                ...state,
                authError  : false
            };
        }
        case 'ENTRPRISE_ADDED':
        {
            console.log('ENTRPRISE_ADDED',action.payload)
            return {
                ...state,
                entrepCurrent  : action.payload
            };
        }
        default:
        {
            return state
        }
    }
};

export default register;