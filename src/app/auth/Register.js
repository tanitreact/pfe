import React, {Component} from 'react';
import {withStyles, Button, Card, CardContent, Checkbox, FormControl, FormControlLabel, TextField, Typography,Icon} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { compose} from 'redux';
import {Redirect} from 'react-router-dom';
import { withRouter } from "react-router";
import InputAdornment from '@material-ui/core/InputAdornment';
import _ from '@lodash';

import {registerWithFirebase} from './store/actions/register.actions'

const styles = theme => ({
    root: {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + darken(theme.palette.primary.dark, 0.5) + ' 100%)',
        color     : theme.palette.primary.contrastText
    }
});

class Register extends Component {

    state = {
        name                 : '',
        email                : '',
        password             : '',
        organism             :  null,
        first_name           : '',
        passwordConfirm      : '',
        role                 : 'admin',
        acceptTermsConditions: false
    };

    // componentDidUpdate(){
    
    //     if(this.props.success===true){
    //         this.context.router.push('/mailconfirm');
    //     }
    // }
    handleChange = (event) => {
        this.setState(
            _.set(
                {...this.state},
                 event.target.name, 
                 event.target.type === 'checkbox' ? event.target.checked : event.target.value
                 )
            );
    };

    canBeSubmitted()
    {
        const {email, password, passwordConfirm, acceptTermsConditions} = this.state;
        return (
            email.length > 0 &&
            password.length > 0 &&
            password.length > 6 &&
            password === passwordConfirm &&
            acceptTermsConditions
        );
    }

    handleSubmit = (event) =>{
        event.preventDefault();
        console.log('email',this.state.email)
        //const {email,password} = this.state
        this.props.registerWithFirebase(this.state)
        this.setState({
            name                 : '',
            email                : '',
            password             : '',
            first_name             : '',
            passwordConfirm      : '',
            //organism :'',
            acceptTermsConditions: false
        })
    }

    render()
    {
        const {classes} = this.props;
        const {name, email, password, passwordConfirm,first_name, acceptTermsConditions,organism} = this.state;
        return (
            <div className={classNames(classes.root, "flex flex-col flex-auto flex-no-shrink p-24 md:flex-row md:p-0")}>

                <div
                    className="flex flex-col flex-no-grow items-center text-white p-16 text-center md:p-128 md:items-start md:flex-no-shrink md:flex-1 md:text-left">

                    <FuseAnimate animation="transition.expandIn">
                        <img className="w-128 mb-32" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Valeo_Logo.svg/1200px-Valeo_Logo.svg.png" alt="logo"/>
                    </FuseAnimate>

                    <FuseAnimate animation="transition.slideUpIn" delay={300}>
                        <Typography variant="h3" color="inherit" className="font-light">
                          GOGO APPLICATION
                        </Typography>
                    </FuseAnimate>
                </div>

                <FuseAnimate animation={{translateX: [0, '100%']}}>

                    <Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>

                        <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-40 ">

                            <Typography variant="h6" className="md:w-full mb-32">NOUVEAU COMPTE</Typography>

                            <form name="registerForm" noValidate className="flex flex-col justify-center w-full">

                                <TextField
                                    className="mb-16"
                                    label="Nom"
                                    autoFocus
                                    type="name"
                                    name="name"
                                    value={name}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>person</Icon>
                                          </InputAdornment>,
                                    }}
                                />

                                <TextField
                                    className="mb-16"
                                    label="Prenom"
                                    type="name"
                                    name="first_name"
                                    value={first_name}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>person</Icon>
                                          </InputAdornment>,
                                    }}
                                />

                                <TextField
                                    className="mb-16"
                                    label="Email"
                                    type="email"
                                    name="email"
                                    value={email}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>email</Icon>
                                          </InputAdornment>,
                                    }}
                                />
                                
                                {/* <TextField
                                    className="mb-16"
                                    label="Organisme"
                                    type="name"
                                    name="organism"
                                    value={organism}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                /> */}

                                <TextField
                                    className="mb-16"
                                    label="Mot de passe"
                                    type="password"
                                    name="password"
                                    value={password}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>vpn_key</Icon>
                                          </InputAdornment>,
                                    }}
                                />

                                <TextField
                                    className="mb-16"
                                    label="Mot de passe (Confirmer)"
                                    type="password"
                                    name="passwordConfirm"
                                    value={passwordConfirm}
                                    onChange={this.handleChange}
                                    variant="outlined"
                                    required
                                    fullWidth
                                    InputProps={{
                                        endAdornment: 
                                        <InputAdornment position="end">
                                          <Icon>vpn_key</Icon>
                                          </InputAdornment>,
                                    }}
                                />

                                <Button 
                                   variant="contained" 
                                   color="primary" 
                                   className="w-full mx-auto mt-16" a
                                   ria-label="Register"
                                  // disabled={!this.canBeSubmitted()}
                                   onClick={this.handleSubmit}
                                >
                                    CREER UN NOUVEAU COMPTE
                                </Button>

                            </form>

                            <div className="flex flex-col items-center justify-center pt-32 pb-24">
                                <span className="font-medium">J'ai deja un compte</span>
                                <Link className="font-medium" to="/login">Se connecter</Link>
                            </div>

                        </CardContent>
                    </Card>
                </FuseAnimate>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerWithFirebase:(user) => dispatch(registerWithFirebase(user)),
    }
}

function mapStateToProps(state)
{
    return {
        success: state.register.success
    }
}
//export default withStyles(styles, {withTheme: true})(Login);
export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    // firestoreConnect([
    //   { collection: 'catalogues'},
    // ])
)(withStyles(styles,{withTheme: true})(Register));

//export default withStyles(styles, {withTheme: true})(Register);
