import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {connect} from 'react-redux';
import { compose} from 'redux';
import {Link} from 'react-router-dom';

import {closeLoginDialog} from './store/actions/login.actions'

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class LoginDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Dialog
          open={this.props.openDialog}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {"Bonjour et bienvenu?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Desolé ce compte n'existe pour, pour creer un nouveau compte cliquer sur <span>nouveau compte</span>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={
               ()=>{
                this.props.closeLoginDialog()
               }
                } color="primary"
            >
              Annulez
            </Button>
            <Button 
               component={Link} 
               to="/register" 
               color="primary"
            >
                    Nouveau compte
                </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state)
{
    console.log('open',state.login.open)
    return {
        openDialog: state.login.open
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        closeLoginDialog:() => dispatch(closeLoginDialog()),
    }
}

export default compose(
    connect(mapStateToProps,mapDispatchToProps),
)(LoginDialog);
