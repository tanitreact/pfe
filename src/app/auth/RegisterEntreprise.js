import React, { Component } from 'react'
import {withStyles, Button, Card, CardContent, Checkbox, FormControl, FormControlLabel, TextField, Typography} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import _ from '@lodash';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { compose} from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

import {registerEntreprise} from './store/actions/register.actions'


const styles = theme => ({
    root: {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + darken(theme.palette.primary.dark, 0.5) + ' 100%)',
        color     : theme.palette.primary.contrastText
    }
});

class RegisterEntreprise extends Component {
    

  state = {
    name_entreprise    : '',
    email_entreprise    : '',
    adress_entreprise            : '',
    city                : '',
    postal_code         : '',
    type_organism       : '',
    acceptTermsConditions: false
  };

  handleChange = (event) => {
      this.setState(
          _.set(
              {...this.state},
               event.target.name, 
               event.target.type === 'checkbox' ? event.target.checked : event.target.value
               )
          );
  };

  canBeSubmitted()
  {
      const {name_entreprise,email_entreprise,adress_entreprise,city,postal_code,type_organism,acceptTermsConditions} = this.state;
      return (
        name_entreprise.length > 0 &&
        adress_entreprise.length > 0 &&
        email_entreprise.length > 0 &&
          city.length > 0 &&
          postal_code > 0 &&
          type_organism >0 &&
          acceptTermsConditions
      );
  }

  handleSubmit = (event) =>{
      event.preventDefault();
       const uidAdmin= this.props.userRegisterUid || ''
      this.props.registerEntreprise(this.state,uidAdmin )
      this.setState({
        name_entreprise     : '',
        adress_entreprise            : '',
        city                : '',
        postal_code         : '',
        type_organism        : '',
        email_entreprise     : ' ',
        acceptTermsConditions: false
      })
      //this.props.history.push('/app/users')
  }

  render()
  {
      const {classes} = this.props;
      const {name_entreprise, adress_entreprise,city,postal_code,type_organism,email_entreprise,acceptTermsConditions} = this.state;

      return (
          <div className={classNames(classes.root, "flex flex-col flex-auto flex-no-shrink p-24 md:flex-row md:p-0")}>

                <div
                    className="flex flex-col flex-no-grow items-center text-white p-16 text-center md:p-128 md:items-start md:flex-no-shrink md:flex-1 md:text-left">

                    <FuseAnimate animation="transition.expandIn">
                        <img className="w-128 mb-32" src="assets/images/logos/fuse.svg" alt="logo"/>
                    </FuseAnimate>

                    <FuseAnimate animation="transition.slideUpIn" delay={300}>
                        <Typography variant="h3" color="inherit" className="font-light">
                          TRAINING APP
                        </Typography>
                    </FuseAnimate>
                </div>

              <FuseAnimate animation={{translateX: [0, '100%']}}>

                  <Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>

                      <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-40 ">

                          <Typography variant="h6" className="md:w-full mb-32">CREER NOUVELLE ORGANISME</Typography>

                          <form name="registerForm" noValidate className="flex flex-col justify-center w-full">

                              <TextField
                                  className="mb-16"
                                  label="Nom"
                                  autoFocus
                                  type="name"
                                  name="name_entreprise"
                                  value={name_entreprise}
                                  onChange={this.handleChange}
                                  variant="outlined"
                                  required
                                  fullWidth
                              />
                              <TextField
                               className="mb-16"
                               label="Email"
                               autoFocus
                               type="email"
                               name="email_entreprise"
                               value={email_entreprise}
                               onChange={this.handleChange}
                               variant="outlined"
                               required
                               fullWidth
                               />

                              <TextField
                                  className="mb-16"
                                  label="Adresse"
                                  type="name"
                                  name="adress_entreprise"
                                  value={adress_entreprise}
                                  onChange={this.handleChange}
                                  variant="outlined"
                                  required
                                  fullWidth
                              />
                               <TextField
                                  className="mb-16"
                                  label="Ville"
                                  type="name"
                                  name="city"
                                  value={city}
                                  onChange={this.handleChange}
                                  variant="outlined"
                                  required
                                  fullWidth
                              />

                              <TextField
                                  className="mb-16"
                                  label="code postale"
                                  type="name"
                                  name="postal_code"
                                  value={postal_code}
                                  onChange={this.handleChange}
                                  variant="outlined"
                                  required
                                  fullWidth
                              />

                              <TextField
                                  className="mb-16"
                                  label="Type d'organisme"
                                  type="name"
                                  name="type_organism"
                                  value={type_organism}
                                  onChange={this.handleChange}
                                  variant="outlined"
                                  required
                                  fullWidth
                              />

                              <Button 
                                 variant="contained" 
                                 color="primary" 
                                 className="w-full mx-auto mt-16" a
                                 ria-label="Register"
                                 //disabled={!this.canBeSubmitted()}
                                 onClick={this.handleSubmit}
                              >
                                  CREER  ORGANISME
                              </Button>

                          </form>

                          <div className="flex flex-col items-center justify-center pt-32 pb-24">
                              <span className="font-medium">Already have an account?</span>
                              <Link className="font-medium" to="/pages/auth/login-2">Login</Link>
                          </div>

                      </CardContent>
                  </Card>
              </FuseAnimate>
          </div>
      );
  }
}
//export default RegisterComplet



const mapStateToProps = (state) => {
  return {
      //entreprises: state.firestore.ordered.entreprises,
      user: state.register.userRegisterUid,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerEntreprise:(entreprise,admin) => dispatch( registerEntreprise(entreprise,admin)),
    }
}
export default compose(
    withRouter,
  connect(mapStateToProps,mapDispatchToProps),
  firestoreConnect([
    { collection: 'entreprises'},
  ]))(withStyles(styles,{withTheme: true})(RegisterEntreprise));
