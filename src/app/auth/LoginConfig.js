import {FuseLoadable} from '@fuse';

export const LoginConfig = {
    settings: {
        layout: {
            config: {
                navbar        : {
                    display : false,
                    folded  : false,
                    position: 'left'
                },
                toolbar       : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                footer        : {
                    display : false,
                    style   : 'fixed',
                    position: 'below'
                },
                leftSidePanel : {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes  : [
        {
            path     : '/login',
            component: FuseLoadable({
                loader: () => import('./Login')
            })
        },
        {
            path     : '/register',
            component: FuseLoadable({
                loader: () => import('./Register')
            })
        },
        {
            path     : '/register-complement',
            component: FuseLoadable({
                loader: () => import('./RegisterEntreprise')
            })
        },
        {
            path     : '/mailconfirm',
            component: FuseLoadable({
                loader: () => import('./MailConfirm')
            })
        },
        {
            path     : '/resetpassword',
            component: FuseLoadable({
                loader: () => import('./ResetPassword')
            })
        }
    ]
};